<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;

class UsersExport implements  FromView, WithHeadings, WithEvents
{
    use Exportable;
    protected $data;
    protected $topic;
 
    public function __construct($data,$topic){
 
     $this->data = $data;
     $this->topic = $topic;
     }
 
     public function view(): View
       {
 
           return view('exports.xml', [
               'data' => $this->data,
               'topic' => $this->topic
           ]);
       }
      public function headings(): array
      {
          return [
 
             ['First row', 'Name'],
             ['Second row', '#','Phone Number','Password'],
 
 
           ];
 
      }
 
 
      public function registerEvents(): array
      {
     return [
         AfterSheet::class    => function(AfterSheet $event) {
             // ... HERE YOU CAN DO ANY FORMATTING
              $cellRange = 'A2:E2';
             $event->sheet->getDelegate()->mergeCells($cellRange);
 
             $event->sheet->getColumnDimension('A')->setWidth(13);
             $event->sheet->getColumnDimension('B')->setWidth(13);
         },
     ];
   }
}
