<?php
if (!function_exists('adminMenu')) {
	function adminMenu()
	{
		return [


			'content' => [
				'title' => 'Content',
				'route' => 'admin.content.pages.index',
				'primary_navigation' => [
					'pages' => [
						'title' => 'Pages',
						'module' => true,

					],
					'menuses' => [
						'title' => 'Menus',
						'module' => true,
						'secondary_navigation' => [
							'menuses' => [
								'title' => 'Menus',
								'module' => true
							],
							'menuTypes' => [
								'title' => 'Menu Types',
								'module' => true,

							],
						]

					],
					'footerTexts' => [
						'title' => 'FooterTexts',
						'module' => true
					],

					//  'socialmedia' => [
					// 	'title' => 'Social Media Links',
					// 	 'module' => true,

					// ],
					// 'projecttopics' => [
					// 	'title' => 'Project Topics',
					// 	 'module' => true,

					// ],
				],
			],
			'sessions' => [
				'title' => 'Sessions',
				'module' => true,

			],


			'resources' => [
				'title' => 'Resources',
				'route' => 'admin.resources.resources.index',
				'primary_navigation' => [
					'policies' => [
						'title' => 'Policies',
						'module' => true,
					],
					'resources' => [
						'title' => 'Resources',
						'module' => true,

					],
					'resourceTypes' => [
						'title' => 'Resource Types',
						'module' => true,

					],

				],
			],

			'courses' => [
				'title' => 'Courses',
				'route' => 'admin.courses.index',

			],
			'jobRoles' => [
				'title' => 'Job Roles',
				'module' => true
			],
			'branches' => [
				'title' => 'Branches',
				'module' => true
			],
			'groups' => [
				'title' => 'Groups',
				'route' => 'admin.groups.groups.index',
				'primary_navigation' => [
					'groups' => [
						'title' => 'Groups',
						'module' => true,

					],
					'groupFormats' => [
						'title' => 'Group Formats',
						'module' => true,

					]
				]
			],
            'reports' => [
				'title' => 'Reports',
				'route' => 'admin.graph.dashboard',
				'primary_navigation' => [
					'graph' => [
						'title' => 'Dashboard',
						'route' => 'admin.graph.dashboard',
					],
					'courseCompletions' => [
						'title' => 'Course Completions',
						'module' => true,

					],

					'branch' => [
						'title' => 'Branch Completion',
						'route' => 'admin.branch.completion',
					],
					'userLicenses' => [
						'title' => 'Course Enrollments',
						'module' => true
					],
					'courseReflections' => [
						'title' => 'Course Reflection',
						'module' => true,

					],
				]
			],

			'evaluation' => [
				'title' => 'Evaluation',
				'route' => 'admin.evaluation.userEvaluations.index',
				'primary_navigation' => [
					'userEvaluations' => [
						'title' => 'User Evaluations',
						'module' => true,
					],

					'evaluations' => [
						'title' => 'Evaluation Questions',
						'module' => true
					],
					'evaluationOptions' => [
						'title' => 'Evaluation Options',
						'module' => true
					],

				]
			],


			'messages' => [
				'title' => 'Messages',
				'route' => 'admin.messages.generalMessages.index',
				'primary_navigation' => [
					'generalMessages' => [
						'title' => 'Get in touch Messages',
						'module' => true
					],
					'platformMessages' => [
						'title' => 'Platform Messages',
						'module' => true
					]
				]
			],

		];
	}
}

if (!function_exists('hrMenu')) {
	function hrMenu()
	{
		return [
			'courses' => [
				'title' => 'Courses',
				'route' => 'admin.courses.index',

			],
			'reports' => [
				'title' => 'Reports',
				'route' => 'admin.reports.courseCompletions.index',
				'primary_navigation' => [
					'graph' => [
						'title' => 'Dashboard',
						'route' => 'admin.graph.dashboard',
					],
					'courseCompletions' => [
						'title' => 'Course Completions',
						'module' => true,

					],

					'branch' => [
						'title' => 'Branch Completion',
						'route' => 'admin.branch.completion',
					],
					'userLicenses' => [
						'title' => 'Course Enrollment',
						'module' => true
					],
					'courseReflections' => [
						'title' => 'Course Reflection',
						'module' => true,

					],

				]
			],
			'sessions' => [
				'title' => 'Sessions',
				'module' => true,

			],


			'resources' => [
				'title' => 'Resources',
				'route' => 'admin.resources.resources.index',
				'primary_navigation' => [

					'resources' => [
						'title' => 'Resources',
						'module' => true,

					],
					'resourceTypes' => [
						'title' => 'Resource Types',
						'module' => true,

					],

				],
			],

			'courses' => [
				'title' => 'Courses',
				'route' => 'admin.courses.index',

			],
			'jobRoles' => [
				'title' => 'Job Roles',
				'module' => true
			],
			'branches' => [
				'title' => 'Branches',
				'module' => true
			],
			'groups' => [
				'title' => 'Groups',
				'route' => 'admin.groups.groups.index',
				'primary_navigation' => [
					'groups' => [
						'title' => 'Groups',
						'module' => true,

					],
					'groupFormats' => [
						'title' => 'Group Formats',
						'module' => true,

					]
				]
			],
			'reports' => [
				'title' => 'Reports',
				'route' => 'admin.reports.courseCompletions.index',
				'primary_navigation' => [

					'courseCompletions' => [
						'title' => 'Course Completions',
						'module' => true,

					],
					'userLicenses' => [
						'title' => 'Course Enrollment',
						'module' => true
					],
					'courseReflections' => [
						'title' => 'Course Reflection',
						'module' => true,

					],

				]
			],

			'evaluation' => [
				'title' => 'Evaluation',
				'route' => 'admin.evaluation.userEvaluations.index',
				'primary_navigation' => [
					'userEvaluations' => [
						'title' => 'User Evaluations',
						'module' => true,
					],

					'evaluations' => [
						'title' => 'Evaluation Questions',
						'module' => true
					],
					'evaluationOptions' => [
						'title' => 'Evaluation Options',
						'module' => true
					],

				]
			],


			'messages' => [
				'title' => 'Messages',
				'route' => 'admin.messages.generalMessages.index',
				'primary_navigation' => [
					'generalMessages' => [
						'title' => 'Get in touch Messages',
						'module' => true
					],
					'platformMessages' => [
						'title' => 'Platform Messages',
						'module' => true
					]
				]
			],
		];
	}
}


if (!function_exists('branchManagerMenu')) {
	function branchManagerMenu()
	{
		return [
			'reports' => [
				'title' => 'Branch Reports',
				'route' => 'admin.branchReport.completion',

			],
		];
	}
}



if (!function_exists('hodMenu')) {
	function hodMenu()
	{
		return [
			'reports' => [
				'title' => 'Department Reports',
				'route' => 'admin.departmentReport.completion',

			],


		];
	}
}
