<?php
if (!function_exists('adminDashboard')) {
    function adminDashboard() {

		return  [
			'modules' => [
                'users' => [
					'name' => 'users',
					'label' => 'Total Number of Registered users',
					'label_singular' => 'Userlist',
					'search' => true,
					'count' => true,
					'create' => false,
				],
				'jobRoles' => [
					'name' => 'jobRoles',
					'label' => 'Job Roles',
					'label_singular' => 'Job Role',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],


			],

		];
    }
}

if (!function_exists('hrDashboard')) {
    function hrDashboard() {
		return  [
			'modules' => [
                'users' => [
					'name' => 'users',
					'label' => 'Total Number of Registered users',
					'label_singular' => 'Userlist',
					'count' => true,
				],

				'jobRoles' => [
					'name' => 'jobRoles',
					'label' => 'Job Roles',
					'label_singular' => 'Job Role',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],


			],

		];
    }
}

