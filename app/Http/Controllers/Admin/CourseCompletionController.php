<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\Models\Branch;
use App\Models\LearningStatus;
use App\Models\Course;
use App\Models\JobRole;
use App\Models\Company;
use App\Models\UserRole;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Exports\CourseCompletionExport;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Config\Repository as Config;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Arr;
use App\Repositories\CourseCompletionRepository;



class CourseCompletionController extends BaseModuleController
{

       /**
     * @var Config
     */
    protected $config;

    /**
     * @var AuthFactory
     */
    protected $authFactory;
   /**
     * Indicates if this module is edited through a parent module.
     *
     * @var bool
     */
    protected $submodule = false;

    /**
     * @var int|null
     */
    protected $submoduleParentId = null;


    protected $perPage = 10000;


    protected $moduleName = 'courseCompletions';

    protected $titleColumnKey = 'username';

    protected $indexOptions = [
        'create' =>false,
        'edit' =>false,
        'permalink' => false,
        'publish' =>false,
    ];



    protected $filters = [];
   
    protected $indexColumns = [

        'username' => [ // field column
            'title' => 'User Name',
            'field' => 'username',
        ],

        'email' => [ // field column
            'title' => 'Email',
            'field' => 'email',
        ],

      
      
        'branch' => [ // relation column
            'title' => 'Branch',
            'sort' => true,
            'relationship' => 'branches',
            'field' => 'title'
        ],

         'course_id' => [ // relation column
            'title' => 'Course',
            'sort' => true,
            'relationship' => 'courses',
            'field' => 'name'
        ],

        'completion_status' => [ // relation column
            'title' => 'Status',
            'field' => 'completion_status'
        ],
        'enrollment_date' => [ // field column
            'title' => 'Enrollment Date',
            'field' => 'enrollment_date',
        ],
        'completion_date' => [ // field column
            'title' => 'Completion Date',
            'field' => 'completion_date',
        ],
    ];


    protected $user;

    public function __construct(Application $app, Request $request,CourseCompletionRepository $repository)
    {
        parent::__construct($app, $request);

        $this->middleware(function ($request, $next) {
            $this->user= Auth::user()->role;
            $this->filters = $this->getFilterAll();
      
            return $next($request);
        });
        
      
    }



  
    protected function indexData($request)
    {

        $GrouphrRoles = ['Group HR'];
        $CompanyhrRoles = ['Company HR'];
        $hodRoles = ['HOD'];
        $branchRoles = ['Branch Manager'];

        // if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$hodRoles )) || in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles ) || in_array(auth()->guard('twill_users')->user()->roleValue,$branchRoles )){
        //    $jobRole = JobRole::published()->where('id', Auth::user()->company_id)->get();
        // }else{
           
        //     $jobRole = JobRole::published()->get();
        // }

        // if(auth()->guard('twill_users')->user() && in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles ) || in_array(auth()->guard('twill_users')->user()->roleValue,$branchRoles )){
          
        //     $branch = Branch::published()->where('company_id', Auth::user()->company_id)->get();
        // }else{

        //     $branch = Branch::published()->get();
        // }
    
        return [
           
            'job_role_idList' => JobRole::published()->get(),
            'branch_idList' =>  Branch::published()->get(),
            'course_idList' =>  Course::published()->where('status',1)->get(),
            'status_typeList' =>  LearningStatus::all(),
            'completion_dateList' => '',
            'enrollment_dateList' => '',
            
        ];
    }

    public function dashboard(Request $request){
       
        $launchYear = "2022";
        $eyear = date('Y');
        $cyear = date('Y');


        $emonth = date('m');
        $cmonth = date('m');


        

        $completionChartOptions = [
			'chart_title' => 'Onboarded Users vs User with Completed Compulsory Modules ',
			'subtitle' => 'Onboarded Users vs User with Completed Compulsory Modules ',
			'chart_type' => 'pie',
			'series_data'=>$this->repository->getCompleteComplusoryModules(),
		];

            /** Top 5 courses Enrollments */
        $yearlyEnrollmentGraph = [
            'chart_title' => 'Top 5 courses enrolled into to date',
            'enrollment_data'=>$this->repository->getYearlyTopEnrollments($eyear,$launchYear ),
            ];

        /** End Top 5 courses */

            /** Top 5 courses Completions */
            $yearlyCompletionGraph = [
                'chart_title' => 'Top 5 courses completed to date',
                'completion_data' => $this->repository->getYearlyTopCompletions($cyear,$launchYear),
            ];

            /** End Top 5 courses */


        $enrollmentCompletionGraph = [
			'chart_title' => 'Monthly Enrollments vs Completions',
			'enrollment_data'=>$this->repository->getTopEnrollments($emonth),
            'completion_data' => $this->repository->getTopCompletions( $cmonth),
		];

 
       $completedRegistrationoption = $this->repository->getCompleteRegistrations();


       $pendingRegistrationoption = $this->repository->getPendingRegistrations();

        return view('admin.courseCompletions.dashboard',compact('completionChartOptions', 'completedRegistrationoption', 'pendingRegistrationoption' ,'enrollmentCompletionGraph', 'yearlyEnrollmentGraph','yearlyCompletionGraph'));
    }



    /** Monthly Filter Graph reports */

    public function eresults(Request $request){

        $nmonth = date('m',strtotime($request->emonth));

        return $this->repository->getTopMonthEnrollments($nmonth);

    }


    public function cresults(Request $request){


        $nmonth = date('m',strtotime($request->cmonth));

        return $this->repository->getTopMonthCompletions($nmonth);

    }

    /** End Monthly Filter Reports */



     /**Yearly Filter Graph Reports */

     public function yearlyEResults(Request $request){

        $nyear = $request->eyear;

        return $this->repository->getTopYearlyEnrollments($nyear);

    }


    public function yearlyCRresults(Request $request){


        $nyear = $request->cyear;

        return $this->repository->getTopYearlyCompletions($nyear);

    }


     /** End Yearly Filter Graph Report */



     /** Branch Completion */

    public function branchCompletion(Request $request){


        if ($request->isMethod('post')) {

            if(empty($request->input('country')) && empty($request->input('search'))){

                $errors = "Please Select one option";

                return Redirect::back()->withErrors($errors);
          }else{

          if ($request->input('country')) {

              $branches = Branch::where('country_id', $request->input('country'))->published()->paginate(50);

          } else if ($request->input('search')) {


            $keyword = $request->input('search');

            $branches = Branch::where(function ($query) use ($keyword) { $query->where('title', 'LIKE', '%' .$keyword. '%');})->published()->paginate(50);

          }
        }


        }else{

            $branches = Branch::published()->paginate(50);

        }

        return view('admin.courseCompletions.branchCompletion', compact('branches'));
    }

    
    public function subsidiaryCompletion(Request $request){


        if ($request->isMethod('post')) {

            if(empty($request->input('search'))){

                $errors = "Please Select one option";

                return Redirect::back()->withErrors($errors);
          }else{

            if ($request->input('search')) {


            $keyword = $request->input('search');

            $companies = Company::where(function ($query) use ($keyword) { $query->where('title', 'LIKE', '%' .$keyword. '%');})->published()->paginate(50);

          }
        }


        }else{

            $companies = Company::published()->paginate(50);

        }

        return view('admin.courseCompletions.subsidiaryCompletion', compact('companies'));
    }

    /** Index data for
    * Company Report Set
    *of Company HR */


    public function companyDashboard(Request $request){

        $eyear = date('Y');
        $cyear = date('Y');


        $emonth = date('m');
        $cmonth = date('m');


        $genderChartOptions = [
            'chart_title' => 'Onboarded User by Gender',
            'subtitle' => 'Onboarded Users with completed profiles',
			'chart_type' => 'pie',
			'series_data'=>$this->repository->getCompanyCompleteProfileGender(),
		];


        $completionChartOptions = [
			'chart_title' => 'Onboarded Users vs User with Completed Compulsory Modules ',
			'subtitle' => 'Onboarded Users vs User with Completed Compulsory Modules ',
			'chart_type' => 'pie',
			'series_data'=>$this->repository->getCompanyCompleteComplusoryModules(),
		];


        $yearlyEnrollmentCompletionGraph = [
			'chart_title' => 'Top 5 Courses of the year (Enrollments vs Completions)',
			'enrollment_data'=>$this->repository->getYearlyCompanyTopEnrollments($eyear),
            'completion_data' => $this->repository->getYearlyCompanyTopCompletions($cyear),
		];

        $enrollmentCompletionGraph = [
			'chart_title' => 'Monthly Enrollments vs Completions',
			'enrollment_data'=>$this->repository->getTopCompanyEnrollments($emonth),
            'completion_data' => $this->repository->getTopCompanyCompletions( $cmonth),
		];


       $completedRegistrationoption = $this->repository->getCompanyCompleteRegistrations();


       $pendingRegistrationoption = $this->repository->getCompanyPendingRegistrations();

        return view('admin.courseCompletions.company.dashboard',compact('completionChartOptions', 'completedRegistrationoption', 'pendingRegistrationoption' ,'enrollmentCompletionGraph', 'yearlyEnrollmentCompletionGraph','genderChartOptions'));
    }



    public function companyEresults(Request $request){

        $nmonth = date('m',strtotime($request->emonth));

        return $this->repository->getTopMonthEnrollments($nmonth);

    }


    public function companyCresults(Request $request){


        $nmonth = date('m',strtotime($request->cmonth));

        return $this->repository->getTopMonthCompletions($nmonth);

    }


      /**Yearly Filter Graph Reports */

      public function companyyearlyEResults(Request $request){

        $nyear = $request->eyear;

        return $this->repository->getTopCompanyYearlyEnrollments($nyear);

    }


    public function companyyearlyCRresults(Request $request){


        $nyear = $request->cyear;

        return $this->repository->getTopCompanyYearlyCompletions($nyear);

    }


     /** End Yearly Filter Graph Report */



    public function companyReportCompletion($parentModuleId = null){

        $parentModuleId = parent::getParentModuleIdFromRequest($this->request) ?? $parentModuleId;

        $this->submodule = isset($parentModuleId);
        $this->submoduleParentId = $parentModuleId;

        $indexData = $this->getCompanyIndexData($this->submodule ? [
            $this->getParentModuleForeignKey() => $this->submoduleParentId,
        ] : []);

        if ($this->request->ajax()) {
            return $indexData + ['replaceUrl' => true];
        }

        if ($this->request->has('openCreate') && $this->request->get('openCreate')) {
            $indexData += ['openCreate' => true];
        }



        $view = Collection::make([
            "$this->viewPrefix.company.index",
        ])->first(function ($view) {
            return View::exists($view);
        });


        return View::make($view, $indexData);
    }





    protected function getCompanyIndexData($prependScope = [])
    {
        $scopes = $this->filterScope($prependScope);
        $items = $this->getCompanyIndexItems($scopes);



        $data = [
            'tableData' => $this->getIndexTableData($items),
            'tableColumns' => $this->getIndexTableColumns($items),
            'tableMainFilters' => $this->getIndexTableMainFilters($items),
            'filters' => json_decode($this->request->get('filter'), true) ?? [],
            'hiddenFilters' => array_keys(Arr::except($this->filters, array_keys($this->defaultFilters))),
            'filterLinks' => $this->filterLinks ?? [],
            'maxPage' => method_exists($items, 'lastPage') ? $items->lastPage() : 1,
            'defaultMaxPage' => method_exists($items, 'total') ? ceil($items->total() / $this->perPage) : 1,
            'offset' => method_exists($items, 'perPage') ? $items->perPage() : count($items),
            'defaultOffset' => $this->perPage,
        ] + $this->getIndexUrls($this->moduleName, $this->routePrefix);

        $baseUrl = $this->getPermalinkBaseUrl();

        $options = [
            'moduleName' => $this->moduleName,
            'skipCreateModal' => $this->getIndexOption('skipCreateModal'),
            'reorder' => $this->getIndexOption('reorder'),
            'create' => $this->getIndexOption('create'),
            'duplicate' => $this->getIndexOption('duplicate'),
            'translate' => $this->moduleHas('translations'),
            'translateTitle' => $this->titleIsTranslatable(),
            'permalink' => $this->getIndexOption('permalink'),
            'bulkEdit' => $this->getIndexOption('bulkEdit'),
            'titleFormKey' => $this->titleFormKey ?? $this->titleColumnKey,
            'baseUrl' => $baseUrl,
            'permalinkPrefix' => $this->getPermalinkPrefix($baseUrl),
            'additionalTableActions' => $this->additionalTableActions(),
        ];

        return array_replace_recursive($data + $options, $this->indexData($this->request));
    }



    protected function getCompanyIndexItems($scopes = [], $forcePagination = false)
    {
        return $this->transformIndexItems($this->repository->getForCompany(
            $this->indexWith,
            $scopes,
            $this->orderScope(),
            $this->request->get('offset') ?? $this->perPage ?? 50,
            $forcePagination
        ));
    }


  /** Index data for
    * Branch Report Set
    *of Branch manager */


     public function branchReportCompletion($parentModuleId = null){




        $parentModuleId = parent::getParentModuleIdFromRequest($this->request) ?? $parentModuleId;

        $this->submodule = isset($parentModuleId);
        $this->submoduleParentId = $parentModuleId;

        $indexData = $this->getBranchIndexData($this->submodule ? [
            $this->getParentModuleForeignKey() => $this->submoduleParentId,
        ] : []);

        if ($this->request->ajax()) {
            return $indexData + ['replaceUrl' => true];
        }

        if ($this->request->has('openCreate') && $this->request->get('openCreate')) {
            $indexData += ['openCreate' => true];
        }



        $view = Collection::make([
            "$this->viewPrefix.branch.index",
        ])->first(function ($view) {
            return View::exists($view);
        });


        return View::make($view, $indexData);
    }





    protected function getBranchIndexData($prependScope = [])
    {
        $scopes = $this->filterScope($prependScope);
        $items = $this->getBranchIndexItems($scopes);



        $data = [
            'tableData' => $this->getIndexTableData($items),
            'tableColumns' => $this->getIndexTableColumns($items),
            'tableMainFilters' => $this->getIndexTableMainFilters($items),
            'filters' => json_decode($this->request->get('filter'), true) ?? [],
            'hiddenFilters' => array_keys(Arr::except($this->filters, array_keys($this->defaultFilters))),
            'filterLinks' => $this->filterLinks ?? [],
            'maxPage' => method_exists($items, 'lastPage') ? $items->lastPage() : 1,
            'defaultMaxPage' => method_exists($items, 'total') ? ceil($items->total() / $this->perPage) : 1,
            'offset' => method_exists($items, 'perPage') ? $items->perPage() : count($items),
            'defaultOffset' => $this->perPage,
        ] + $this->getIndexUrls($this->moduleName, $this->routePrefix);

        $baseUrl = $this->getPermalinkBaseUrl();

        $options = [
            'moduleName' => $this->moduleName,
            'skipCreateModal' => $this->getIndexOption('skipCreateModal'),
            'reorder' => $this->getIndexOption('reorder'),
            'create' => $this->getIndexOption('create'),
            'duplicate' => $this->getIndexOption('duplicate'),
            'translate' => $this->moduleHas('translations'),
            'translateTitle' => $this->titleIsTranslatable(),
            'permalink' => $this->getIndexOption('permalink'),
            'bulkEdit' => $this->getIndexOption('bulkEdit'),
            'titleFormKey' => $this->titleFormKey ?? $this->titleColumnKey,
            'baseUrl' => $baseUrl,
            'permalinkPrefix' => $this->getPermalinkPrefix($baseUrl),
            'additionalTableActions' => $this->additionalTableActions(),
        ];

        return array_replace_recursive($data + $options, $this->indexData($this->request));
    }



    protected function getBranchIndexItems($scopes = [], $forcePagination = false)
    {
        return $this->transformIndexItems($this->repository->getForBranch(
            $this->indexWith,
            $scopes,
            $this->orderScope(),
            $this->request->get('offset') ?? $this->perPage ?? 50,
            $forcePagination
        ));
    }


   /** Index data for
    * Job role Report Set
    *of HOD */


   public function departmentReportCompletion($parentModuleId = null){


    $parentModuleId = parent::getParentModuleIdFromRequest($this->request) ?? $parentModuleId;

    $this->submodule = isset($parentModuleId);
    $this->submoduleParentId = $parentModuleId;

    $indexData = $this->getJobRoleIndexData($this->submodule ? [
        $this->getParentModuleForeignKey() => $this->submoduleParentId,
    ] : []);

    if ($this->request->ajax()) {
        return $indexData + ['replaceUrl' => true];
    }

    if ($this->request->has('openCreate') && $this->request->get('openCreate')) {
        $indexData += ['openCreate' => true];
    }



    $view = Collection::make([
        "$this->viewPrefix.jobRole.index",
    ])->first(function ($view) {
        return View::exists($view);
    });


    return View::make($view, $indexData);

 }


    protected function getJobRoleIndexData($prependScope = [])
    {
        $scopes = $this->filterScope($prependScope);
        $items = $this->getJobRoleIndexItems($scopes);



        $data = [
            'tableData' => $this->getIndexTableData($items),
            'tableColumns' => $this->getIndexTableColumns($items),
            'tableMainFilters' => $this->getIndexTableMainFilters($items),
            'filters' => json_decode($this->request->get('filter'), true) ?? [],
            'hiddenFilters' => array_keys(Arr::except($this->filters, array_keys($this->defaultFilters))),
            'filterLinks' => $this->filterLinks ?? [],
            'maxPage' => method_exists($items, 'lastPage') ? $items->lastPage() : 1,
            'defaultMaxPage' => method_exists($items, 'total') ? ceil($items->total() / $this->perPage) : 1,
            'offset' => method_exists($items, 'perPage') ? $items->perPage() : count($items),
            'defaultOffset' => $this->perPage,
        ] + $this->getIndexUrls($this->moduleName, $this->routePrefix);

        $baseUrl = $this->getPermalinkBaseUrl();

        $options = [
            'moduleName' => $this->moduleName,
            'skipCreateModal' => $this->getIndexOption('skipCreateModal'),
            'reorder' => $this->getIndexOption('reorder'),
            'create' => $this->getIndexOption('create'),
            'duplicate' => $this->getIndexOption('duplicate'),
            'translate' => $this->moduleHas('translations'),
            'translateTitle' => $this->titleIsTranslatable(),
            'permalink' => $this->getIndexOption('permalink'),
            'bulkEdit' => $this->getIndexOption('bulkEdit'),
            'titleFormKey' => $this->titleFormKey ?? $this->titleColumnKey,
            'baseUrl' => $baseUrl,
            'permalinkPrefix' => $this->getPermalinkPrefix($baseUrl),
            'additionalTableActions' => $this->additionalTableActions(),
        ];

        return array_replace_recursive($data + $options, $this->indexData($this->request));
    }



    protected function getJobRoleIndexItems($scopes = [], $forcePagination = false)
    {
        return $this->transformIndexItems($this->repository->getForJobRole(
            $this->indexWith,
            $scopes,
            $this->orderScope(),
            $this->request->get('offset') ?? $this->perPage ?? 50,
            $forcePagination
        ));
    }





    public function export(Request $request)
    {

        $data = json_decode($request->input('tableData'));
        $topic[] = ['Name' => 'Course Completion Reports 4G Capital_'.Carbon::now()];

        return Excel::download(new CourseCompletionExport($data,$topic), 'Course Completion Reports'.config('app.name').'_'.Carbon::now().'.xlsx');
    }
   
    
    public function getFilterAll(){

      $superRole = ['SUPERADMIN'];
      $adminRoles = ['Admin'];
      $GrouphrRoles = ['HR'];
      $CompanyhrRoles = ['Company HR'];
      $hodRoles = ['HOD'];
      $branchRoles = ['Branch Manager'];
       
      if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$superRole ) || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ) || in_array(auth()->guard('twill_users')->user()->roleValue,$GrouphrRoles ))){
        $filters= [
            'branch_id' => 'branch_id',
            'job_role_id' => 'job_role_id',
            'course_id' => 'course_id',
            'status_type'=> 'status_type',
            'enrollment_date'=> 'enrollment_date',
            'completion_date'=> 'completion_date',
        ];

    }
    if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles ))){
        $filters= [
            'branch_id' => 'branch_id',
            'job_role_id' => 'job_role_id',
            'course_id' => 'course_id',
            'status_type'=> 'status_type',
            'enrollment_date'=> 'enrollment_date',
            'completion_date'=> 'completion_date',
        ];
    }

    if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$hodRoles ))){
        $filters= [
            'job_role_id' => 'job_role_id',
            'course_id' => 'course_id',
            'status_type'=> 'status_type',
            'enrollment_date'=> 'enrollment_date',
            'completion_date'=> 'completion_date',
        ];
    }

    if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$branchRoles ))){
        $filters= [
            'branch_id' => 'branch_id',
            'job_role_id' => 'job_role_id',
            'course_id' => 'course_id',
            'status_type'=> 'status_type',
            'enrollment_date'=> 'enrollment_date',
            'completion_date'=> 'completion_date',
        ];
    }
        return $filters;

    }



    public function companyInfo($id)
  {

    $branch = Branch::where('company_id', $id)->orderByTranslation('title')->get();
    $jobRole = JobRole::where('company_id', $id)->orderByTranslation('title')->get();

    return response()->json(['branch' => $branch, 'jobRole'=>$jobRole]);
  }

}
