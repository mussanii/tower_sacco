<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\EvaluationOptionRepository;
use App\Repositories\CourseRepository;

class EvaluationController extends ModuleController
{
    protected $moduleName = 'evaluations';
    
    protected $indexOptions = [
        'permalink' => false,
        'publish' =>false,
    ];

    protected function formData($request)
    {
        return [
			'optionList' => app(EvaluationOptionRepository::class)->listAll('title'),
            'courseList' => app(CourseRepository::class)->listAll('name'),
        ];

	}
}
