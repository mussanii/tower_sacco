<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class EvaluationOptionController extends ModuleController
{
    protected $moduleName = 'evaluationOptions';
    
    protected $indexOptions = [
        'permalink' => false,
        'publish' =>false,
    ];
    
}
