<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class GeneralMessageController extends ModuleController
{
    protected $moduleName = 'generalMessages';
    protected $titleColumnKey = 'name';
    
    protected $indexOptions = [
        'create' =>false,
        'edit' =>false,
        'permalink' => false,
        'publish' =>false,
    ];
    

    protected $indexColumns = [
        
        'name' => [ // field column
            'title' => 'Name',
            'field' => 'name',
        ],

        'email' => [ // field column
            'title' => 'Email',
            'field' => 'email',
        ],

        'contact_phone' => [ // relation column
            'title' => 'Phone',
            'field' => 'contact_phone'
        ],

         'subject' => [ // relation column
            'title' => 'Subject',
            'field' => 'subject'
        ],
        'contact_message' => [ // relation column
            'title' => 'Message',
            'field' => 'contact_message'
        ],
    
    ];
}
