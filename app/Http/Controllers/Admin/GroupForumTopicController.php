<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class GroupForumTopicController extends ModuleController
{
    protected $moduleName = 'groupForumTopics';
    
}
