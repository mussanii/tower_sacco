<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\MenusRepository;
use App\Repositories\MenuTypeRepository;

class MenusController extends ModuleController
{
    protected $moduleName = 'menuses';
     
    protected $indexOptions = [
        'reorder' => true,
];

    protected function formData($request)
    {

        return [
			'menuList' => app(MenusRepository::class)->listAll('title'),
            'menuTypeList' => app(MenuTypeRepository::class)->listAll('title'),
			
        ];

	}
    
}
