<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Models\User;
use A17\Twill\Models\Enums\UserRole;
use Illuminate\Config\Repository as Config;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;


class UserController extends ModuleController
{
    //
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var AuthFactory
     */
    protected $authFactory;

    /**
     * @var string
     */
    protected $namespace = 'A17\Twill';

    /**
     * @var string
     */
    protected $moduleName = 'users';

    /**
     * @var string[]
     */
    protected $indexWith = ['medias'];

    /**
     * @var array
     */
    protected $defaultOrders = ['name' => 'asc'];

    /**
     * @var array
     */
    protected $defaultFilters = [
        'search' => 'search',
    ];

    /**
     * @var array
     */
    protected $filters = [
        'role' => 'role',
    ];

    /**
     * @var string
     */
    protected $titleColumnKey = 'name';

    /**
     * @var array
     */
    protected $indexColumns = [
        'name' => [
            'title' => 'Name',
            'field' => 'name',
        ],
        'email' => [
            'title' => 'Email',
            'field' => 'email',
            'sort' => true,
        ],
        'job_role_value' => [
            'title' => 'Job Role',
            'field' => 'job_role_value',
            'sort' => true,
            'sortKey' => 'job_role',
        ],

        'branch_value' => [
            'title' => 'Branch',
            'field' => 'branch_value',
            'sort' => true,
            'sortKey' => 'branch',
        ],

        'created_at' => [
            'title' => 'Registered on',
            'field' => 'created_at',
            'sort' => true,

        ],
        'last_login_at' => [
            'title' => 'Last Login',
            'field' => 'last_login_at',
            'sort' => true,

        ],
    ];

    /**
     * @var array
     */
    protected $indexOptions = [
        'permalink' => false,
    ];

    /**
     * @var array
     */
    protected $fieldsPermissions = [
        'role' => 'manage-users',
    ];

    public function __construct(Application $app, Request $request, AuthFactory $authFactory, Config $config)
    {
        parent::__construct($app, $request);

        $this->authFactory = $authFactory;
        $this->config = $config;

        $this->removeMiddleware('can:edit');
        $this->removeMiddleware('can:delete');
        $this->removeMiddleware('can:publish');
        $this->middleware('can:manage-users', ['only' => ['index']]);
        $this->middleware('can:edit-user,user', ['only' => ['store', 'edit', 'update', 'destroy', 'bulkDelete', 'restore', 'bulkRestore']]);
        $this->middleware('can:publish-user', ['only' => ['publish']]);

        if ($this->config->get('twill.enabled.users-image')) {
            $this->indexColumns = [
                'image' => [
                    'title' => 'Image',
                    'thumb' => true,
                    'variant' => [
                        'role' => 'profile',
                        'crop' => 'default',
                    ],
                ],
            ] + $this->indexColumns;
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function indexData($request)
    {
        return [
            'defaultFilterSlug' => 'published',
            'create' => $this->getIndexOption('create') && $this->authFactory->guard('twill_users')->user()->can('manage-users'),
            'roleList' => Collection::make(UserRole::toArray()),
            'single_primary_nav' => [
                'users' => [
                    'title' => twillTrans('twill::lang.user-management.users'),
                    'module' => true,
                ],
            ],
            'customPublishedLabel' => twillTrans('twill::lang.user-management.enabled'),
            'customDraftLabel' => twillTrans('twill::lang.user-management.disabled'),
        ];
    }

    /**
     * @param Request $request
     * @return array
     * @throws \PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException
     * @throws \PragmaRX\Google2FA\Exceptions\InvalidCharactersException
     */
    protected function formData($request)
    {
        $user = $this->authFactory->guard('twill_users')->user();
        $with2faSettings = $this->config->get('twill.enabled.users-2fa') && $user->id == $request->route('user');

        if ($with2faSettings) {
            $user->generate2faSecretKey();

            $qrCode = $user->get2faQrCode();
        }

        return [
            'roleList' => Collection::make(UserRole::toArray()),
            'single_primary_nav' => [
                'users' => [
                    'title' => twillTrans('twill::lang.user-management.users'),
                    'module' => true,
                ],
            ],
            'customPublishedLabel' => twillTrans('twill::lang.user-management.enabled'),
            'customDraftLabel' => twillTrans('twill::lang.user-management.disabled'),
            'with2faSettings' => $with2faSettings,
            'qrCode' => $qrCode ?? null,
        ];
    }

    /**
     * @return array
     */
    protected function getRequestFilters()
    {
        return json_decode($this->request->get('filter'), true) ?? ['status' => 'published'];
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $items
     * @param array $scopes
     * @return array
     */
    public function getIndexTableMainFilters($items, $scopes = [])
    {
        $statusFilters = [];

        array_push($statusFilters, [
            'name' => twillTrans('twill::lang.user-management.active'),
            'slug' => 'published',
            'number' => $this->repository->getCountByStatusSlug('published'),
        ], [
            'name' => twillTrans('twill::lang.user-management.disabled'),
            'slug' => 'draft',
            'number' => $this->repository->getCountByStatusSlug('draft'),
        ]);

        if ($this->getIndexOption('restore')) {
            array_push($statusFilters, [
                'name' => twillTrans('twill::lang.user-management.trash'),
                'slug' => 'trash',
                'number' => $this->repository->getCountByStatusSlug('trash'),
            ]);
        }

        return $statusFilters;
    }

    /**
     * @param string $option
     * @return bool
     */
    protected function getIndexOption($option)
    {
        if (in_array($option, ['publish', 'delete', 'restore'])) {
            return $this->authFactory->guard('twill_users')->user()->can('manage-users');
        }

        return parent::getIndexOption($option);
    }

    /**
     * @param \A17\Twill\Models\Model $item
     * @return array
     */
    protected function indexItemData($item)
    {

        $user = $this->authFactory->guard('twill_users')->user();
        $canEdit = $user->can('manage-users') || $user->id === $item->id;
        return [
            'edit' => $canEdit ? $this->getModuleRoute($item->id, 'edit') : null,
        ];
    }


    /**
     * @param int $id
     * @param int|null $submoduleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, $submoduleId = null)
    {


        $params = $this->request->route()->parameters();

        $id = last($params);

        $item = $this->repository->getById($id);
        if ($this->repository->delete($id)) {
            $this->fireEvent();
            activity()->performedOn($item)->log('deleted');
            return $this->respondWithSuccess(twillTrans('twill::lang.listing.delete.success', ['modelTitle' => $this->modelTitle]));
        }

        return $this->respondWithError(twillTrans('twill::lang.listing.delete.error', ['modelTitle' => $this->modelTitle]));
    }



    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceDelete()
    {


        $user = User::where('id', $this->request->get('id'))->first();

        dd($this->retireEdxUser($user));

        if ($this->repository->forceDelete($this->request->get('id'))) {
            $this->fireEvent();
            return $this->respondWithSuccess(twillTrans('twill::lang.listing.force-delete.success', ['modelTitle' => $this->modelTitle]));
        }

        return $this->respondWithError(twillTrans('twill::lang.listing.force-delete.error', ['modelTitle' => $this->modelTitle]));
    }

    private function retireEdxUser($user)
    {



        $configLms = config()->get("settings.lms.live");
        $configApp = config()->get("settings.app");

        //if password reset, perform edx password resets
        $username = $user->username;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $configLms['LMS_RETIRE_ACCOUNT_URL']);
        //var_dump($response);die;
        $csrfToken = null;
        foreach ($response->getHeader('Set-Cookie') as $key => $cookie) {
            if (strpos($cookie, 'csrftoken') === FALSE) {
                continue;
            }
            $csrfToken = explode('=', explode(';', $cookie)[0])[1];
            break;
        }

        if (!$csrfToken) {
            //Error, reactivate reset
            Toastr::error("There was a problem logging you in. Please try again later or report to support.");
            return;
        }

        $data = [
            'username' => $username
        ];
        $headers = [
            'Content-Type' => ' application/x-www-form-urlencoded ; charset=UTF-8',
            'Accept' => ' text/html,application/json',
            'X-CSRFToken' => $csrfToken,
            'Cookie' => ' csrftoken=' . $csrfToken,
            'Origin' => $configLms['LMS_BASE'],
            'Referer' => $configLms['LMS_BASE'],
            'X-Requested-With' => ' XMLHttpRequest',
        ];
        $client = new \GuzzleHttp\Client(['verify' => $configApp['VERIFY_SSL']]);
        $cookieJar = \GuzzleHttp\Cookie\CookieJar::fromArray([
            'csrftoken' => $csrfToken
        ], env('LMS_DOMAIN'));

        $client = new \GuzzleHttp\Client();
        try {

            $response = $client->request('POST',  $configLms['LMS_RETIRE_ACCOUNT_URL'], [
                'form_params' => $data,
                'headers' => $headers,
                'cookies' => $cookieJar
            ]);

            //var_dump($response);die;
            //dd($response);
            return true;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset
            return $e->getMessage();
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}
