<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Models\Course;
use Illuminate\Support\Collection;
use App\Repositories\UserLicenseRepository;

class UserLicenseController extends ModuleController
{
    protected $moduleName = 'userLicenses';
    

    protected $titleColumnKey = 'username';
    
    protected $indexOptions = [
        'create' =>false,
        'edit' =>false,
        'permalink' => false,
        'publish' =>false,
        'delete' =>false,
    ];

    protected $filters = [
        'course_id'=> 'course_id',
        'enrollment_date'=> 'enrollment_date'
    ];

    protected $indexColumns = [

        'username' => [ // relation column
            'title' => 'Name',
            'field' => 'username'
        ],
        'email' => [ // field column
            'title' => 'Email',
            'sort' => true,
            'relationship' => 'user',
            'field' => 'email'
        ],

         'course_id' => [ // relation column
            'title' => 'Course',
            'sort' => true,
            'relationship' => 'course',
            'field' => 'name'
        ],
    
        'created_at' => [ // field column
            'title' => 'Enrollment Date',
            'field' => 'created_at',
        ],
    ];


    protected function indexData($request)
    {
        return [
            'course_idList' =>  Course::all(),
            'enrollment_dateList' => '',
        ];
    }
    
}
