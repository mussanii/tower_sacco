<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Models\PasswordValidation;
use App\Models\User;
use App\Models\PasswordReset;
use Mail;
use Session;
use Illuminate\Http\Request;
use AfricasTalking\SDK\AfricasTalking;
use App\Http\Requests\Front\VerifyRequest;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;



    public function sendResetLinkEmail(Request $request){

     

        if(strpos($request->input('username'), '@') !== false && strpos($request->input('username'), '.') !== false){
            
            //verify email activation to microsite;
          $user = User::where('email', $request->input('username'))->first();
      
          }

          if(empty($user)){

            return back()->with('status', 'This email is not registered on the platform. Please confirm the email or proceed and register on the platform.');
  
          }else{
    
        $validation = $this->validationToken($user, $length = 6);
        
        
        $token = self::generateRandomString(16);

       
        $reset = new PasswordReset();
        $reset->email = $user->email;
        $reset->token = $token;
        $reset->save();

        Mail::send('emails.reset',['name' => $user->first_name.' '.$user->last_name, 'username' => $user->username, 'url' => route('password.reset', $reset->token)],
        function ($message) use ($user) {
          $message->from('toweracademy@towersacco.co.ke', 'The Tower Sacco eLearning Program');
          $message->to($user->email, $user->first_name);
          $message->subject('The Tower Sacco eLearning Program Reset Password');
        });

      }
        // $userData = [
        //     'otp_requested'    => true,
        //     'otp_for_user' => $user->email,
        //     'user_id' => $user->id,
        // ];
        //       //dd($user, $user->profile, $token, $notification);
        //       session($userData);
      
        // return redirect('/password/verify')->with('status', 'A verification code has been sent to your registration email. Please enter the code to proceed');	
       
         return back()->with('status', 'An email has been sent to your email address. Please follow the instructions in the email to reset your password.If you do not see the email in your inbox, please check your junk, spam or promotions folder.');
    }

    public function verifyForm(){

        if (request()->session()->get('otp_requested')==null) {
                abort(403, 'You are not allowed on this page');
            }
          $user_id = request()->session()->get('user_id');
            
            return view('auth.passwords.verify',[
                'user_id' => $user_id
            ]);
    }

    public function verify(VerifyRequest $request){
        $data = $request->validated();
        // $user = $this->repository->createForValidation($data);
        $verify = PasswordValidation::where('user_id', $data['user'])->where('token', $data['token'])->first();

        if(!empty($verify)){
        $user = User::where('id', $verify->user_id)->first();

        $reset = PasswordReset::where('email', $user->phone)->first();
        
       return redirect()->route('password.reset', $reset->token);
     }


    }

    protected function generateRandomString($length = 10)
    {
      $characters = '0123456789';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[ rand(0, $charactersLength - 1) ];
      }
    
      return $randomString;
    }

    protected function validationToken($user, $length){
		 
        $validation = new PasswordValidation();
        $validation->user_id  = $user->id;
        $validation->token = $this->generateRandomString($length);
        $validation->save();


        return $validation;


	}
    
}
