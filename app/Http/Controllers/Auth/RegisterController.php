<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests\Front\RegisterRequest;
use App\Http\Requests\Front\VerifyRequest;
use App\Http\Requests\Front\LoginRequest;
use Monarobase\CountryList\CountryListFacade as CountryList;
use App\Helpers\Front;
use App\Models\TwillUsersAuth;
use App\Models\User;
use Carbon\Carbon;
use AfricasTalking\SDK\AfricasTalking;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Ixudra\Curl\Facades\Curl;
use Redirect;
use App\Models\JobRole;
use App\Models\Branch;

use App\Edx\EdxAuthUser;

use Auth;
use Toastr;
use Session;
use Mail;
use App;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    // use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');

       
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required'],
            'password' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    
    public function showRegistrationForm(){
      $roles = JobRole::published()->orderByTranslation('title')->get();
      $branches = Branch::published()->orderByTranslation('title')->get();

      return view('site.auth.register',['roles'=>$roles, 'branches'=>$branches]);
      }


     
}
