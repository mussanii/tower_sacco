<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use App\Models\User;
use App\Models\PasswordReset as Reset;
use Hash;
use Session;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

     /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }


    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );

        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($request, $response)
                    : $this->sendResetLinkFailedResponse($request, $response);
    }

    /**
     * Validate the email for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $request->validate(['email' => 'required|email']);
    }

    /**
     * Get the needed authentication credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only('email');
    }

    /**
     * Get the response for a successful password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse(Request $request, $response)
    {
        return back()->with('status', trans($response));
    }

    /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return back()
                ->withInput($request->only('email'))
                ->withErrors(['email' => trans($response)]);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }


    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token)
    { 

        if ($token) {
            $token = $token;
        }else{
            $token = null;
        //Show session message
        $course_errors = [
            'title' => 'Password Reset Failed',
            'content' => 'There was a problem resetting your password. Please try again later or report to support.',
            // 'action'=>'Google',
            // 'link'=>'google.com'
        ];
     
        Session::flash('course_error', $course_errors);
      return redirect()->route('home');
        }

        $reset = Reset::where('token',$token)->first();

        $user = User::where('phone', $reset->email)->first();

        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $user->email, 'user' => $user]
        );
    }


    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $request->validate($this->rules(), $this->validationErrorMessages());
        
        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.

       $user = User::where('email',$request->input('email'))->first();

       
        $response = $this->resetPassword($user, $request->input('password'));
         
      
        if($response !== true){
            $this->sendResetFailedResponse();
  
          }else{
              $this->sendResetResponse();
          }
          return redirect()->route('home');
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed|min:8',
        ];
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [];
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentialsPassword(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
    }


    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $edxreset = $this->resetEdxPassword($user, $password);

       

        if($edxreset !== true){

            return false;
           
        }else{
            $user->password = Hash::make($password);

            $user->setRememberToken(self::generateRandomString(60));
    
                if( $user->save()){
                    
                    return true;
                }
       
           
        }
       

        // $edxreset = $this->resetEdxPassword($user, $password);

        // return $edxreset;

        // if($edxreset !== true){
        //   $this->sendResetFailedResponse();

        // }else{
        //     $this->sendResetResponse();
        // }

        // event(new PasswordReset($user));
        

    }


    /**
     * Get the response for a successful password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse()
    {
        //Show session message
        $redirectMessage = [
            'title' => 'Password Reset Successful',
            'content' => 'You have successfully changed your password. Please log in to continue browsing through the platform.',
            // 'action'=>'Google',
            // 'link'=>'google.com'
        ];
      Session::flush();

        Session::flash('course_success', $redirectMessage);
      return redirect()->route('home');
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetFailedResponse()
    {
        //Show session message
        $redirectMessage = [
            'title' => 'Password Reset Failed',
            'content' => 'There was a problem resetting your password. Please try again later or report to support.',
            // 'action'=>'Google',
            // 'link'=>'google.com'
        ];
      Session::flush();

        Session::flash('course_error', $redirectMessage);
      return redirect()->route('home');
    }

    private function resetEdxPassword($user, $password)
    {

        

        $configLms = config()->get("settings.lms.live");
        $configApp = config()->get("settings.app");
    
        //if password reset, perform edx password resets
        $email = $user->email;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $configLms['LMS_RESET_PASSWORD_PAGE']);
        //var_dump($response);die;
        $csrfToken = null;
            foreach ($response->getHeader('Set-Cookie') as $key => $cookie) {
                if (strpos($cookie, 'csrftoken') === FALSE) {
                    continue;
                }
                $csrfToken = explode('=', explode(';', $cookie)[0])[1];
                break;
            }

            if (!$csrfToken) {
                //Error, reactivate reset
                Toastr::error("There was a problem logging you in. Please try again later or report to support.");
                return;
            }

        $data = [
            'email' => $email,
            'password' => $password,
            'csrfmiddlewaretoken' => $csrfToken
        ];
        $headers = [
            'Content-Type' => ' application/x-www-form-urlencoded ; charset=UTF-8',
            'Accept' => ' text/html,application/json',
            'X-CSRFToken' => $csrfToken,
            'Cookie' => ' csrftoken=' . $csrfToken,
            'Origin' => $configLms['LMS_BASE'],
            'Referer' => $configLms['LMS_BASE'],
            'X-Requested-With' => ' XMLHttpRequest',
        ];
        $client = new \GuzzleHttp\Client(['verify' => $configApp['VERIFY_SSL']]);
        $cookieJar = \GuzzleHttp\Cookie\CookieJar::fromArray([
            'csrftoken' => $csrfToken
        ], env('LMS_DOMAIN'));

        $client = new \GuzzleHttp\Client();
        try {

          $response = $client->request('POST', $configLms['LMS_RESET_PASSWORD_API_URL'], [
            'form_params' => $data,
            'headers'=>$headers,
            'cookies' => $cookieJar
          ]);

        //var_dump($response);die;
          //dd($response);
          return true;

        }catch (\GuzzleHttp\Exception\ClientException $e) {
          $responseJson = $e->getResponse();
          $response = json_decode($responseJson->getBody()->getContents(),true);
          //Error, delete user

          return $response;
        }catch ( \Exception $e){
          //Error, reactivate reset
          return $e->getMessage();
          // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }

    }


    protected function generateRandomString($length = 10)
    {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[ rand(0, $charactersLength - 1) ];
      }

      return $randomString;
    }
}
