<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SessionMeeting;
use App\Traits\ZoomMeetingTrait;
use Illuminate\Support\Facades\Validator;
use Mail;
use Toastr;

class SessionController extends Controller
{
    //

      //
      use ZoomMeetingTrait;

      const MEETING_TYPE_INSTANT = 1;
      const MEETING_TYPE_SCHEDULE = 2;
      const MEETING_TYPE_RECURRING = 3;
      const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;
  
  
      public function list(Request $request)
  {
      $path = 'users/me/meetings';
      $response = $this->zoomGet($path);
      $data = json_decode($response->getBody(), true);
      $data['meetings'] = array_map(function (&$m) {
          $m['start_at'] = $this->toZoomTimeFormat($m['start_time'], $m['timezone']);
          return $m;
      }, $data['meetings']);
  
  
      return view('meetings.index', compact('data'));
  }
  
  
      public function show($id)
      {
          $meeting = $this->get($id);
  
          return view('meetings.index', compact('meeting'));
      }
  
      public function form(){
  
          return view('meetings.create');
  
      }
  
      public function store(Request $request)
      {
  
          $users = array('paul@farwell-consultants.com');
          $create = $this->create($request->all());
          
  
          foreach($users as $user){
  
          Mail::send('emails.meeting',['url' => $create['data']['join_url'], 'topic' => $create['data']['topic'], 'agenda' => $create['data']['agenda'], 'start_time' => $create['data']['start_time'], 'host' => $create['data']['host_email']],
          function ($message) use ($user) {
            $message->from('support@farwell-consultants.com', 'Meeting Invite');
            $message->to($user, $user);
            $message->subject('The Erevuka eLearning Zoom Meeting');
          });
  
          }
          Toastr::success('Meeting Created successfully.');
          return redirect()->route('meetings.index');
      }
  
      public function update($meeting, Request $request)
      {
          dd($request->all());
          
          $this->update($meeting->zoom_meeting_id, $request->all());
  
          return redirect()->route('meetings.index');
      }
  
      public function destroy(ZoomMeeting $meeting)
      {
          $delete = $this->delete($meeting->id);
  
          dd($delete);
          
          Toastr::success('Meeting deleted successfully.');
          return redirect()->route('meetings.index');
      }
}
