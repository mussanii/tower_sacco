<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\PageRepository;
use App\Models\Course;
use App\Models\UserLicense;
use App\Models\CompanyLicense;
use App\Edx\EdxAuthUser;
use App\Edx\StudentCourseEnrollment;
use App\Models\User;
use App\Models\CourseReflection;
use Illuminate\Http\Request;
use Toastr;
use Auth;
//use ngunyimacharia\openedx\Facades\openedx;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Ixudra\Curl\Facades\Curl;
use Cookie;
use PDF;
use Redirect;
Use Session;
use Validator;


class CourseController extends Controller
{
    //
    protected $pageKey;
    public function __construct()
    {
	$this->detailsKey = PageRepository::PAGE_COURSEDETAILS;

	}


    public function detail(Request $request, $slug)
    {

        if (!isset($_COOKIE['edinstancexid'])) {
            Auth::logout();
            return redirect()->route('login');
        }
       $key = $this->detailsKey;
        $itemPage = app(PageRepository::class)->getPage($key);
       
        $course = Course::where('short_name','=', $slug)->first();
    
        if($course){
        if($course->start > Carbon::today()->toDateString())
        {
            $start = 1;
            $date = date('l jS \of F Y', strtotime($course->start));
            $poptxt = 'This course will be available from '.$date.'';
        }else{
            $start = 0;
            $poptxt = '';
        }
        
        // $enrolled = false;
         $enrolled = Auth::check() ? $this->checkEnrollmentStatus($course->course_id) : false;
         
         if(Auth::check() && UserLicense::where(['course_id' => $course->id, 'user_id' => Auth::user()->id])->first()){
            $license = true;
         }else{
            $license = false;
         }
         $licensed = Auth::check() ?  $license : false;
        //['course' => $course, 'courses' => $courses, 'enrolled' => $enrolled, 'start' => $start, 'text' => $poptxt]
        return view('site.pages.'.$key,['pageItem' => $itemPage,'course' => $course,  'start' => $start, 'poptxt' => $poptxt,'enrolled' => $enrolled,'licensed'=>$licensed]);
        }else{

            return redirect()->back();
        }
    }


    public function enroll($id)
    {
        $configLms = config()->get("settings.lms.live");
        
        $course = Course::where(['course_id' => $id])->first();

        $url = $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware';
          
        $license = UserLicense::where(['course_id' => $course->id, 'user_id' => Auth::user()->id])->first();
          
          if(!empty($license)){
            if (isset($license->enrolled_at)) {
                return redirect($url);
            }else{
                $enroll = $this->edxEnroll($id);
             if($enroll){
              $license->update(['enrolled_at' => Carbon::today()->toDateTimeString()]);

               return Redirect::back();

             }
            }
          }else{
            if($this->checkEnrollmentStatus($id) == true){
               $licenses = new UserLicense();
               $licenses->course_id = $course->id;
               $licenses->user_id = Auth::user()->id;
               $licenses->enrolled_at = Carbon::today()->toDateTimeString();
               $licenses->expired_at = Carbon::now()->endOfYear()->toDateTimeString();
               $licenses->company_license_id = 1;
               $licenses->username = Auth::user()->first_name .' '. Auth::user()->last_name;
               $licenses->save();

               $course_success = [
                   'title' => 'Congratulations!',
                   'content' => 'You have successfully enrolled for the course. Click on Take Course to start the course.',
               ];
               
            Session::flash('course_success', $course_success);
             return Redirect::back();
            }
             $enroll = $this->edxEnroll($id);
            
             if($enroll){

               $licenses = new UserLicense();
               $licenses->course_id = $course->id;
               $licenses->user_id = Auth::user()->id;
               $licenses->enrolled_at = Carbon::today()->toDateTimeString();
               $licenses->expired_at = Carbon::now()->endOfYear()->toDateTimeString();
               $licenses->company_license_id = 1;
               $licenses->username = Auth::user()->first_name .' '. Auth::user()->last_name;
               $licenses->save();

               $course_success = [
                   'title' => 'Congratulations!',
                   'content' => 'You have successfully enrolled for the course. Click on Take Course to start the course.',
               ];
               
               Session::flash('course_success', $course_success);
                return Redirect::back();

             }else{

               $course_errors = [
                     'title' => 'Sorry, course enrollment issues',
                     'content' => 'looks like something went wrong with your course enrollment confirmation.',
                 ];
               
               Session::flash('course_errors', $course_errors);
               return Redirect::back();
             }

           }
    }


    private function checkEnrollmentStatus($course_id){
        //dd(isset($_COOKIE['edinstancexid']));
        $configLms = config()->get("settings.lms.live");
        if (!isset($_COOKIE['edinstancexid'])) {
            Auth::logout();
            return false;
        }
        $client = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                ]
            ]
        );
      
        $request = $client->request('GET',  $configLms['LMS_BASE'] . '/api/enrollment/v1/enrollment/' . Auth::user()->username . ',' . $course_id);
      
        $response = json_decode($request->getBody()->getContents());
        
        if ($response && $response->is_active == true) {
            $enrollmentStatus = true;
        } else {
            $enrollmentStatus = false;
        }
        //dd($enrollmentStatus);
        return $enrollmentStatus;

    }



    private function edxEnroll($course_id)
    {
        $configLms = config()->get("settings.lms.live");
        if ($this->checkEnrollmentStatus($course_id)) {
            return Toastr::error("You're already enrolled to this course");
        }

        $courseInfoObject = new \stdClass();
        $courseInfoObject->course_id = $course_id;
        $enollAttributesObject = new \stdClass();
        $enollAttributesObject->namespace = 'honor';
        $enollAttributesObject->name = 'Towersacco';
        $enollAttributesObject->value = 'Towersacco';
        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->user = Auth::user()->slug;
        $enrollmentInfoObject->mode = 'honor';
        $enrollmentInfoObject->is_active = true;
        $enrollmentInfoObject->course_details = $courseInfoObject;
        $enrollmentInfoObject->enrollment_attributes = [$enollAttributesObject];
        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                ]
            ]
        );
        try {
            $response = $enrollClient->request('POST', $configLms['LMS_BASE'] . '/api/enrollment/v1/enrollment', [
                \GuzzleHttp\RequestOptions::JSON => $enrollmentInfoObject
            ]);
            //dd($response);
            return true; //Toastr::success("You have successfully enrolled into this course");
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            return false;
        }
    }


    private function edxgetCourses()
  {
    $configLms = config()->get("settings.lms.live");
        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('GET', $configLms['LMS_BASE'] . '/api/courses/v1/courses/?page_size=1000');
            $courses =  json_decode($response->getBody()->getContents())->results;
            foreach ($courses as $key => $value) {
                $course = (array)$value;
                $courses[$key] = (array)$courses[$key];
                $course['overview'] = $this->getOverview($course);
                //Remove unwanted fields
                $course['course_video_uri'] = $course['media']->course_video->uri;
                $course['course_image_uri'] = $configLms['LMS_BASE'] . $course['media']->course_image->uri;
                unset($course['media']);
                unset($course['course_id']);
                //Format datetime
                $vowels = array("T", "Z");
                $course['start'] =  date('Y-m-d H:m:i', strtotime(str_replace($vowels, " ", $course['start'])));
                $course['end'] = date('Y-m-d H:m:i', strtotime(str_replace($vowels, " ",$course['end'])));
                $course['enrollment_start'] = date('Y-m-d H:m:i', strtotime($course['enrollment_start']));
                $course['enrollment_end'] = date('Y-m-d H:m:i', strtotime($course['enrollment_end']));
                //Format time
                $exploded_effort = explode(":", $course['effort']);
                switch (count($exploded_effort)) {
                    case '3':
                        $course['effort'] = Carbon::createFromTime($exploded_effort[0], $exploded_effort[1], $exploded_effort[2])->toTimeString();
                        break;
                    case '2':
                        $course['effort'] = Carbon::createFromTime(0, $exploded_effort[0], $exploded_effort[1])->toTimeString();
                        break;
                }
                $courses[$key] = $course;
            }
            return $courses;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = $responseJson->getBody()->getContents();
            //dd($response);
        }
  }


      private function getOverview($course)
    {
        $configLms = config()->get("settings.lms.live");
        $client = new \GuzzleHttp\Client();
        try {
            //Get course description
            $request = $client->request('GET', $configLms['LMS_BASE'] . '/api/courses/v1/courses/' . $course['id']);
            $response = json_decode($request->getBody()->getContents());
            return $response->overview;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = $responseJson->getBody()->getContents();
            return Toastr::error("Error enrolling into course");
            return false;
        }
    }

  


public function processReflection(Request $request){
 
    $validator = Validator::make($request->except('_token'), [
        'username' => 'required',
        'course' => 'required',
        'user' => 'required',
        'reflection' =>  'required',
    ]);

    if($validator->fails()){
       return response()->json(['message' => $validator->errors()], 401);
     }else{

  
    $process = new CourseReflection();
    $process->username = $request->username;
    $process->user_id = $request->user;
    $process->course_id = $request->course;
    $process->reflection = $request->reflection;
    $process->save();

      return response()->json([ 'message' => 'A Validation code has been sent to your phone!', 'status'=>"OK"], 201);
     }
}
  

}
