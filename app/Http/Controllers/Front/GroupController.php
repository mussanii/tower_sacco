<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\GroupUser;
use App\Models\GroupType;
use App\Models\GroupFormat;
use App\Models\GroupTheme;
use App\Models\User;
use App\Models\Notification;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use Auth;
use Toastr;
use File;
use Mail;
use Session;
use A17\Twill\Models\Enums\UserRole;
use App\Models\GroupActivity;
use App\Models\GroupActivityReply;
use App\Models\GroupActivityReaction;
use App\Models\GroupActivityReplyReaction;
use App\Models\GroupActivityReplyReplies;
use App\Http\Requests\Front\CreateActivityRequest;
use App\Models\GroupResource;
use App\Models\ResourceType;
use App\Models\ResourceTheme;
use App\Models\GroupForum;
use App\Models\GroupForumTopic;
use App\Models\GroupForumUser;
use App\Models\GroupForumReply;
use App\Models\GroupForumReaction;
use App\Models\GroupForumReplyReaction;
use App\Models\GroupForumReplyReplies;

use Monarobase\CountryList\CountryListFacade as CountryList;
use Symfony\Component\HttpFoundation\Response;
use Redirect;

class GroupController extends Controller
{

  public function __construct()
  {
  $this->admin = 'SUPERADMIN';
 
}

  public function list(Request $request){
   

    if($request->isMethod('post')){

      $groups = Group::where('group_theme',$request->input('theme'))->get();
      $AACgroup = Group::where('group_type_id',Group::TYPE_AAC)->where('group_theme',$request->input('theme'))->get();
      $Partgroup = Group::where('group_type_id',Group::TYPE_PARTICIPANTS)->where('group_theme',$request->input('theme'))->get();
      $groupTypes = GroupType::all();
      $groupFormat = GroupFormat::all();
      $groupTheme = GroupTheme::all();
      

    }else{
      $groups = Group::all();
      $AACgroup = Group::where('group_type_id',Group::TYPE_AAC)->get();
      $Partgroup = Group::where('group_type_id',Group::TYPE_PARTICIPANTS)->get();
      $groupTypes = GroupType::all();
      $groupFormat = GroupFormat::all();
      $groupTheme = GroupTheme::all();
    }

    return view('site.pages.connect.connect_groups', ['groups'=>$groups,'groupTypes'=>$groupTypes, 'groupFormat'=>$groupFormat, 'groupTheme'=> $groupTheme, 'AACgroups'=> $AACgroup, 'Partgroup'=> $Partgroup]);

  }

   public function details($id,$key){

     $group = Group::where('id',$id)->first();
     $activities = GroupActivity::with(['user', 'replies.replyReplies', 'replies.replyReactions', 'reactions'])
     ->where('group_id', $group->id)->latest()->get();

     $resources = GroupResource::where('group_id', $group->id)->latest()->get();
     
    $resourceTypes = ResourceType::published()->orderBy('position', 'asc')->get();
   
   $requests = GroupUser::where('groups_id',$id)->where('joined',2)->get();

   $receive = count($requests);
    
     return view('site.pages.connect.group_details', ['group'=>$group, 'activities'=> $activities,  'resourceTypes' => $resourceTypes, 'resources'=> $resources,'requests'=>$receive, 'key'=>$key]);

   }


   public function request($id,$key){
   
    $group= Group::where('id',$id)->first();

    if($group->format->id == Group::TYPE_PRIVATE){
      $joined = 2;
    }else{
      $joined = 1;
    }

     $groupuser = new GroupUser();
     $groupuser->twill_users_id = Auth::user()->id;
     $groupuser->groups_id = $id;
     $groupuser->joined = $joined;
     $groupuser->save();


     if($joined ==2){
      $course_success = [
        'title' => 'Congratulations!',
        'content' => 'You have successfully requested to join the group',
     ];
     }else{
      $course_success = [
        'title' => 'Congratulations!',
        'content' => 'You have successfully joined the group',
     ];

     }
   
  
   Session::flash('course_success', $course_success);

     return redirect()->route('pages',['key'=>$key]);

  }


  public function activate($id,$key){
   
     $groupuser = GroupUser::where('twill_users_id', Auth::user()->id)->where('groups_id',$id)->first();
     $groupuser->joined = 1;
     $groupuser->update();

     $course_success = [
      'title' => 'Congratulations!',
      'content' => 'You have successfully joined the group',
   ];
  
   Session::flash('course_success', $course_success);

     return redirect()->route('group.details',['id'=>$id,'key'=> $key]);

  }

  public function decline($id,$key){
   
     $groupuser = GroupUser::where('twill_users_id', Auth::user()->id)->where('groups_id',$id)->first();
     $groupuser->delete();
    

     $course_success = [
      'title' => 'Group!',
      'content' => 'You have declined to join the group',
   ];
  
   Session::flash('course_errors', $course_success);


     return redirect()->route('pages',['key'=>$key]);

  }


    public function form(){

    
        $formats = GroupFormat::all();
        $users = User::where('id','!=',Auth::user()->id)->where('id','!=',1)->get();

        return view('site.pages.connect.group_create',['users'=>$users,'formats'=>$formats]);

    }

    public function store(Request $request){

      $request->validate([
        'title' => ['required'],
        'description' => ['required'],
        'format' => ['required'],
        'image' => ['required'],
      ]);
  
     $users = $request->input('users');
      
     $link = $request->input('link');

      $group = new Group();
      $group->title = $request->input('title');
      $group->description= $request->input('description');
      $group->group_type_id = $request->input('type');
      $group->group_format = $request->input('format');
      $group->group_theme = $request->input('theme');
      $group->created_by = Auth::user()->id;
      $group->published = 1;

      if (!is_null($request->file('image')))
      {
        $group->cover_image = $this->Fileupload($request,'image','Connect_Groups');
      }

     $group->save();
    
     $name = $group->user->first_name . ' ' . $group->user->last_name;

     $message = [
      'type'=> Notification::GROUP,
      'subject'=> 'Group Invite',
      'model_id' => $group->id,
      'message' => 'A group titled <b>'. $group->title .'</b> has been created to on the platform by '.$name.'<br> In order to accept the invite please click the link below <a href="'.route("group.activate",["id"=> $group->id,'key'=> $link]).'" class="btn-drk-left" style=" background: #F87522;padding: 10px 15px;color: #fff;font-size: 18px;text-decoration: none;"> Accept Invite</a></p>',
     ];

     $groupus = new GroupUser();
     $groupus->twill_users_id =  $group->created_by;
     $groupus->groups_id = $group->id;
     $groupus->joined = 1;
     $groupus->save();

     if($users){
      foreach($users as $user){
        $groupuser = new GroupUser();
        $groupuser->twill_users_id = $user;
        $groupuser->groups_id = $group->id;
        $groupuser->joined = 3;
        $groupuser->save();
 
       
        $users = User::where('id',$user)->first();
 
 
        $users->notify(new \App\Notifications\NewGroupUserNotification($message));
 
        Mail::send('emails.group_invite_email',['name' => $users->first_name.' '.$users->last_name, 'creater' => $name, 'title'=> $group->title, 'url' => route('login'),'accept_url' => route("group.activate",["id"=> $group->id,'key'=> $link]),'decline_url' => route("group.decline",["id"=> $group->id,'key'=> $link]) ],
        function ($message) use ($users) {
          $message->from('support@farwell-consultants.com', 'Tower Sacco eLearning Program');
          $message->to($users->email, $users->first_name);
          $message->subject('Tower Sacco eLearning Program Group Invite');
        });
 
      }
     }
 
      if ($group) {
        $course_success = [
          'title' => 'Congratulations!',
          'content' => 'Group successfully created.',
       ];
      
       Session::flash('course_success', $course_success);
      } else {
        $course_error = [
          'title' => 'Sorry!',
          'content' => 'There was a problem creating the Group. Please try again.',
       ];
      
       Session::flash('course_error', $course_success);
        
      }
      return redirect()->route('pages',['key'=> $link]);
    }



    public function myFollowRequests($id)
    {
        $details = [];
        $i = 0;
        $req = GroupUser::where('groups_id',$id)->where('joined',2)->get();

        foreach ($req as $request) {
            $user = User::where('id', $request->twill_users_id)->first();

            if (isset($user->id)) {
                $details[$i]['id'] = $request->id;
                $details[$i]['user_id'] = $user->id;
                $details[$i]['name'] = ucwords($user->first_name.' '.$user->last_name);
                $i++;
            }
        }
        return response()->json(['details' => $details], Response::HTTP_OK);
    }


    public function myFollowAccept($id, $status)
    {

    
        $following = GroupUser::where('id',$id)->where('joined', 2)->first();

        $following->joined = $status;
        $following->update();

        if ($status == 1) {
            $redirectMessage = [
               'title' => 'Success',
               'content' => 'Thank you for accepting follow request.',
           ];

            Session::flash('course_success', $redirectMessage);
        } else {
            $redirectMessage = [
               'title' => 'Success',
               'content' => 'Thank you for responding follow request.',
           ];

            Session::flash('course_success', $redirectMessage);
        }


        return Redirect::back();
    }


    public function createAcivity(CreateActivityRequest $request)
    {
        $images = $request->file('images');
        $gifs = $request->file('gifs');
        $files = $request->file('files');

        $imagesToSave = GroupActivity::saveFiles($images);
        $gifsToSave = GroupActivity::saveFiles($gifs);
        $filesToSave = GroupActivity::saveFiles($files);

        $activity = new GroupActivity([
            'user_id' => Auth::id(),
            'group_id' => $request->input('group'),
            'body' => $request->input('body'),
            'images' => json_encode($imagesToSave),
            'gifs' => json_encode($gifsToSave),
            'attachments' => json_encode($filesToSave)
        ]);

        $activity->save();

        return redirect()->back();
    }



    public function deleteActivity(GroupActivity $userActivity)
    {
        $userActivity->delete();

        return redirect()->back();
    }

    /**
     * Create Activity Reply
     *
     * @param model $userActivity
     * @param request $request
     */
    public function createAcivityReply(Request $request, GroupActivity $userActivity)
    {
     
        $images = $request->file('images');
        $gifs = $request->file('gifs');

        $imagesToSave = GroupActivity::saveFiles($images);
        $gifsToSave = GroupActivity::saveFiles($gifs);

        $reply = new GroupActivityReply([
            'group_activity_id' => $userActivity->id,
            'user_id' => Auth::id(),
            'comment' => $request->input('comment'),
            'images' => json_encode($imagesToSave),
            'gifs' => json_encode($gifsToSave)
        ]);

        $reply->save();

        return redirect()->back();
    }

    /**
     * Delete Reply
     *
     * @param model $userActivityReply
     */
    public function deleteReply(GroupActivityReply $userActivityReply)
    {
        $userActivityReply->deleted_by = Auth::id();

        $userActivityReply->save();

        $userActivityReply->delete();

        return redirect()->back()->withSuccess('Reply deleted successfully');
    }


    public function reaction(Request $request, $userActivityId)
    {
        if (!empty($request->data == 'like')) {
            $reactions = GroupActivityReaction::firstOrCreate([
                'user_id' => Auth::id(),
                'group_activity_id' => $userActivityId
            ]);
            if ($reactions->save()) {
                $reactions->update([
                    'likes' => GroupActivityReaction::REACTION,
                    'dislikes' => 0
                ]);
            }

        } elseif (!empty($request->data == 'dislike')) {
            $reactions = GroupActivityReaction::firstOrCreate([
                'user_id' => Auth::id(),
                'group_activity_id' => $userActivityId
            ]);
            if ($reactions->save()) {
                $reactions->update([
                    'likes' => 0,
                    'dislikes' => GroupActivityReaction::REACTION
                ]);
            }
        }

        return [
            'likes' => GroupActivityReaction::where('group_activity_id', $userActivityId)->get()->sum('likes'),
            'dislikes' => GroupActivityReaction::where('group_activity_id', $userActivityId)->get()->sum('dislikes')
        ];
    }

    public function replyOnActionReply(Request $request, GroupActivity $userActivity, GroupActivityReply $userActivityReply)
    {
        $images = $request->file('images');
        $gifs = $request->file('gifs');

        $imagesToSave = $this->saveFiles($images);
        $gifsToSave = $this->saveFiles($gifs);

        $reply = new GroupActivityReplyReplies([
            'user_activity_id' => $userActivity->id,
            'group_activity_reply_id' => $userActivityReply->id,
            'user_id' => Auth::id(),
            'comment' => $request->input('comment'),
            'images' => json_encode($imagesToSave),
            'gifs' => json_encode($gifsToSave)
        ]);

        $reply->save();

        return redirect()->back();
    }

    public function reactOnReply(Request $request, $userActivityId, $userActivityReplyId)
    {
        if (!empty($request->data == 'like')) {
            $reactions = GroupActivityReplyReaction::firstOrCreate([
                'user_id' => Auth::id(),
                'group_activity_id' => $userActivityId,
                'group_activity_reply_id' => $userActivityReplyId
            ]);
            if ($reactions->save()) {
                $reactions->update([
                    'likes' => GroupActivityReaction::REACTION,
                    'dislikes' => 0
                ]);
            }
        } elseif (!empty($request->data == 'dislike')) {
            $reactions = GroupActivityReplyReaction::firstOrCreate([
                'user_id' => Auth::id(),
                'group_activity_id' => $userActivityId,
                'group_activity_reply_id' => $userActivityReplyId
            ]);
            if ($reactions->save()) {
                $reactions->update([
                    'likes' => 0,
                    'dislikes' => GroupActivityReaction::REACTION
                ]);
            }
        }

        return [
            'likes' => GroupActivityReplyReaction::where('group_activity_id', $userActivityId)
                    ->where('group_activity_reply_id', $userActivityReplyId)->get()->sum('likes'),
            'dislikes' => GroupActivityReplyReaction::where('group_activity_id', $userActivityId)
                    ->where('group_activity_reply_id', $userActivityReplyId)->get()->sum('dislikes')
        ];
    }

    /**
     * Process files eg. Images & Gif and save them in publick directory
     * @param files $files
     */
    public function saveFiles($files)
    {
        return collect($files)->map(function ($file) {
            $fileName = uniqid().$file->getClientOriginalName();
            $fileName = strtolower(str_replace(' ', '_', $fileName));
            $file->move('Connect_Groups/activity', $fileName);
            return $fileName;
        });
    }


    public function groupResource(Request $request ){

    if($request->hasFile('video_file')){
      $image = $request->file('video_file');
      $VideoName = $image->getClientOriginalName();
      $image->move(public_path('Connect_Groups/video_file'), $VideoName);
      }
    if($request->hasFile('audio_file')){
        $image = $request->file('audio_file');
        $AudioName = $image->getClientOriginalName();
        $image->move(public_path('Connect_Groups/audio_file'), $AudioName);
      }
    if($request->hasFile('downloable_file')){
        $image = $request->file('downloable_file');
        $DownloadName = $image->getClientOriginalName();
        $image->move(public_path('Connect_Groups/downloable_file'), $DownloadName);
       }
      if($request->input('article_title')){
        $title = $request->input('article_title');
        $description = $request->input('article_description');
      }
     
      if($request->input('audio_title')){
        $title = $request->input('audio_title');
        $description = $request->input('audio_description');
      }
      if($request->input('video_title')){
        $title = $request->input('video_title');
        $description = $request->input('video_description');
      }

      if($request->input('publication_title')){
        $title = $request->input('publication_title');
        $description = $request->input('publication_description');
      }

    $resource = new GroupResource();
    $resource->title = $title;
    $resource->description = $description;
    $resource->external_link = $request->input('external_link');
    $resource->resource_type_id = $request->input('resource_type_id');
    $resource->resource_theme_id = $request->input('resource_theme_id');
    $resource->user_id = $request->input('resource_creator');
    $resource->group_id = $request->input('group_id');
    $resource->resource_country = $request->input('resource_country');
    $resource->cover_image = (! empty($FileName)) ? $FileName : null ;
    $resource->audio_file = (! empty($AudioName)) ? $AudioName : null ;
    $resource->video_file = (! empty($VideoName)) ? $VideoName : null ;
    $resource->downloable_file = (! empty($DownloadName)) ? $DownloadName : null ;


    $resource->save();


       
        return response()->json(['success'=>'Resource has been uploaded successfully.']);

}


public function forumIndex(Request $request, $id = null){

  // dd($id);
  if($request->isMethod('post')){
    $forums = GroupForum::where('forum_topic',$request->input('theme'))->where('group_id',$id)->latest()->get();
}else{
    $forums = GroupForum::where('group_id',$id)->latest()->get();
}
  $types = GroupForum::getTypes();
  $topics = GroupForumTopic::all();
  // $forums = GroupForum::latest()->get();


  return view('site.pages.connect.group_connect_forums',['topics'=>$topics, 'types'=> $types,'forums'=>$forums,'group_id'=>$id]);

}

public function groupStoreTopic(Request $request){

 $group_id = $request->input('group_id');

  $topic = new GroupForumTopic();
  $topic->title = $request->input('title');
  $topic->save();
  
  $course_success = [
      'title' => 'Congratulations!',
      'content' => 'Topic successfully created.',
   ];
  
   Session::flash('course_success', $course_success);

   return redirect()->route('group.connect.forum',['groupid'=>$group_id]);
  

}

public function storeForum(Request $request){

  $forum = new GroupForum();
  $forum->title = $request->input('title');
  $forum->description = $request->input('description');
  $forum->forum_type = $request->input('type');
  $forum->forum_topic = $request->input('topic');
  $forum->created_by = Auth::user()->id;
  $forum->group_id = $request->input('group_id');

  $forum->save();

  $user = new GroupForumUser();
  $user->group_forum_id = $forum->id;
  $user->user_id = $forum->created_by;
  $user->joined= 1;

  $user->save();


  $course_success = [
      'title' => 'Congratulations!',
      'content' => ' IBCLN successfully created.',
   ];
  
   Session::flash('course_success', $course_success);

   return redirect()->route('group.connect.forum',['groupid'=>$forum->group_id]);

}

public function follow($id){

   $forum = new GroupForumUser();
   $forum->group_forum_id = $id;
   $forum->user_id = Auth::user()->id;
   $forum->joined= 1;

   $forum->save();


   return redirect()->route('group.forum.details',['id'=>$id]);

}


public function getForums($id){

  $forums = GroupForum::with('topic','user')->where('forum_topic',$id)->latest()->get();

  return['forums'=>$forums];


}

public function getStatus($group, $user){

  $status = GroupForumUser::where('group_forum_id',$group)->where('user_id',$user)->first();

  if(empty($status)){
      return '0';
  }else{
      return $status->joined;
  }


}


public function detailForum($id){


  $activities = GroupForum::with(['user', 'replies.replyReplies', 'replies.replyReactions', 'reactions'])
  ->where('id', $id)->latest()->get();


   return view('site.pages.connect.group_forum_detail', ['activities' => $activities]);

}

  /**
* Create Activity Reply
*
* @param model $userActivity
* @param request $request
*/
public function forumCreateAcivityReply(Request $request, GroupForum $userActivity)
{


  $reply = new GroupForumReply([
      'group_forum_id' => $userActivity->id,
      'user_id' => Auth::id(),
      'comment' => $request->input('description'),
      
  ]);

  $reply->save();

  return redirect()->back();
}

// /**
// * Delete Reply
// *
// * @param model $userActivityReply
// */
// public function deleteReply(ForumReply $userActivityReply)
// {
//   $userActivityReply->deleted_by = Auth::id();

//   $userActivityReply->save();

//   $userActivityReply->delete();

//   return redirect()->back()->withSuccess('Reply deleted successfully');
// }


public function forumReaction(Request $request, $userActivityId)
{
  if (!empty($request->data == 'like')) {
      $reactions = GroupForumReaction::firstOrCreate([
          'user_id' => Auth::id(),
          'group_forum_id' => $userActivityId
      ]);
      if ($reactions->save()) {
          $reactions->update([
              'likes' => GroupForumReaction::REACTION,
              'dislikes' => 0
          ]);
      }

  } elseif (!empty($request->data == 'dislike')) {
      $reactions = GroupForumReaction::firstOrCreate([
          'user_id' => Auth::id(),
          'group_forum_id' => $userActivityId
      ]);
      if ($reactions->save()) {
          $reactions->update([
              'likes' => 0,
              'dislikes' => GroupForumReaction::REACTION
          ]);
      }
  }

  return [
      'likes' => GroupForumReaction::where('group_forum_id', $userActivityId)->get()->sum('likes'),
      'dislikes' => GroupForumReaction::where('group_forum_id', $userActivityId)->get()->sum('dislikes')
  ];
}

public function forumReplyOnActionReply(Request $request, GroupForum $userActivity, GroupForumReply $userActivityReply)
{
  $images = $request->file('images');
  $gifs = $request->file('gifs');

  $imagesToSave = $this->saveFiles($images);
  $gifsToSave = $this->saveFiles($gifs);

  $reply = new GroupForumReplyReplies([
      'group_forum_id' => $userActivity->id,
      'group_forum_reply_id' => $userActivityReply->id,
      'user_id' => Auth::id(),
      'comment' => $request->input('description-reply'),
      'images' => json_encode($imagesToSave),
      'gifs' => json_encode($gifsToSave)
  ]);

  $reply->save();

  return redirect()->back();
}

public function forumReactOnReply(Request $request, $userActivityId, $userActivityReplyId)
{
  if (!empty($request->data == 'like')) {
      $reactions = GroupForumReplyReaction::firstOrCreate([
          'user_id' => Auth::id(),
          'group_forum_id' => $userActivityId,
          'group_forum_reply_id' => $userActivityReplyId
      ]);
      if ($reactions->save()) {
          $reactions->update([
              'likes' => GroupForumReaction::REACTION,
              'dislikes' => 0
          ]);
      }
  } elseif (!empty($request->data == 'dislike')) {
      $reactions = GroupForumReplyReaction::firstOrCreate([
          'user_id' => Auth::id(),
          'group_forum_id' => $userActivityId,
          'group_forum_reply_id' => $userActivityReplyId
      ]);
      if ($reactions->save()) {
          $reactions->update([
              'likes' => 0,
              'dislikes' => GroupForumReaction::REACTION
          ]);
      }
  }

  return [
      'likes' => GroupForumReplyReaction::where('group_forum_id', $userActivityId)
              ->where('forum_reply_id', $userActivityReplyId)->get()->sum('likes'),
      'dislikes' => GroupForumReplyReaction::where('group_forum_id', $userActivityId)
              ->where('forum_reply_id', $userActivityReplyId)->get()->sum('dislikes')
  ];
}


    // public function edit($id){
      
    //   $group = Group::where('id',$id)->first();
    //   $types = Group::getTypes();
    //   $users = User::where('id','!=',1)->get();


    //   return view('facilitator.groups.edit', ['group' => $group, 'types'=>$types, 'users'=>$users ]);

    // }


    // public function update(Request $request, $id){
    //   $request->validate([
    //     'title' => ['required'],
    //     'description' => ['required'],
    //     'type' => ['required'],
    //   ]);

    //   $group = Group::where('id',$id)->first();
    //   $group->title = $request->input('title');
    //   $group->description= $request->input('description');
    //   $group->type = $request->input('type');
    //   $group->created_by = Auth::user()->id;
      
    //   if (!is_null($request->file('image')))
    //   {
    //     $group->image = $this->Fileupload($request,'image','Groups');
    //   }

    //   $group->save();

    //   if (!is_null($request->input('users')))
    //   {
    //     $users = $request->input('users');
       
    //     GroupUser::where('group_id', $group->id)->delete();

    //     foreach($users as $user){
    //       $groupuser = new GroupUser();
    //       $groupuser->user_id = $user;
    //       $groupuser->group_id = $group->id;
    //       $groupuser->save();
   
    //     }

    //   }
      
    //   if ($group) {
    //     Toastr::success('Group successfully Updated.');
    //   } else {
    //     Toastr::error('There was a problem creating the Group. Please try again.');
    //   }
    //   return redirect()->route('group.index');
      

    // }

    // public function delete($id){

    //   Group::where('id',$id)->delete();
 
    //   Toastr::success('Group successfully Deleted.');
      
    //   return redirect()->route('group.index');
    //  }
   

  public function delete(){

  }

    public function Fileupload ($request,$file = 'file', $folder)
    {
            $upload = null;
            //create folder if none
            if (!File::exists($folder)) {
                    File::makeDirectory($folder, 0755, true);
            }
            //chec fo file
            if (!is_null($request->file($file))) {
                    $extension = $request->file($file)->getClientOriginalExtension();
                    $fileName = rand(11111, 99999).uniqid().'.' . $extension;
                    $upload = $request->file($file)->move($folder, $fileName);
                    if ($upload) {
                            $upload = $fileName;
                    } else {
                            $upload = null;
                    }
            }
            return $upload;
    }

}
