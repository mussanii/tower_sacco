<?php

namespace App\Http\Controllers\Front;

// use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PageRepository;
use App\Repositories\ResourceRepository;
use A17\Twill\Http\Controllers\Front\Controller;
use App\Models\Session as SessionMeeting;
use App\Models\Resource;
use App\Models\SessionResource;
use App\Models\User;
use App\Models\Event;
use App\Models\Blog;
use App\Models\ResourceType;
use App\Models\JobRole;
use App\Models\CourseCategory;
use App\Models\ResourceTheme;
use App\Models\Forum;
use App\Models\GeneralMessage;
use App\Models\ForumTopic;
use Carbon\Carbon;
use Auth;
use Validator;
use Mail;
use Redirect;
use Session;
use App;
use App\Models\Policy;
use App\Models\Translations\PolicyTranslation;
use Notification;
use App\Notifications\GeneralMessageSent;
use Spatie\GoogleCalendar\Event as Events; 
use Monarobase\CountryList\CountryListFacade as CountryList;

class PageController extends Controller
{
    //

    protected $pageKey;

    public function __construct()
    {
		$this->homeKey = PageRepository::PAGE_HOMEPAGE;

    $this->middleware('auth', ['except' => ['index','contactStore']]);
    parent::__construct();
	}


  public function homeLink(Request $request){
   

    $data = $request->all();
    if ($request->isMethod('post')) {
      if ($request->input('category')) {

        $type = $request->input('category');
     
      }

      if ($request->input('search')) {
        $search = $request->input('search');
        
      
      }

    }else{

      $type = '';
      $search = '';
    }

    $courseCategories = CourseCategory::with(['courses'=>function($query){
      //$query->where('company_id',Auth::user()->company_id);
   }])->published()->orderBy('position', 'asc')->get();
    $jobRoles = JobRole::published()->orderBy('position', 'asc')->get();
    $itemPage = app(PageRepository::class)->getPage($this->homeKey);
    $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
    $this->seo->canonical = $itemPage->seo_canonical;
    $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
    $this->seo->image = $itemPage->present()->imageSeo;

    return view('site.pages.home', [
      'pageItem' => $itemPage,
      'jobRoles' => $jobRoles,
      'jobRole' => $type,
      'search' => $search,
      'courseCategories'=>$courseCategories
    ]);
  }


	public function index(Request $request)
    {

      
      if ($request->isMethod('post')) {
       
      $data = $request->all();
     
        // $validator = Validator::make($data, [
        //   'category'    => 'required',
        //   ]);
         
        if ($request->input('category')) {
  
          $type = $request->input('category');
          
        }
  
        if ($request->input('search')) {
          $search = $request->input('search');
          
         
        }
  
      }else{
  
        $type = '';
        $search ='';
      }
      $courseCategories = CourseCategory::with(['courses'=>function($query){
        //$query->where('company_id',Auth::user()->company_id);
     }])->published()->orderBy('position', 'asc')->get();
  
      $jobRoles = JobRole::published()->orderBy('position', 'asc')->get();
      $itemPage = app(PageRepository::class)->getPage($this->homeKey);
      $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
      $this->seo->canonical = $itemPage->seo_canonical;
      $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
      $this->seo->image = $itemPage->present()->imageSeo;
  
      return view('site.pages.home', [
        'pageItem' => $itemPage,
        'jobRoles' => $jobRoles,
        'jobRole' => $type ?? null,
        'search' => $search ?? null,
        'courseCategories'=>$courseCategories
      ]);
    }



    public function pages(Request $request, $key){

   
      
         if($request->isMethod('post')){


             if($request->input('key') ==='groups'){
              $type = $request->input('search');
              $forums = Forum::latest()->get();
              $policies = Policy::published()->orderBy('position', 'asc')->get();
             }

            if($request->input('key') ==='our-progress'){
              $type = $request->input('branch');
              $forums = Forum::latest()->get();
              $policies = Policy::published()->orderBy('position', 'asc')->get();
             }
             
      if ($request->input('search')) {
        $search = $request->input('search');
      


        
        $forums = Forum::latest()->get();
        $policies = Policy::whereHas('translations', function ($query) use ($search) {
        $query->where('title', 'LIKE', '%' . $search . '%');
      })
      ->published()
      ->latest()
      ->get();

   
     

      }
      if($request->input('year'))
      {
        $year =$request->input('year');
        $policies = Policy::whereYear('created_at',$year)->get();


      
      }
            if($request->input('category')){

              $type = $request->input('category');
              $forums = Forum::latest()->get();
              $policies = Policy::published()->orderBy('position', 'asc')->get();
             }
             if($request->input('theme')){
              $policies = Policy::where('title', $request->input('theme'))->latest()->get();
          
            $forums = Forum::where('forum_topic',$request->input('theme'))->latest()->get();
          }
        }else{
            $forums = Forum::latest()->get();
            $policies = Policy::published()->orderBy('position', 'asc')->get();
            $type= '';
          
        }
     
   
       $types = Forum::getTypes();

      $jobRoles = JobRole::published()->orderBy('position', 'asc')->get();
      $topics = ForumTopic::all();
      $itemPage = app(PageRepository::class)->getPage($key);
      $courseCategories = CourseCategory::with(['courses'=>function($query){
        //$query->where('company_id',Auth::user()->company_id);
     }])->published()->orderBy('position', 'asc')->get();
      $this->seo->title = !empty($title=$itemPage->seo_title) ? $title : "{$itemPage->title}";
      $this->seo->canonical = $itemPage->seo_canonical;
      $this->seo->description = mb_substr($itemPage->present()->seo_description,0, 255);
      $this->seo->image = $itemPage->present()->imageSeo;

       if($key === 'home'){
          return redirect()->route('home');

       }else{
          return view('site.pages.'.$key,[
              'pageItem' => $itemPage,
              'jobRoles' => $jobRoles,
              'jobRole'=> $type ?? null,
              'topics'=>$topics,
              'types'=> $types,
              'forums'=>$forums ?? null,
              'policies'=>$policies ?? null,
              'courseCategories' => $courseCategories,
              'search'=> $search ?? null,

          ]);

       }
  }

  public function resourceDetails(Request $request, $id){
    $itemPage = Resource::where('id', '=', $id)->first();
    $related = Resource::where('content_bucket_id', '=', $itemPage->content_bucket_id)->latest()->take(2)->get(); 

      return view('site.pages.resources_details',[
        'pageItem' => $itemPage,
        'related' => $related,

        
      ]);
  }


  public function save_likedislike(Request $request){
    $data=new LikeDislike;
    $data->comment_id=$request->post;
    $data->user_id = Auth::user()->id;
    if($request->type=='like'){
        $data->like=1;
    }
    $data->save();
    return response()->json([
        'bool'=>true
    ]);
}


public function contactStore(Request $request)
{
    //Validate
    $request->validate([
        'name' => ['required', 'string', 'max:255'],
         'email' =>['required','email'],
        'phone' => ['required', 'string', 'max:255'],
        'subject' => ['required', 'string', 'max:255'],
        'comment' => ['required', 'string'],
    ]);

    //Get message
    $message = new GeneralMessage;
    $message->name = $request->input('name');
    $message->email = $request->input('email');
    $message->contact_phone = $request->input('phone');
    $message->subject = $request->input('subject');
    $message->contact_message = $request->input('comment');
    $message->save();

    //Send email
    $contact_emails = explode(',', env('CONTACT_EMAILS'));
    foreach ($contact_emails as $email) {
        Notification::route('mail', $email)
            ->notify(new GeneralMessageSent($message));
    }


    //Show session message
    $redirectMessage = [
        'title' => 'Message sent',
        'content' => 'Thank you for your enquiry. We will respond to you as soon as possible.',
        // 'action'=>'Google',
        // 'link'=>'google.com'
    ];
    Session::flash('course_success', $redirectMessage);
    return redirect()->back();
}

    protected function getRepository()
    {
        return App::make("App\\Repositories\\ResourceRepository");
	}


  public function sessionDetail(Request $request, $id)
  {
      $itemPage = SessionMeeting::where('id', '=', $id)->with('comments.replies')->first();
      $upcoming = SessionMeeting::where('start_time', '>=', Carbon::now())->first();
      $previous = SessionMeeting::where('start_time', '<', Carbon::today()->toDateString())->limit(1)->orderBy('position', 'desc')->get();
     

      // dd(count($articles));
      return view(
          'site.pages.session_details',
          [
          'pageItem' => $itemPage,
          'upcoming' => $upcoming,
          'previous' => $previous,
          ]
      );
  }


  public function policyDetail(Request $request, $id)
  {
   
      $policy = Policy::find($id);
   

      // dd(count($articles));
      return view(
          'site.pages.policy_details',
          [
          'policy' => $policy,
          
          ]
      );
  }


}
