<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Front\RegisterRequest;
use App\Http\Requests\Front\VerifyRequest;
use App\Http\Requests\Front\LoginRequest;
use Monarobase\CountryList\CountryListFacade as CountryList;
use App\Helpers\Front;
use App\Models\TwillUsersAuth;
use App\Models\User;
use App\Models\Projecttopic;
use App\Models\ProjectUser;
use Carbon\Carbon;
use AfricasTalking\SDK\AfricasTalking;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Ixudra\Curl\Facades\Curl;
use Redirect;

use App\Edx\EdxAuthUser;
use App\Models\JobRole;
use App\Models\Branch;

use Auth;
use Toastr;
use Session;
use Mail;
use App;



class UserController extends Controller
{
  //

  public function __construct()
  {
    $this->repository = $this->getRepository();
  }


  /**
   * @param RegisterRequest $request
   */

  public function showRegistrationForm()
  {


    return view('site.auth.register');
  }



  public function create(RegisterRequest $request)
  {

    return  App::environment(['local', 'staging']) ? $this->handleCreateLocal($request) : $this->handleCreateLive($request);
  }



  public function handleCreateLocal($request)
  {

    $code = array();
    $data = $request->validated();

    $user = $this->repository->createLocalForRegister($data);

    $validation = $this->validationToken($user, $length = 6);


    // Mail::send(
    //   'emails.token',
    //   ['name' => $user->first_name . ' ' . $user->last_name,  'code' => $validation->token],
    //   function ($message) use ($user) {
    //     $message->from('support@farwell-consultants.com', 'Welcome to Tower Sacco Academy');
    //     $message->to($user->email, $user->first_name);
    //     $message->subject('The Tower Sacco Academy Account Verification');
    //   }
    // );


    if (substr($user->phone, 0, 1) === '0') {
      $phone = substr_replace($user->phone, "254", 0, 1);
    } elseif (substr($user->phone, 0, 1) === '2') {

      $phone = $user->phone;
    } elseif (substr($user->phone, 0, 1) === '+') {

      $phone = substr_replace($user->phone, "", 0, 1);
    } else {
      $phone = "254" . $user->phone;
    }



    $response = Curl::to('https://restapi.uwaziimobile.com/v1/authorize')
      ->asJson()
      ->withHeaders(array('Content-Type' => 'application/json', 'Accept' => 'application/json'))
      ->withData([
        'username' => env('UWAZII_USERNAME'),
        'password' => env('UWAZII_PASSWORD'),
      ])
      ->post();
    $authorization_code = $response->data->authorization_code;
    $token = Curl::to('https://restapi.uwaziimobile.com/v1/accesstoken')
      ->asJson()
      ->withHeaders(array('Content-Type' => 'application/json', 'Accept' => 'application/json'))
      ->withData([
        'authorization_code' => $authorization_code,
      ])
      ->post();
    $access_token = $token->data->access_token;
    $message = 'Your verification code on Tower Sacco Academy  is: ' . $validation->token;

    $sms = Curl::to('https://restapi.uwaziimobile.com/v1/send')
      ->asJson()
      ->withHeaders(array('Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Access-Token' => $access_token))
      ->withData([[
        "number" => $phone,
        "senderID" => env('UWAZII_SMSFROM'),
        "text" => $message,
        "type" => "sms"
      ]])
      ->post();

    $userData = [
      'otp_requested'    => true,
      'otp_for_user' => $user->email,
      'user_id' => $user->id,
    ];
    //dd($user, $user->profile, $token, $notification);
    session($userData);

    return redirect('/user/verify');
  }



  public function handleCreateLive($request)
  {
    $configApp = config()->get("settings.app");
    $code = array();
    $data = $request->validated();



    $user = $this->repository->createForRegister($data);

    $edxResponse =  $this->edxRegister($user);

    if ($edxResponse !== true) {
      $user->delete();
      return Redirect::back()->with('register_error', $edxResponse[0]['user_message']);
    }
    $validation = $this->validationToken($user, $length = 6);


    Mail::send(
      'emails.token',
      ['name' => $user->first_name . ' ' . $user->last_name,  'code' => $validation->token],
      function ($message) use ($user) {
        $message->from('support@farwell-consultants.com', 'Welcome to Tower Sacco Academy');
        $message->to($user->email, $user->first_name);
        $message->subject('The Tower Sacco Academy Account Verification');
      }
    );


    if (substr($user->phone, 0, 1) === '0') {
      $phone = substr_replace($user->phone, "254", 0, 1);
    } elseif (substr($user->phone, 0, 1) === '2') {

      $phone = $user->phone;
    } elseif (substr($user->phone, 0, 1) === '+') {

      $phone = substr_replace($user->phone, "", 0, 1);
    } else {
      $phone = "254" . $user->phone;
    }

    $response = Curl::to('https://restapi.uwaziimobile.com/v1/authorize')
      ->asJson()
      ->withHeaders(array('Content-Type' => 'application/json', 'Accept' => 'application/json'))
      ->withData([
        'username' =>  $configApp['UWAZII_USERNAME'],
        'password' =>  $configApp['UWAZII_PASSWORD'],
      ])
      ->post();
    $authorization_code = $response->data->authorization_code;
    $token = Curl::to('https://restapi.uwaziimobile.com/v1/accesstoken')
      ->asJson()
      ->withHeaders(array('Content-Type' => 'application/json', 'Accept' => 'application/json'))
      ->withData([
        'authorization_code' => $authorization_code,
      ])
      ->post();
    $access_token = $token->data->access_token;
    $message = 'Your verification code on Tower Sacco Academy  is: ' . $validation->token;
    $sms = Curl::to('https://restapi.uwaziimobile.com/v1/send')
      ->asJson()
      ->withHeaders(array('Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Access-Token' => $access_token))
      ->withData([[
        "number" => $phone,
        "senderID" =>  $configApp['UWAZII_SMSFROM'],
        "text" => $message,
        "type" => "sms"
      ]])
      ->post();

    $userData = [
      'otp_requested'    => true,
      'otp_for_user' => $user->email,
      'user_id' => $user->id,
    ];
    //dd($user, $user->profile, $token, $notification);
    session($userData);

    return redirect('/user/verify');
  }

  /**
   * Verify Form
   */

  public function verifyForm()
  {

    if (request()->session()->get('otp_requested') == null) {
      abort(403, 'You are not allowed on this page');
    }
    $user_id = request()->session()->get('user_id');

    return view('site.auth.verify', [
      'user_id' => $user_id
    ]);
  }


  /**
   * @param VerifyRequest $request
   */
  public function verify(VerifyRequest $request)
  {
    $data = $request->validated();
    // $user = $this->repository->createForValidation($data);
    $verify = TwillUsersAuth::where('user_id', $data['user'])->where('token', $data['token'])->first();

    if (!empty($verify)) {
      $user = User::where('id', $verify->user_id)->first();

      if (!$user) {
        $course_error = [
          'title' => 'Error!',
          'content' => 'The user account does not exsist please register on the platform',
        ];
        Session::flash('course_errors', $course_error);
        return redirect('/');
      } else {
        if (!empty($user->activated_at)) {

          $course_success = [
            'title' => 'Account Activated!',
            'content' => 'You have successfully activated your account. 
                 Please note the programme facilitator will need to publish the account before you can login.
                 A notification will be sent to your registration phone number and email.',
          ];

          Session::flash('course_success', $course_success);

          return redirect('/');
        } else {

          $edxuser = EdxAuthUser::where('username', $user->username)->first();


          if ($edxuser === null || $edxuser->authRegistration->activation_key === null) {

            return redirect()->route('back')->with('login_error', 'There was a problem updating your account. Please try again later or report to support');
          }
          //Update edxuser names and active
          $edxuser->first_name = $user->first_name;
          $edxuser->last_name = $user->last_name;
          $edxuser->email = $user->email;
          $edxuser->is_active =  1;
          $edxuser->save();

          $user->activated_at = Carbon::now();
          $user->save();


          // session()->forget('otp_requested');
          // session()->forget('otp_for_user');

          $course_success = [
            'title' => 'Congratulations!',
            'content' => 'You have successfully activated your account. 
               Please note the programme administrator will need to publish the account before you can login.
               A notification will be sent to your registration email.',
          ];

          Session::flash('course_success', $course_success);

          return redirect()->route('login');
        }
      }
    } else {
      $course_error = [
        'title' => 'Error!',
        'content' => 'The verification code used does not exsist please proceed to the registration page and request for a new code',
      ];
      Session::flash('course_errors', $course_error);
      return redirect('/');
    }
  }


  public function resendCode(Request $request)
  {
    return view('site.auth.resend');
  }


  public function resendStore(Request $request)
  {
    $configApp = config()->get("settings.app");
    $user = User::where('email', $request->input('email'))->first();

    if ($user) {
      if (empty($user->activated_at)) {

        if (empty($user->activated_at)) {
          $verify = TwillUsersAuth::where('user_id', $user->id)->first();

          Mail::send(
            'emails.token',
            ['name' => $user->first_name . ' ' . $user->last_name,  'code' => $verify->token],
            function ($message) use ($user) {
              $message->from('support@farwell-consultants.com', 'Welcome to Tower Sacco Academy');
              $message->to($user->email, $user->first_name);
              $message->subject('The Tower Sacco Academy Account Verification');
            }
          );



          if (substr($user->phone, 0, 1) === '0') {
            $phone = substr_replace($user->phone, "254", 0, 1);
          } elseif (substr($user->phone, 0, 1) === '2') {

            $phone = $user->phone;
          } elseif (substr($user->phone, 0, 1) === '+') {

            $phone = substr_replace($user->phone, "", 0, 1);
          } else {
            $phone = "254" . $user->phone;
          }

          $response = Curl::to('https://restapi.uwaziimobile.com/v1/authorize')
            ->asJson()
            ->withHeaders(array('Content-Type' => 'application/json', 'Accept' => 'application/json'))
            ->withData([
              'username' =>  $configApp['UWAZII_USERNAME'],
              'password' =>  $configApp['UWAZII_PASSWORD'],
            ])
            ->post();
          $authorization_code = $response->data->authorization_code;
          $token = Curl::to('https://restapi.uwaziimobile.com/v1/accesstoken')
            ->asJson()
            ->withHeaders(array('Content-Type' => 'application/json', 'Accept' => 'application/json'))
            ->withData([
              'authorization_code' => $authorization_code,
            ])
            ->post();
          $access_token = $token->data->access_token;
          $message = 'Your verification code on Tower Sacco Academy  is: ' . $verify->token;
          $sms = Curl::to('https://restapi.uwaziimobile.com/v1/send')
            ->asJson()
            ->withHeaders(array('Content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Access-Token' => $access_token))
            ->withData([[
              "number" => $phone,
              "senderID" => env('UWAZII_SMSFROM'),
              "text" => $message,
              "type" => "sms"
            ]])
            ->post();


          $userData = [
            'otp_requested'    => true,
            'otp_for_user' => $user->email,
            'user_id' => $user->id,
          ];
          //dd($user, $user->profile, $token, $notification);
          session($userData);

          return redirect('/user/verify');
        } else {
          $course_success = [
            'title' => 'Congratulations!',
            'content' => 'You have successfully activated your account. 
                     Please note the programme administrator will need to publish the account before you can login.
                     A notification will be sent to your registration email once your account is published.',
          ];

          Session::flash('course_success', $course_success);

          return redirect()->route('login');
        }
      } else {
        $course_success = [
          'title' => 'Account Activated!',
          'content' => 'Your account is activated. 
                    Please proceed to login.',
        ];

        Session::flash('course_success', $course_success);
        return redirect()->route('login');
      }
    } else {

      $course_error = [
        'title' => 'Error!',
        'content' => 'The user account does not exsist please register on the platform',
      ];
      Session::flash('course_errors', $course_error);
      return redirect('/');
    }
  }


  public function profile(Request $request)
  {
    //Get user
    $user = Auth::user();

    //dd($user);
    if ($request->isMethod('post')) {

      //Validate
      $request->validate([
        'first_name' => ['required', 'string', 'max:255'],
        'last_name' => ['required', 'string', 'max:255'],
        'phone' => ['required', 'unique:twill_users,phone,' . $user->id]
      ]);



      //Get updates
      $updates = $request->all();


      //Save changes
      $user = User::find($user->id);
      if ($user->update($updates)) {
        return Redirect::back()->with('register_success', 'Your profile has been successfully updated.');
      } else {
        //Show session message

        return Redirect::back()->with('register_error', 'There was a problem updating your profile.Please refresh the page and try again.');
      }
    }

    $roles = JobRole::all();
    $branches = Branch::all();

    if (strpos(Auth::user()->email, 'noemail')) {
      $user->email = '';
    }

    return view('site.pages.user.profile', ['user' => $user, 'roles' => $roles, 'branches' => $branches]);
  }


  public function picture(Request $request)
  {
    if (!Auth::check()) {
      return response('Unauthorized', 403);
    }

    $user = Auth::user();

    // //Save to storage
    // $ppic = $request->file('ppic');
    // $extension = $ppic->getClientOriginalExtension();
    // $filename = $ppic->getFilename() . '.' . $extension;


    // Storage::disk('public')->put($filename,  File::get($ppic));

    // //Delete old file
    // Storage::disk('public')->delete($user->profile_pic);

    //Save to user
    $user->profile_pic = $this->Fileupload($request, 'ppic', 'uploads');
    if ($user->save()) {

      return Redirect::back()->with('register_success', 'Your profile photo has been successfully updated.');
    } else {
      return Redirect::back()->with('register_error', 'Profile photo updating failed. Please refresh the page and try again.');
    }
  }



  /**
   * Get the guard to be used during registration.
   *
   * @return \Illuminate\Contracts\Auth\StatefulGuard
   */
  protected function guard()
  {
    return Auth::guard();
  }

  /**
   * @return \A17\Twill\Repositories\ModuleRepository
   */
  protected function getRepository()
  {
    return App::make("App\\Repositories\\UserRepository");
  }

  protected function validationToken($user, $length)
  {

    $validation = new TwillUsersAuth();
    $validation->user_id  = $user->id;
    $validation->token = $this->generateRandomString($length);
    $validation->save();


    return $validation;
  }


  public function Fileupload($request, $file = 'file', $folder = null)
  {
    $upload = null;
    //create folder if none
    if (!File::exists($folder)) {
      File::makeDirectory($folder);
    }
    //chec fo file
    if (!is_null($request->file($file))) {
      $extension = $request->file($file)->getClientOriginalExtension();
      $fileName = rand(11111, 99999) . uniqid() . '.' . $extension;
      $upload = $request->file($file)->move($folder, $fileName);
      if ($upload) {
        $upload = $fileName;
      } else {
        $upload = null;
      }
    }
    return $upload;
  }
  private function edxRegister($user)
  {

    $configLms = config()->get("settings.lms.live");
    $configApp = config()->get("settings.app");


    //Validate user
    if ($user === null) {
      return;
    }
    //Package data to be sent
    if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
      $email = $user->email;
    } else {
      $email = $user->email . '@local.com';
    }
    $data = [
      'email' => $email,
      'name' => $user->first_name . ' ' . $user->last_name,
      'username' => $user->username,
      'honor_code' => 'true',
      'password' => request()->get('password'),
      'country' => 'KE',
      'terms_of_service' => 'true',
    ];

    $headers = array(
      'Content-Type' => 'application/x-www-form-urlencoded',
      'cache-control' => 'no-cache',
      'Referer' => env('APP_URL') . '/register',
    );

    $client = new \GuzzleHttp\Client();

    try {

      $response = $client->request('POST', $configLms['LMS_REGISTRATION_URL'], [
        'form_params' => $data,
        'headers' => $headers,
      ]);

      return true;
    } catch (\GuzzleHttp\Exception\ClientException $e) {

      $responseJson = $e->getResponse();
      $response = json_decode($responseJson->getBody()->getContents(), true);
      //Error, delete user
      $user->delete();

      //Delete password resets
      //PasswordReset::where('email', '=', $user->email)->delete();
      $errors = [];
      foreach ($response as $key => $error) {
        //Return error
        $errors[] = $error;
      }
      //echo "CATCH 1";

      return $errors[0];
    } catch (\Exception $e) {

      //Error, delete user
      $user->delete();
      //Delete password resets
      //PasswordReset::where('email', '=', $user->email)->delete();
      //echo "CATCH 2";
      //echo $e->getMessage();
      //die;
      return $e->getMessage();
    }
  }

  protected function generateRandomString($length)
  {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
  }
}
