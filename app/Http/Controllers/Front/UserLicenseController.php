<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserLicense;
use App\Models\Course;
use App\Models\MyCommunity;
use App\Models\User;
use App\Models\TrainingComment;
use App\Models\TrainingLike;
use App\Models\UserEvaluation;
use App\Models\Evaluation;
use App\Edx\StudentCourseEnrollment;
use Auth;
use Redirect;
use Session;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cookie;
use Symfony\Component\HttpFoundation\Response;
use PDF;
use A17\Twill\Models\Enums\UserRole;
use DB;
use App;
use App\Models\JobroleCourse;
use App\Models\CourseCompletion;
use App\Models\SerialNumber;

class UserLicenseController extends Controller
{
    //


    public function profile()
    {

        return  App::environment(['local', 'staging']) ? $this->profileLocal() : $this->profileLive();

    }


    public function profileLocal(){

        // if (!isset($_COOKIE['edinstancexid'])) {

        //     Auth::logout();
        //     return false;
        // }
        $compulsories = array();
          $complete = array();
          $scores = array();
          $max_score = array();

        $courses = Course::orderBy('name')->get();

        $future_courses = Course::where('start', '>', Carbon::today()->toDateString())->get();
        $compulsory = JobroleCourse::where('job_role_id','=',Auth::user()->job_role_id)->get();
        // dd($compulsory);
        $completed = CourseCompletion::where('user_id','=',Auth::user()->id)->get();
        $completed_complusory = CourseCompletion::where('user_id','=',Auth::user()->id)->get();

        $coursesid = [];
        foreach ($future_courses as $id => $course) {
            $coursesid[] = $course->id;
        }

        $user = User::where('email', Auth::user()->email)->first();

        $enrollments = UserLicense::where(['user_id' => $user->id])->get();

        foreach($enrollments as $enrollment){

             //dd($enrollment);

            $Ecourse = Course::where('id', '=', $enrollment->course_id)->first();

            if($Ecourse){

            $l = UserLicense::where(['course_id' => $Ecourse->id, 'user_id' => Auth::user()->id])->first();

            if(empty($l)){

                    $lic = new UserLicense();
                    $lic->course_id = $Ecourse->id;
                    $lic->user_id = Auth::user()->id;
                    $lic->enrolled_at = Carbon::now();
                    $lic->expired_at =  Carbon::now()->endOfYear()->toDateTimeString();
                    $lic->company_license_id = 1;
                    $lic->username = Auth::user()->first_name .' '. Auth::user()->last_name;
                    $lic->save();
            }
        }
        }
        //$licensed = UserLicense::where('user_id', Auth::user()->id)->delete();
        // $licensed = UserLicense::where('user_id', Auth::user()->id)->latest()->get();
        // dd($licensed);
        $user = Auth::user()->id;
        $licensed = UserLicense::leftJoin('course_completions', function($join) use ($user) {
            $join->on('user_licenses.course_id', '=', 'course_completions.course_id')
                ->where('course_completions.user_id', $user);
                    
        })
        
        ->select('user_licenses.*','course_completions.created_at as completion_date')
        ->where(function($query) use ($user) {
            $query->where('user_licenses.user_id', $user); 
            
        })->orderBy('course_completions.completion_date', 'desc')->get();
        // dd($licensed);
        // dd($licensed);
        $licenseid = [];
        foreach ($licensed as $key => $license) {
            if (isset($license->course->id)) {
                $licenseid[] = $license->course->id;
           
            } else {
                unset($licensed[$key]);
            }
        }

        $result = array_diff($licenseid, $coursesid);

        $upcoming = [];
        $licenses = [];
        foreach ($licensed as $key => $license) {
            if (in_array($license->course->id, $result)) {
                $licenses[] = $license;
            } else {
                $upcoming[] = $license;
            }
            if (!empty($_GET['year'])) {

                $timestamp = strtotime($license->created_at);

                $y = date('Y', $timestamp);


                if ($y != $_GET['year']) {


                    unset($licenses[$key]);
                }
            }

            if (!empty($_GET['month'])) {
                $timestamp = strtotime($license->created_at);

                $m = substr(date('m', $timestamp), 1);


                if ($m != $_GET['month']) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($_GET['status'])) {
                if ($license->status != $_GET['status']) {
                    unset($licenses[$key]);
                }
            }
        }



        foreach($compulsory as $comp){
            $compulsories[] = $comp->course_id;
           }
        foreach($completed as $compl){
             $complete[] = $compl->course_id;

        }

         foreach($completed_complusory as $key => $l){
           if(!in_array($l->course_id,$compulsories)){
             unset($completed_complusory[$key]);
           }
         }


         foreach($completed_complusory as $cc){
            $scores[] = $cc->score;
               $max_score[] = $cc->max_score;
          }

          $Found = array_diff($compulsories,$complete);
              //dd(count($Found));
          if(count($Found) > 0){
              $download = 0;
          }else{
              $download = 1;
          }

        $pass = 70;

        if(array_sum($max_score) == 0){
         $total = 0;
        }else{
         $total = (array_sum($scores)/ array_sum($max_score));
        }


        return view('site.pages.user_licenses.courses', ['licenses' => $licenses, 'coursess' => $courses, 'upcoming' => $upcoming,'compulsory'=> $compulsory, 'completed'=>$completed, 'pass'=>$pass, 'total'=>$total,'completed_complusory'=>$completed_complusory]);

    }







    public function profileLive(){
         if (!isset($_COOKIE['edinstancexid'])) {
            Auth::logout();

            $course_errors = [
                'title' => 'Sorry',
                'content' => 'The Session is no longer active. Please login again to proceed',
            ];

            Session::flash('course_errors', $course_errors);
            return redirect()->to('/');
        }
        $compulsories = array();
          $complete = array();
          $scores = array();
          $max_score = array();

        $courses = Course::orderBy('name')->get();

        $future_courses = Course::where('start', '>', Carbon::today()->toDateString())->get();
        $compulsory = JobroleCourse::where('job_role_id','=',Auth::user()->job_role_id)->get();
        $completed = CourseCompletion::where('user_id','=',Auth::user()->id)->get();
        $completed_complusory = CourseCompletion::where('user_id','=',Auth::user()->id)->get();

        $coursesid = [];
        foreach ($future_courses as $id => $course) {
            $coursesid[] = $course->id;
        }

        $user = EdxAuthUser::where('email', Auth::user()->email)->first();

        $enrollments = StudentCourseEnrollment::where(['user_id' => $user->id])->get();

        foreach($enrollments as $enrollment){
            $Ecourse = Course::where('course_id', '=', $enrollment->course_id)->first();

            if($Ecourse){

                $l = UserLicense::where(['course_id' => $Ecourse->id, 'user_id' => Auth::user()->id])->first();

                if(empty($l)){

                        $lic = new UserLicense();
                        $lic->course_id = $Ecourse->id;
                        $lic->user_id = Auth::user()->id;
                        $lic->enrolled_at = Carbon::now();
                        $lic->expired_at =  Carbon::now()->endOfYear()->toDateTimeString();
                        $lic->company_license_id = 1;
                        $lic->username = Auth::user()->first_name .' '. Auth::user()->last_name;
                        $lic->save();
                }

            }

        }
        //$licensed = UserLicense::where('user_id', Auth::user()->id)->delete();
        $user = Auth::user()->id;
        $licensed = UserLicense::leftJoin('course_completions', function($join) use ($user) {
            $join->on('user_licenses.course_id', '=', 'course_completions.course_id')
                ->where('course_completions.user_id', $user);
                    
        })
        
        ->select('user_licenses.*','course_completions.created_at as completion_date')
        ->where(function($query) use ($user) {
            $query->where('user_licenses.user_id', $user); 
            
        })->orderBy('course_completions.completion_date', 'desc')->get();

        $licenseid = [];
        foreach ($licensed as $key => $license) {
            if (isset($license->course->id)) {
                $licenseid[] = $license->course->id;
            } else {
                unset($licensed[$key]);
            }
        }

        $result = array_diff($licenseid, $coursesid);

        $upcoming = [];
        $licenses = [];
        foreach ($licensed as $key => $license) {
            if (in_array($license->course->id, $result)) {
                $licenses[] = $license;
            } else {
                $upcoming[] = $license;
            }
            if (!empty($_GET['year'])) {

                $timestamp = strtotime($license->created_at);

                $y = date('Y', $timestamp);

                if ($y != $_GET['year']) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($_GET['month'])) {
                $nmonth = date('m',strtotime($_GET['month']));
                $m = date('m',strtotime($license->created_at));

                if ($m != $nmonth) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($_GET['status'])) {
                if ($license->status != $_GET['status']) {
                    unset($licenses[$key]);
                }
            }
        }



        foreach($compulsory as $comp){
            $compulsories[] = $comp->course_id;
           }
        foreach($completed as $compl){
             $complete[] = $compl->course_id;

        }

         foreach($completed_complusory as $key => $l){
           if(!in_array($l->course_id,$compulsories)){
             unset($completed_complusory[$key]);
           }
         }


         foreach($completed_complusory as $cc){
            $scores[] = $cc->score;
               $max_score[] = $cc->max_score;
          }

          $Found = array_diff($compulsories,$complete);
              //dd(count($Found));
          if(count($Found) > 0){
              $download = 0;
          }else{
              $download = 1;
          }

        $pass = 70;

        if(array_sum($max_score) == 0){
         $total = 0;
        }else{
         $total = (array_sum($scores)/ array_sum($max_score));
        }


        return view('site.pages.user_licenses.courses', ['licenses' => $licenses, 'coursess' => $courses, 'upcoming' => $upcoming,'compulsory'=> $compulsory, 'completed'=>$completed, 'pass'=>$pass, 'total'=>$total,'completed_complusory'=>$completed_complusory]);
    }
    /**
     * Display a listing of achievements by the profile
     *
     * @return \Illuminate\Http\Response
     */
    public function achievements()
    {
        $future_courses = Course::where('start', '>', Carbon::today()->toDateString())->get();
        $coursesid = [];
        foreach ($future_courses as $id => $course) {
            $coursesid[] = $course->course_id;
        }
        $licensed = UserLicense::where('user_id', Auth::user()->id)->get();
        $licenseid = [];
        foreach ($licensed as $key => $license) {
            if (isset($license->course->course_id)) {
                $licenseid[] = $license->course->course_id;
            } else {
                unset($licensed[$key]);
            }
        }

        $result = array_diff($licenseid, $coursesid);

        $licenses = [];

        foreach ($licensed as $key => $license) {
            if (in_array($license->course->course_id, $result)) {
                $licenses[] = $license;
            } else {
                $upcoming[] = $license;
            }

            if (!empty($_GET['year'])) {
                if ($license->created_at->year != $_GET['year']) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($_GET['month'])) {
                if ($license->created_at->month != $_GET['month']) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($_GET['status'])) {
                if ($license->status != $_GET['status']) {
                    unset($licenses[$key]);
                }
            }
        }

        return view('site.pages.user_licenses.achievements', ['licenses' => $licenses]);
    }


    public function community(Request $request)
    {
         if ($request->query('search', false)) {

            $users = User::where(function ($query) {
                $query->where('first_name', 'LIKE', '%' . $_GET['search'] . '%')
                    ->orWhere('last_name', 'LIKE', '%' . $_GET['search'] . '%');
            })->get();

            $results = [];
            foreach($users as $user){
              $results[] = $user->id;
            }
            //dd($results);
            $licensed = UserLicense::all()->groupBy('user_id');

            $licenses = [];
            foreach ($licensed as $key => $license) {
              if(isset($license[0]->user->id)){
                if (in_array($license[0]->user_id, $results ))
                 {
                  $licenses[] = $license[0];
                 }

                }else{
                  unset($licensed[$key]);
                }
            }
        }else{

          $users = UserLicense::where('user_id', Auth::user()->id)->get();

          $courses = [];

          foreach($users as $us){
            $courses[] = $us->user_id;
          }

          $licensed = UserLicense::all()->groupBy('user_id');

          $licenses = [];
          foreach ($licensed as $key => $license) {
            if(isset($license[0]->user->id)){
              if (!in_array($license[0]->user_id, $courses ))
               {
                $licenses[] = $license[0];
               }

              }else{
                unset($licensed[$key]);
              }
          }
        }
         //dd($licensed);


          $req = MyCommunity::where('follow_id',Auth::user()->id)->where('status',1)->get();
          $receive = count($req);



         return view('site.pages.user_licenses.myCommunity', ['licenses' => $licenses,'receive' => $receive]);

    }

    public function communityParticipants()
    {
        $data = [];

        $countries =  DB::table('twill_users')->groupBy('phone_locale')->get();

        foreach ($countries as $country) {
            array_push($data, strtolower($country->phone_locale));
        }

        return $data;
    }


    public function communityCountries()
    {
        $data = [];

        $countries =  DB::table('twill_users')->groupBy('phone_locale')->get();

        foreach ($countries as $country) {
            array_push($data, strtolower($country->phone_locale));
        }

        return $data;
    }
    public function communityCountry($name)
    {
        foreach ((new \App\Helpers\Front())->getCountriesList() as $key => $country) {
            if ($country['name'] == $name) {
                $this->country = $key ;
            }
        }

        $countryFlag = Countries::where('postal', $this->country)->first();

        $country_flag =$countryFlag->flag['svg_path'];

        $flag = substr($country_flag, strpos($country_flag, "data") + 5);



        $licensed = User::where('phone_locale', '=', $this->country)->get();

        $req = MyCommunity::where('follow_id', Auth::user()->id)->where('status', 1)->get();

        $receive = count($req);

        return view('site.pages.connect.community_country_users', ['licenses' => $licensed,'receive' => $receive, 'name'=>$name, 'flag'=>$flag]);

        // dd( $this->country);




      //   $users = UserLicense::where('user_id', Auth::user()->id)->get();

      //   $courses = [];

      //   foreach($users as $us){
      //     $courses[] = $us->user_id;
      //   }

      //   $licensed = UserLicense::all()->groupBy('user_id');

      //   $licenses = [];
      //   foreach ($licensed as $key => $license) {
      //     if(isset($license[0]->user->id)){
      //       if (!in_array($license[0]->user_id, $courses ))
      //        {
      //         $licenses[] = $license[0];
      //        }

      //      if (!empty($_GET['year'])) {
      //             if ($license[0]->created_at->year != $_GET['year']) {
      //                 unset($licenses[$key]);
      //             }
      //         }

      //         if (!empty($_GET['month'])) {
      //             if ($license[0]->created_at->month != $_GET['month']) {
      //                 unset($licenses[$key]);
      //             }
      //         }

      //         if (!empty($_GET['status'])) {
      //             if ($license[0]->status != $_GET['status']) {
      //                 unset($licenses[$key]);
      //             }
      //         }
      //       }else{
      //         unset($licensed[$key]);
      //       }
      //   }

      //   $req = MyCommunity::where('follow_id',Auth::user()->id)->where('status',1)->get();

      //   $receive = count($req);

      //  return view('site.pages.user_licenses.myCommunity', ['licenses' => $licenses,'receive' => $receive]);
    }


    public function myFollow($follow, $id)
    {


        $status = MyCommunity::where('user_id', Auth::user()->id)->where('follow_id', $follow)->first();

        if ($status && $id == 0) {
            $follows = MyCommunity::where(['id' => $status->id])->first();
            $follows->user_id = Auth::user()->id;
            $follows->follow_id = $follow;
            $follows->status = 1;
            $follows->update();
        } elseif ($status && $id == 1) {
            $follo = MyCommunity::where(['id' => $status->id])->first();
            $follo->user_id = Auth::user()->id;
            $follo->follow_id = $follow;
            $follo->status = 2;
            $follo->update();
        } elseif ($status && $id == 2) {
            $foll = MyCommunity::where(['id' => $status->id])->first();
            $foll->user_id = Auth::user()->id;
            $foll->follow_id = $follow;
            $foll->status = 0;
            $foll->update();
        } elseif ($status && $id == 3) {
            $fol = MyCommunity::where(['id' => $status->id])->first();
            $fol->user_id = Auth::user()->id;
            $fol->follow_id = $follow;
            $fol->status = 3;
            $fol->update();
        } elseif ($status && $id == 4) {
            $fo = MyCommunity::where(['id' => $status->id])->first();
            $fo->user_id = Auth::user()->id;
            $fo->follow_id = $follow;
            $fo->status = 0;
            $fo->update();
        } else {
            $f = new MyCommunity();
            $f->user_id = Auth::user()->id;
            $f->follow_id = $follow;
            $f->status = 1;
            $f->save();
        }




        if($id == 0){
            $redirectMessage = [
                'title' => 'Success',
                'content' => 'Thank you for the follow request.',
            ];

        }else{
        $redirectMessage = [
            'title' => 'Success',
            'content' => 'Thank you for responding follow request.',
        ];
    }
         Session::flash('course_success', $redirectMessage);

        return Redirect::back();
    }


    public function myFollowRequests()
    {
        $details = [];
        $i = 0;
        $req = MyCommunity::where('follow_id', Auth::user()->id)->where('status', 1)->get();

        foreach ($req as $request) {
            $user = User::where('id', $request->user_id)->first();

            if (isset($user->id)) {
                $details[$i]['id'] = $request->id;
                $details[$i]['user_id'] = $user->id;
                $details[$i]['name'] = ucwords($user->first_name.' '.$user->last_name);
                $i++;
            }
        }
        return response()->json(['details' => $details], Response::HTTP_OK);
    }


    public function myFollowAccept($id, $status)
    {
        $following = MyCommunity::where('id', $id)->where('status', 1)->first();

        $following->status = $status;
        $following->update();

        if ($status == 2) {
            $redirectMessage = [
               'title' => 'Success',
               'content' => 'Thank you for accepting follow request.',
           ];

            Session::flash('course_success', $redirectMessage);
        } else {
            $redirectMessage = [
               'title' => 'Success',
               'content' => 'Thank you for responding follow request.',
           ];

            Session::flash('course_success', $redirectMessage);
        }


        return Redirect::back();
    }


    public function trainingFeed()
    {
        $following = MyCommunity::where('user_id', Auth::user()->id)->where('status', 2)->get();

        $folo = [];

        foreach ($following as $f) {
            $folo [] = $f->follow_id;
        }

        $licensed = UserLicense::all();

        $licenses = [];
        foreach ($licensed as $key => $license) {
            if (isset($license->user->id)) {
                if (in_array($license->user->id, $folo)) {
                    $licenses[] = $license;
                }
                if ($license->user->id == Auth::user()->id) {
                    unset($licenses[$key]);
                }
                if (!empty($_GET['year'])) {
                    if ($license->created_at->year != $_GET['year']) {
                        unset($licenses[$key]);
                    }
                }

                if (!empty($_GET['month'])) {
                    if ($license->created_at->month != $_GET['month']) {
                        unset($licenses[$key]);
                    }
                }

                if (!empty($_GET['status'])) {
                    if ($license->status != $_GET['status']) {
                        unset($licenses[$key]);
                    }
                }
            } else {
                unset($licensed[$key]);
            }
        }


        return view('site.pages.user_licenses.trainingfeed', ['licenses' => $licenses]);
    }




    public function myComment(Request $request)
    {

        if ($request->input('unique')) {
            $comment = TrainingComment::where('user_id', Auth::user()->id)->where('comment_id', $request->input('follow'))->where('course_id', $request->input('course'))->where('parent_id', $request->input('unique'))->first();
        } else {
            $comment = TrainingComment::where('user_id', Auth::user()->id)->where('comment_id', $request->input('follow'))->where('course_id', $request->input('course'))->first();
        }

        if ($comment) {
            $comments = TrainingComment::where('id', $comment->id)->first();
            $comments->user_id = Auth::user()->id;
            $comments->comment_id = $request->input('follow');
            $comments->course_id = $request->input('course');
            $comments->parent_id = $request->input('unique');
            $comments->comment = $request->input('comment');
            $comments->update();
        } else {
            $commen = new TrainingComment();
            $commen->user_id = Auth::user()->id;
            $commen->comment_id = $request->input('follow');
            $commen->course_id = $request->input('course');
            $commen->parent_id = $request->input('unique');
            $commen->comment = $request->input('comment');
            $commen->save();
        }

        $site_success = [
        'title' => '',
        'content' => 'Thank you for keeping the conversation going!',
    ];

        Session::flash('redirectMessage', $site_success);

        return Redirect::route('profile.trainingFeed');
    }

    public function myLike($follow, $id)
    {
        $likes = TrainingLike::where('user_id', Auth::user()->id)->where('like_id', $follow)->where('course_id', $id)->first();

        if ($likes) {
            $redirectMessage = [
            'title' => 'Thank you for like',
            'content' => 'You can only like a feed once.',
        ];

            Session::flash('redirectMessage', $redirectMessage);
            return Redirect::route('profile.trainingFeed');
        } else {
            $like = new TrainingLike();
            $like->user_id = Auth::user()->id;
            $like->like_id = $follow;
            $like->course_id = $id;
            $like->save();
        }

        return Redirect::route('profile.trainingFeed');
    }


    public function mycomments($follow, $course)
    {
        $details = [];
        $i = 0;
        $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $course)->whereNull('parent_id')->get();

        foreach ($comments as $comment) {
            $user = User::where('id', $comment->user_id)->first();
            $course = Course::where('id', $comment->course_id)->first();

            $details[$i]['id'] = $comment->id;
            $details[$i]['user_id'] = $user->id;
            $details[$i]['course_id'] = $comment->course_id;
            $details[$i]['name'] = ucwords($user->first_name.' '.$user->last_name);
            $details[$i]['position'] = $user->roles->title;
            $details[$i]['course_name'] = $course->name;
            $details[$i]['comment'] = $comment->comment;
            $details[$i]['created_at'] = $comment->created_at->format('M d Y H:i:s');
            $details[$i]['pic'] = $user->profile_pic;
            $i++;
        }
        return response()->json(['details' => $details], Response::HTTP_OK);
    }

    public function subCommentCount($follow, $course, $id)
    {
        $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $course)->where('parent_id', $id)->get();
        $count = count($comments);
        return response()->json(['count' => $count], Response::HTTP_OK);
    }


    public function subComment($follow, $course, $id)
    {
        $detail = [];
        $i = 0;
        $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $course)->where('parent_id', $id)->get();

        foreach ($comments as $comment) {
            $user = User::where('id', $comment->user_id)->first();
            $course = Course::where('id', $comment->course_id)->first();

            $detail[$i]['id'] = $comment->id;
            $detail[$i]['user_id'] = $user->id;
            $detail[$i]['course_id'] = $comment->course_id;
            $detail[$i]['name'] = ucwords($user->first_name.' '.$user->last_name);
            $detail[$i]['position'] = $user->roles->title;
            $detail[$i]['course_name'] = $course->name;
            $detail[$i]['comment'] = $comment->comment;
            $detail[$i]['created_at'] = $comment->created_at->format('M d Y H:i:s');
            $detail[$i]['pics'] = $user->profile_pic;
            $i++;
        }

        return response()->json(['subdetails' => $detail], Response::HTTP_OK);
    }



    public function myProgress()
    {
          $compulsories = array();
          $complete = array();
          $scores = array();
          $max_score = array();
    //       $compulsory = JobroleCourse::where('job_role_id','=',Auth::user()->role)->get();
    //       $completed = CourseCompletion::where('user_id','=',Auth::user()->id)->get();

    //        $completed_complusory = CourseCompletion::where('user_id','=',Auth::user()->id)->get();

    //        foreach($compulsory as $comp){
    //         $compulsories[] = $comp->course_id;
    //        }
    //       foreach($completed as $compl){
    //          $complete[] = $compl->course_id;

    //        }

    //      foreach($completed_complusory as $key => $l){
    //        if(!in_array($l->course_id,$compulsories)){
    //          unset($completed_complusory[$key]);
    //        }
    //      }

    //      foreach($completed_complusory as $cc){
    //        $scores[] = $compl->score;
    //        $max_score[] = $compl->max_score;
    //      }

    //      $Found = array_diff($compulsories,$complete);
    //          //dd(count($Found));
    //      if(count($Found) > 0){
    //          $download = 0;
    //      }else{
    //          $download = 1;
    //      }

    //    $pass = 0.8;
    //   if(array_sum($max_score) == 0){
    //       $total = 0;
    //      }else{
    //       $total = (array_sum($scores)/ array_sum($max_score));
    //      }

         $required = JobroleCourse::where('job_role_id','=', Auth::user()->job_role_id)->get();


         $compulsory = JobroleCourse::where('job_role_id','=',Auth::user()->job_role_id)->get();
         $completed = CourseCompletion::where('user_id','=',Auth::user()->id)->get();
         $completed_complusory = CourseCompletion::where('user_id','=',Auth::user()->id)->get();

         foreach($compulsory as $comp){
             $compulsories[] = $comp->course_id;
            }
         foreach($completed as $compl){
              $complete[] = $compl->course_id;

         }

          foreach($completed_complusory as $key => $l){
            if(!in_array($l->course_id,$compulsories)){
              unset($completed_complusory[$key]);
            }
          }


          foreach($completed_complusory as $cc){
             $scores[] = $cc->score;
                $max_score[] = $cc->max_score;
           }

           $Found = array_diff($compulsories,$complete);
               //dd(count($Found));
           if(count($Found) > 0){
               $download = 0;
           }else{
               $download = 1;
           }

         $pass = 0.8;



         if(array_sum($max_score) == 0){
          $total = 0;
         }else{
          $total = (array_sum($scores)/ array_sum($max_score));
         }


       return view('site.pages.user_licenses.myProgress', ['compulsory'=> $compulsory, 'completed'=>$completed, 'pass'=>$pass, 'total'=>$total,'completed_complusory'=>$completed_complusory, 'required' =>$required]);

    }





    public function evaluate($id){
        $sections = Evaluation::where('course_id',$id)->get();
        $evaluation = UserEvaluation::where('user_id','=',Auth::user()->id)->first();

        return view('site.pages.user_licenses.evaluate',['sections'=>$sections, 'evaluation'=>$evaluation, 'id'=>$id]);

      }


      public function Evaluationstore(Request $request){

        //dd($_POST);

      foreach ($_POST as $key => $value) {
         $keys[]=$key;
         $values [] = $value;
        }


        $userEvaluation = new UserEvaluation;
        $userEvaluation->user_id = Auth::user()->id;
        $userEvaluation->course_id = $request->input('course_id');
        $userEvaluation->question1 = $keys[2];
        $userEvaluation->answer1 = $values[2];
        $userEvaluation->question2 = $keys[3];
        $userEvaluation->answer2 = $values[3];
        $userEvaluation->question3 = $keys[4];
        $userEvaluation->answer3 = $values[4];
        $userEvaluation->question4 = $keys[5];
        $userEvaluation->answer4 = $values[5];
        $userEvaluation->question5 = $keys[6];
        $userEvaluation->answer5 = $values[6];
        if(!empty($keys[7])){
        $userEvaluation->question6 = $keys[7];
        $userEvaluation->answer6 = $values[7];
        }
        if(!empty($keys[8])){
          $userEvaluation->question7 = $keys[8];
          $userEvaluation->answer7 = $values[8];
        }
        if(!empty($keys[9])){
          $userEvaluation->question8 = $keys[9];
          $userEvaluation->answer8 = $values[9];
        }
        if(!empty($keys[10])){
          $userEvaluation->question9 = $keys[10];
          $userEvaluation->answer9 = $values[10];
        }
        if(!empty($keys[11])){
          $userEvaluation->question10 = $keys[11];
          $userEvaluation->answer10 = $values[11];
        }
        $userEvaluation->save();

        $redirectMessage = [
            'title' => 'Thank you for Evaluating the course',
            'content' => 'You can now download your certificate.',
        ];
        Session::flash('redirectMessage', $redirectMessage);
        return redirect()->route('profile.courses');

      }

    public function certificate($courseid)
    {
        // $user = EdxAuthUser::where('email', Auth::user()->email)->firstOrFail();
        $user = User::where('id', Auth::user()->id)->first();
        $c = Course::where('id', $courseid)->first();

        $name = ucfirst($user->first_name).' '.ucfirst($user->last_name);
        $date = Carbon::today()->isoFormat('Do MMMM YYYY');

        $number = SerialNumber::where('user_id','=', Auth::user()->id)->first();

        if($number){
           $cert_number = $number->serial_number ;
        }else{

           $serial = new SerialNumber();
           $serial->username = Auth::user()->username;
           $serial->user_id = Auth::user()->id;
           $serial->serial_number = self::generateRandomString(16);

           $serial->save();

           $cert_number = $serial->serial_number ;
        }


        $data = [
        'name' => $name,
        'date' => $date,
        'course_name' => $c->name,
        'role' => $user->roles->title,
        'cert_number' => $cert_number,
         ];


        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('site.pages.user_licenses.certificate', $data)->setPaper('a4', 'landscape');

        return $pdf->download('Certificate_'.$name.'.pdf');

        //return  $pdf->stream();
    }



    protected function generateRandomString($length = 10)
    {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[ rand(0, $charactersLength - 1) ];
      }

      return $randomString;
    }
}
