<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;

class DynamicDashboard
{
    public function handle(Request $request, Closure $next)
    {

		$superRole = ['SUPERADMIN'];
		$adminRoles = ['Admin'];
		$hrRoles = ['HR'];


		if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$superRole ) || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ))){
			config(['twill.dashboard' => adminDashboard()]);
		}

		if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$hrRoles ))){
			config(['twill.dashboard' => hrDashboard()]);
		}


        return $next($request);
    }
}
