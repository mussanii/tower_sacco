<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;

class DynamicMenu
{
    public function handle(Request $request, Closure $next)
    {

		$superRole = ['SUPERADMIN'];
		$adminRoles = ['Admin'];
        $hrRoles = ['HR'];
        $hodRoles = ['HOD'];
        $branchRoles = ['Branch Manager'];




		if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$superRole ) || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ))){
			config(['twill-navigation' => adminMenu()]);
		}

		if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$hrRoles ))){
			config(['twill-navigation' => hrMenu()]);
		}


        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$hodRoles ))){
			config(['twill-navigation' => hodMenu()]);
		}

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$branchRoles ))){
			config(['twill-navigation' => branchManagerMenu()]);
		}
        return $next($request);
    }
}
