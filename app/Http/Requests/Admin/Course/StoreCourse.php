<?php

namespace App\Http\Requests\Admin\Course;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreCourse extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.course.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['nullable', 'string'],
            'short_description' => ['nullable', 'string'],
            'overview' => ['nullable', 'string'],
            'more_info' => ['nullable', 'string'],
            'effort' => ['nullable', 'string'],
            'start' => ['nullable', 'date'],
            'end' => ['nullable', 'date'],
            'enrol_start' => ['nullable', 'date'],
            'enrol_end' => ['nullable', 'date'],
            'price' => ['required', 'numeric'],
            'course_image_uri' => ['nullable', 'string'],
            'course_video_uri' => ['nullable', 'string'],
            'job_group_id' => ['nullable', 'string'],
            'status' => ['required', 'boolean'],
            'course_category_id' => ['nullable', 'string'],
            'slug' => ['nullable', Rule::unique('courses', 'slug'), 'string'],
            'order_id' => ['nullable', 'integer'],
            'course_video' => ['nullable', 'string'],
            'sessions_id' => ['nullable', 'string'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
