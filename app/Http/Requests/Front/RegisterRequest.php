<?php

namespace App\Http\Requests\Front;

use A17\Twill\Http\Requests\Admin\Request;

class RegisterRequest extends Request
{
    public function rulesForCreate()
    {

  
            $rules = [
                'first_name' => ['required','max:255'],
                 'last_name' => ['required'],
                 'job_role'=>['required'],
                 'mobile_number'=>['required','unique:twill_users,phone'],
                 'password' => ['required', 'string', 'min:8','confirmed'],
                 'register_receive_information' => [],
                 'branch' => ['required'],
                 'registration_email' => ['required', 'unique:twill_users,email'],
              ];
           
        

        return $rules;
    }

    public function rulesForUpdate()
    {
        return [];
    }
}
