<?php

namespace App\Http\Requests\Front;

use A17\Twill\Http\Requests\Admin\Request;

class VerifyRequest extends Request
{
    public function rulesForCreate()
    {
        return [
          'user' => ['required'],
          'token' => ['required'],
        ];
    }

    public function rulesForUpdate()
    {
        return [];
    }
}
