<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    //

    protected $fillable = [
        'comment', 'user_id', 'blog_id','emoji'
    ];  

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id', 'id');
    }

    public function replies()
    {
        return $this->hasMany(BlogCommentReplies::class, 'comment_id')->orderBy('created_at','DESC');
    }

    //likes
    public function likes(){
        return $this->hasMany(BlogLikes::class ,'comment_id')->sum('like');
    }
    // Dislikes
    public function dislikes(){
        return $this->hasMany(BlogLikes::class ,'comment_id')->sum('dislike');
    }
}
