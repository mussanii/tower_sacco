<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{   
    protected $fillable = [
        'comment', 'user_id', 'session_meeting_id','emoji'
    ];   
     
    public function user()
    {
        return $this->belongsTo(User::class , 'user_id', 'id');
    }

    public function replies()
    {
        return $this->hasMany(CommentReply::class)->orderBy('created_at','DESC');
    }

    //likes
    public function likes(){
        return $this->hasMany('App\Models\LikeDislike','comment_id')->sum('like');
    }
    // Dislikes
    public function dislikes(){
        return $this->hasMany('App\Models\LikeDislike','comment_id')->sum('dislike');
    }
}