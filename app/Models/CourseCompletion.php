<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use Carbon\Carbon;

class CourseCompletion extends Model
{
    //
    use HasPosition;

    protected $fillable = ["course_id", "username", "user_id", "grade", "percent", "passed", "created_at","enrollment_date"];


    public function courses()
    {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function branches()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }

    public function getCompletionDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getEnrollmentDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }
    

    public function getCompletionStatusAttribute(){

        $completed = $this->checkCompletionsStatus($this->course_id, $this->user_id) ;
       
         return $completed['status'];
    
    
      }


   


   

      private function checkCompletionsStatus($course_id, $user_id){

       $completion = CourseCompletion::where('course_id', $course_id)->where('user_id', $user_id)->first();
   
       if($completion){
        
           if($completion->score >= 0.80){
   
               $completionStatus = 1;
               $status = "Completed";
   
           }else{
   
               $completionStatus = 2;
               $status = 'Completed but failed';
           }
       }else{
           $completionStatus = false;
           $status = 'In Progress';
       }
       
       return [
       'status' => $status,
       'completionStatus'=>$completionStatus
        ];
   
   }
}
