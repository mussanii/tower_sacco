<?php

namespace App\Models;


use A17\Twill\Models\Model;

class CourseReflection extends Model 
{
    

    protected $fillable = [
        'published',
        'title',
        'description',
        'user_id',
        'course_id',
        'username',
        'reflection'
    ];
    

    public function courses()
    {
        return $this->belongsTo(Course::class, 'course_id', 'course_id');
    }

}
