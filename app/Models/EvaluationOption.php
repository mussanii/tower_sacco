<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;


class EvaluationOption extends Model
{
    //

    protected $fillable = [
        'published',
        'title',
        'type',
        'value1',
        'value2',
        'value3',
        'value4',
        'value5',
    ];

    public function evaluation()
    {
        return $this->hasOne(Evaluation::class);
    }
}