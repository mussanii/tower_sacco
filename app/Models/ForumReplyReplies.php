<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumReplyReplies extends Model
{
    //
    use SoftDeletes;
    
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function saveFiles($files)
    {
        
        return collect($files)->map(function ($file) {
            $fileName = uniqid().$file->getClientOriginalName();
            $fileName = strtolower(str_replace(' ', '_', $fileName));
            $file->move('images/forumReplies', $fileName);
            return $fileName;
        });
    }
}
