<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupForumReply extends Model
{
    //
    use SoftDeletes;

    protected $guarded = [];

    public function replyReplies()
    {
        return $this->hasMany(GroupForumReplyReplies::class)->latest();
    }

    public function replyReactions()
    {
        return $this->hasMany(GroupForumReplyReaction::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
