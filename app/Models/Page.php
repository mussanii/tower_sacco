<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Page extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'key',
        'header_title',
        'variant',
		'description',
		'seo_title',
		'seo_description',
		'seo_canonical',
        'position',
    ];
    
    public $translatedAttributes = [
        'title',
        'header_title',
        'description',
        'url',
        'link_text',
		'seo_title',
		'seo_description',
		'seo_canonical',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];
 

    public $filesParams = ['slide_show'];
    
    protected $presenter = 'App\Presenters\Front\PagePresenter';

	protected $with = ['translations', 'medias','files'];
    
    public $mediasParams = [


        'hero_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    
                ]
            ]
        ]
    ];
}
