<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Block;
use A17\Twill\Models\Model;
use Share;
use willvincent\Rateable\Rateable;

class Policy extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition, Rateable;

    protected $fillable = [
        'published',
        'title',
        'description',
        'long_description',
        'position',
        'resource_type_id',
        'cover_image',
        'audio_file',
        'video_file',
        'downloable_file',
        'external_link',
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'long_description',
        'active',
    ];
    
    
    public $slugAttributes = [
        'title',
    ];

    public $filesParams = ['download_resource','resource_audio','resource_video'];
    


	protected $with = ['translations', 'medias','files'];
    protected $presenter = 'App\Presenters\Front\ResourcePresenter';

    protected $presenterAdmin = 'App\Presenters\Front\ResourcePresenter';
    
    public $mediasParams = [
        'resource_image' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];

    public function resourceDownloableBlocks()
    {
        // Adjust the relationship method according to your actual implementation
        return $this->blocks()->where('type', 'resource_downloable');
    }

    public function type()
    {
        return $this->hasOne(ResourceType::class,'id','resource_type_id');
    }

    public function getShareAttribute()
    {
        return Share::load(route('resource.details', $this->id), $this->name)->services('email','linkedin','facebook','twitter');
    }

    public function documents()
    {
        return $this->hasMany(PolicyDocument::class)->orderBy('position');
    }

  
}
