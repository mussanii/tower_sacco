<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class CourseRevision extends Revision
{
    protected $table = "course_revisions";
}
