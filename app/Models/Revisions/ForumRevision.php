<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class ForumRevision extends Revision
{
    protected $table = "forum_revisions";
}
