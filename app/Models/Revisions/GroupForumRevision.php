<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class GroupForumRevision extends Revision
{
    protected $table = "group_forum_revisions";
}
