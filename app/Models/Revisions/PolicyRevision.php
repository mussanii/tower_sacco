<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class PolicyRevision extends Revision
{
    protected $table = "policy_revisions";
}
