<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class RoleRevision extends Revision
{
    protected $table = "role_revisions";
}
