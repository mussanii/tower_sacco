<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SerialNumber extends Model
{
    //
    protected $fillable = [
        'username',
        'serial_number',
        'user_id',
    ];
}
