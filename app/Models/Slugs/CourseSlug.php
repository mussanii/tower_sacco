<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class CourseSlug extends Model
{
    protected $table = "course_slugs";
}
