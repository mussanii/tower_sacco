<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class FeatureWeekSlug extends Model
{
    protected $table = "feature_week_slugs";
}
