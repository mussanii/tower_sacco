<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class GroupTypeSlug extends Model
{
    protected $table = "group_type_slugs";
}
