<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class PlatformMessageSlug extends Model
{
    protected $table = "platform_message_slugs";
}
