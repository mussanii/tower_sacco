<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class ResourceSlug extends Model
{
    protected $table = "resource_slugs";
}
