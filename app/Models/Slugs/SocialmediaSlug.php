<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class SocialmediaSlug extends Model
{
    protected $table = "socialmedia_slugs";
}
