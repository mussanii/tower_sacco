<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Branch;

class BranchTranslation extends Model
{
    protected $baseModuleModel = Branch::class;
}
