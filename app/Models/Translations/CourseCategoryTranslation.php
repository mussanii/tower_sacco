<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\CourseCategory;

class CourseCategoryTranslation extends Model
{
    protected $baseModuleModel = CourseCategory::class;
}
