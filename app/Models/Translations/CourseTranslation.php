<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Course;

class CourseTranslation extends Model
{
    protected $baseModuleModel = Course::class;
}
