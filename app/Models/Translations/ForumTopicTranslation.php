<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\ForumTopic;

class ForumTopicTranslation extends Model
{
    protected $baseModuleModel = ForumTopic::class;
}
