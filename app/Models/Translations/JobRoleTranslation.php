<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\JobRole;

class JobRoleTranslation extends Model
{
    protected $baseModuleModel = JobRole::class;
}
