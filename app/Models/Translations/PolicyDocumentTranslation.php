<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\PolicyDocument;

class PolicyDocumentTranslation extends Model
{
    protected $baseModuleModel = PolicyDocument::class;
}
