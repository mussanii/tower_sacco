<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Policy;

class PolicyTranslation extends Model
{
    protected $baseModuleModel = Policy::class;
}
