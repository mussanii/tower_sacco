<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\ResourceDocument;

class ResourceDocumentTranslation extends Model
{
    protected $baseModuleModel = ResourceDocument::class;
}
