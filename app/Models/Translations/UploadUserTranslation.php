<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\UploadUser;

class UploadUserTranslation extends Model
{
    protected $baseModuleModel = UploadUser::class;
}
