<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TwillUsersAuth extends Model
{
    //

    protected $fillable = [
        'user_id',
        'token',
	];
}
