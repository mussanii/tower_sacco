<?php

namespace App\Models;

use A17\Twill\Models\User as TwillUser;

use App\Models\Domain;
use Illuminate\Support\Facades\Auth;

class User extends TwillUser
{
    protected $fillable = [
        // ...
        'name',
        'email',
        'password',
        'role',
        'username',
        'phone',
        'phone_locale',
        'is_activated',
        'last_login_at',
        'registered_at',
        'require_new_password',
         'company_id',
         'is_company_admin',
         'secondary_email',
        'platform_information',
        'profile_pic',
        'first_name',
        'last_name',
        'affiliation',
        'super_power',
        'contact_consent',
        'current_project',
        'branch_id',
        'job_role_id',

    ];


    public function resources()
    {
        return $this->hasMany(SessionResource::class, 'id', 'user_id');
    }


    public function roles()
    {
        return $this->belongsTo(JobRole::class, 'job_role_id', 'id');
    }

    public function branches()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }
   

    public function contentBuckets(){
       $content = array();
       $buckets = ContentBucketRole::where('role_id', Auth::user()->role)->get();

       foreach( $buckets as $bucket){
        $content[] = $bucket->content_bucket_id;
       }
    
        return $content;
    }


    public function getCommunityStatus($follow)
    {
        $status = MyCommunity::where('user_id', Auth::user()->id)->where('follow_id', $follow)->first();

        if (empty($status['status']) || $status['status'] == 0) {
            $status['status'] = 0;
        }

        return $status['status'];
    }


    public function getCommunityAction($follow)
    {
        $status = MyCommunity::where('user_id', Auth::user()->id)->where('follow_id', $follow)->first();

        if (empty($status['status']) || $status['status'] == 0) {
            $id = 0;
            return route('profile.myFollow', [$follow,$id]);
        } elseif ($status['status'] == 1 && $status['follow_id'] == Auth::user()->id) {
            $id = 1;
            return route('profile.myFollow', [$follow,$id]);
        } elseif ($status['status'] == 2) {
            $id = 2;
            return route('profile.myFollow', [$follow,$id]);
        }
    }

    public function getCommunityLink($follow)
    {
        $status = MyCommunity::where('user_id', Auth::user()->id)->where('follow_id', $follow)->first();

        if (empty($status['status']) || $status['status'] == 0) {
            $status['status'] = 0;
        }

        return $status['status'];
    }



    public function getMyFollowers($follow)
    {
        $status = MyCommunity::where('follow_id', $follow)->get();

        return $status;
    }


    public function getMyFollowing($follow)
    {
        $status = MyCommunity::where('user_id', $follow)->get();

        return $status;
    }

    public function getGroupStatus($group){

        $status = GroupUser::where('groups_id',$group)->where('twill_users_id',Auth::user()->id)->first();

        if(empty($status)){
            return '0';
        }else{
            return $status->joined;
        }


    }

    public function getForumStatus($group){

        $status = ForumUser::where('forum_id',$group)->where('user_id',Auth::user()->id)->first();

        if(empty($status)){
            return '0';
        }else{
            return $status->joined;
        }


    }

    public function activities()
    {
        return $this->hasMany(UserActivity::class);
    }

    public function scopeFilterUsers($query, $value)
    {
        return $query->where('name', '!=', Auth::user()->name)->where('name', 'like', "%{$value}%");
    }

    public function messages()
    {
        return $this->hasMany(UserMessages::class);
    }

    /**
     * Set Name Attribute
     *
     * @return string
     */
    public function setNameAttribute($name): string
    {
        return $this->attributes['name'] = $name;
    }

    /**
     * Get Name Attribute
     *
     * @return string
     */
    public function getNameAttribute($name): string
    {
        return ucwords($name);
    }


  

}
