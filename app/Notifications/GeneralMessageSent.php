<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\HtmlString;

class GeneralMessageSent extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        //
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject(Lang::get('New Enquiry for Tower Sacco E-Learning'))
        ->greeting(Lang::get(''))
        ->line(Lang::get('New Enquiry for Tower Sacco E-Learning'))
        ->line(new HtmlString('<b>Subject:</b> '.$this->message->subject))
        ->line(new HtmlString('<b>Message:</b> '.$this->message->contact_message))
        ->line(new HtmlString('<b>Sender Name:</b> '.$this->message->name))
        ->line(new HtmlString('<b>Sender Phone Number:</b> '.$this->message->contact_phone));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
