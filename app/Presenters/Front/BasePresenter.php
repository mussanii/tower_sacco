<?php

namespace App\Presenters\Front;

use App\Presenters\Presenter;

class BasePresenter extends Presenter
{
    public function blocks()
    {
        return $this->entity->blocks->where('parent_id', null);
    }

    public function shortTitle()
    {
        return $this->entity->short_title;
    }

    public function shortDescription()
    {
        return $this->entity->short_description ?? "";
    }

    public function getLink($locale = null)
    {
        return $this->url($locale);
    }

    public function url()
    {
        return $this->entity->url();
    }

    public function imageURL($role, $crop = "default", $params = [], $has_fallback =false)
    {
        return ($this->hasImage($role, $crop) || $has_fallback) ? $this->entity->image($role, $crop, $params, !$has_fallback) : null;
    }

    public function hasImage($role, $crop = "default")
    {
        return $this->getImageMedia($role, $crop) != null;
    }

    public function getImageMedia($role, $crop = "default")
    {
        return $this->entity->imageObject($role, $crop);
    }

    public function imageMedia($role, $crop = "default", $params = [], $has_fallback =false)
    {
        if (($src = $this->imageURL($role, $crop, $params, $has_fallback)) != null) {
            if (($media=$this->getImageMedia($role, $crop))==null) {
                $w = 1000;
                $h = 1000;
            } else {
                list($w,$h) = $this->mediaImageWidthHeight($media);
            }

            return (object)[
                'src' => $src,
                'width' => $w,
                'height' => $h,
                'crop_x' => $media->pivot->crop_x,
                'crop_y' => $media->pivot->crop_y,
                'ratio' => $media != null ? $this->mediaImageRatio($media, $this->imageDefaultRation($role, $crop, $media->pivot->ratio)) : 1,
                'orientation' => $media != null ? $this->mediaImageOrientation($media) : "",
                'caption' => $this->imageCaption($role, $crop),
                'altText' => $this->imageAltText($role, $crop),
                'mediaId' => $media != null ? $media->pivot->media_id : null,
                'lqip_data'  => $media != null ? $media->pivot->lqip_data : null,
            ];
        }

        return null;
    }

    public function imagesMedia($role, $crop = "default")
    {
        $medias = [];
        foreach ($this->entity->imageObjects($role, $crop) as $image) {
            list($width,$height) = $this->mediaImageWidthHeight($image);
            if (($src = $this->entity->image('', '', [], false, false, $image)) != null) {
                $medias[] = (object) [
                    'src' => $src,
                    'width' => $width,
                    'height' => $height,
                    'crop_x' => $media->pivot->crop_x,
                    'crop_y' => $media->pivot->crop_y,
                    'ratio' => $this->mediaImageRatio($image, $this->imageDefaultRation($role, $crop, $image->pivot->ratio)),
                    'orientation' => $this->mediaImageOrientation($image),
                    'caption' => $this->mediaImageCaption($image),
                    'altText' => $this->mediaImageAltText($image),
                    'mediaId' => $image->pivot->media_id
                ];
            }
        }
        return $medias;
    }

    public function imageLQIP($role, $crop = "default")
    {
        if (($media=$this->getImageMedia($role, $crop)) != null) {
            return $this->mediaImageLQIP($media);
        }
        return "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    }

    public function mediaImageLQIP($media)
    {
        if ($media->pivot->lqip_data != null && !empty($media->pivot->lqip_data)) {
            return $media->pivot->lqip_data;
        }
        return "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    }

    public function imageAltText($role, $crop = "default")
    {
        if (($media=$this->getImageMedia($role, $crop)) != null) {
            return $this->mediaImageAltText($media);
        }
        return $this->entity->imageAltText($role) ?? "";
    }

    public function mediaImageAltText($media)
    {
        $metadatas = json_decode($media->pivot->metadatas);
        $language = config('app.locale');
        return $metadatas->altText->$language ?? $media->alt_text ?? "";
    }

    public function imageCaption($role, $crop = "default")
    {
        if (($media=$this->getImageMedia($role, $crop)) != null) {
            return $this->mediaImageCaption($media);
        }
        return $this->entity->imageCaption($role) ?? "";
    }

    public function mediaImageCaption($media)
    {
        $metadatas = json_decode($media->pivot->metadatas);
        $language = config('app.locale');
        return $metadatas->caption->$language ?? $media->caption ?? "";
    }

    public function imageOrientation($role, $crop = "default")
    {
        if (($media=$this->getImageMedia($role, $crop)) != null) {
            return $this->mediaImageOrientation($media);
        }
        return "";
    }

    public function mediaImageOrientation($media)
    {
        if ($media->pivot != null && $media->pivot->crop_w != null) {
            return $media->pivot->crop_w == $media->pivot->crop_h ? 'square' : ($media->pivot->crop_w > $media->pivot->crop_h ? 'landscape' : 'portrait');
        }
        return $media->width == ($media->height ? 'square' : $media->width > $media->height ) ? 'landscape' : 'portrait';
    }

    public function imageWidthHeight($role, $crop = "default")
    {
        if (($media=$this->getImageMedia($role, $crop)) != null) {
            return $this->mediaImageWidthHeight($media);
        }
        return [0,0];
    }

    public function mediaImageWidthHeight($media)
    {
        if ($media->pivot != null && $media->pivot->crop_w != null) {
            return [$media->pivot->crop_w, $media->pivot->crop_h];
        }
        return [$media->width, $media->height];
    }

    public function imageRatio($role, $crop = "default")
    {
        if (($media=$this->getImageMedia($role, $crop)) != null) {
            return $this->mediaImageRatio($media, $this->imageDefaultRation($role, $crop, $media->pivot->ratio));
        }
        return 1;
    }

    public function imageDefaultRation($role, $crop = "default", $rationName = "default")
    {
        $ratio = 0;

        if (is_a($this->entity, 'A17\Twill\Models\Block')) {
            $mediasParams = config('twill.block_editor.crops');
        } else {
            $mediasParams = $this->entity->mediasParams;
        }

        if (isset($mediasParams[$role][$crop])) {
            foreach($mediasParams[$role][$crop] as $roleCrop){
                if ($roleCrop['name'] == $rationName) {
                    $ratio = $roleCrop['ratio'];
                    break;
                }
            }
        }
        return $ratio;
    }

    public function mediaImageRatio($media, $defaultRatio = 0)
    {
        if ($media->pivot != null && $media->pivot->crop_w != null) {
            if ($defaultRatio != 0 && $media->pivot->crop_h==$media->height
            && $media->pivot->crop_w==$media->width && $media->pivot->crop_y==0
            && $media->pivot->crop_x==0) {
                return 1/$defaultRatio;
            }
            return $media->pivot->crop_h / $media->pivot->crop_w;
        }
        return $defaultRatio != 0 ? 1/$defaultRatio : $media->height / $media->width;
    }

    protected function editionBrowserItems($repository, $key, $with =[])
    {
        return $repository->getItemsIdsOrdered(
            $this->entity->browserIds($key),
            [],
            $with
        );
    }

    public function translations()
    {
        $list = collect();
        foreach(getAlternateLanguages($this->entity->getAllActiveTranslations()) as $localeName => $url) {
            $lg = new class() {
                public $lang, $url;
            };
            $lg->lang = $localeName;
            $lg->url = $url;
            $list[] = $lg;
        }
        return $list;
    }
}
