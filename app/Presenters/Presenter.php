<?php

namespace App\Presenters;

use Carbon\Carbon;
use Illuminate\Support\Str;

abstract class Presenter
{

    protected $entity; // This is to store the original model instance

    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    // Call the function if that exists, or return the property on the original model
    public function __get($property)
    {
        if (method_exists($this, $property)) {
            return $this->{$property}();
        }

        $propertyCase = Str::camel($property);
        if (method_exists($this, $propertyCase)) {
            return $this->{$propertyCase}();
        }
        return $this->entity->{$property};
    }

    public function getImage($role, $crop, $dimensions, $has_fallback = false)
    {
        return $this->entity->image($role, $crop, $dimensions, $has_fallback);
    }
}