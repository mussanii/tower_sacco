<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\CourseCompletion;
use App\Models\User;
use App\Models\JobroleCourse;
use App\Models\Course;
use App\Models\UserLicense;
use DB;
use PDO;
use Auth;
use Carbon\Carbon;

class CourseCompletionRepository extends ModuleRepository
{


    public function __construct(CourseCompletion $model)
    {
        $this->model = $model;
    }

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $scopes
     * @return \Illuminate\Database\Query\Builder
     */
    public function filter($query, array $scopes = [])
    {


        $likeOperator = $this->getLikeOperator();

        foreach ($this->traitsMethods(__FUNCTION__) as $method) {
            $this->$method($query, $scopes);
        }

        unset($scopes['search']);

        if (isset($scopes['exceptIds'])) {
            $query->whereNotIn($this->model->getTable() . '.id', $scopes['exceptIds']);
            unset($scopes['exceptIds']);
        }
        
        // dd($scopes);
        foreach ($scopes as $column => $value) {

            if ($column === 'status_type') {

                if ($value === '1') {

                    $query->where('score', '>=', 0.8);
                } elseif ($value === '2') {

                    $query->whereBetween('score', [0.2, 0.79]);
                } elseif ($value === '3') {

                    $query->where('score', '<', 0.2);
                }
            } elseif ($column === 'completion_date') {

                $range = explode(' - ', $value);

                $query->whereBetween('completion_date', [Carbon::parse($range[0]), Carbon::parse($range[1])]);

                //$query->where('enrollment_date', 'LIKE', '%' . $value . '%');

            } elseif ($column === 'enrollment_date') {

                $range = explode(' - ', $value);

                $query->whereBetween('enrollment_date', [Carbon::parse($range[0]), Carbon::parse($range[1])]);

                //$query->where('enrollment_date', 'LIKE', '%' . $value . '%');

            } elseif($column === 'company_id' ){
                 
                
                $query->where('company_id', '=', $value);

            }elseif($column === 'branch_id' ){
                 
                if($value != " "){

                    $query->where('branch', '=', $value);
                }
               

            }elseif($column === 'job_role_id' ){
                 
                if($value != " "){

                    $query->where('job_role_id', '=', $value);
                }
               

            }
            
            
            
            else {
                if (method_exists($this->model, 'scope' . ucfirst($column))) {

                    $query->$column();
                } else {

                   
                    if (is_array($value)) {
                        $query->whereIn($column, $value);
                    } elseif ($column[0] == '%') {
                        $value && ($value[0] == '!') ? $query->where(substr($column, 1), "not $likeOperator", '%' . substr($value, 1) . '%') : $query->where(substr($column, 1), $likeOperator, '%' . $value . '%');
                    } elseif (isset($value[0]) && $value[0] == '!') {
                        $query->where($column, '<>', substr($value, 1));
                    } elseif ($value !== '') {
                        $query->where($column, $value);
                    }
                }
            }
        }

        return $query;
    }


    protected function getLikeOperator()
    {
        if (DB::connection()->getPDO()->getAttribute(PDO::ATTR_DRIVER_NAME) === 'pgsql') {
            return 'ILIKE';
        }

        return 'LIKE';
    }



    public function getCompleteRegistrations()
    {

        $registrations = array();

        $months = [
            'Jan' => '1', 'Feb' => '2', 'Mar' => '3', 'Apr' => '4', 'May' => '5', 'Jun' => '6',
            'Jul' => '7', 'Aug' => '8', 'Sep' => '9', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12'
        ];

        foreach ($months as $key => $value) {

            $c = User::published()->whereNotNull('job_role_id')->whereMonth('registered_at', $value)->whereYear('registered_at', '=', date('Y'))->get();

            $registrations[$key] = count($c);
        }


        $data = collect($registrations);

        return $data;
    }


    public function getPendingRegistrations()
    {

        $pending = array();

        $months = [
            'Jan' => '1', 'Feb' => '2', 'Mar' => '3', 'Apr' => '4', 'May' => '5', 'Jun' => '6',
            'Jul' => '7', 'Aug' => '8', 'Sep' => '9', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12'
        ];

        foreach ($months as $key => $value) {

            $c = User::published()->whereNull('job_role_id')->orWhere('job_role_id', '=', '')->whereMonth('registered_at', $value)->whereYear('registered_at', '=', date('Y'))->get();

            $pending[$key] = count($c);
        }
        $data = collect($pending);

        return $data;
    }


    public function getYearlyTopEnrollments($year,$launchYear)
    {
        $enrollments = array();
        $completed = UserLicense::whereYear('created_at', '>=', $launchYear)->whereYear('created_at', '<=', $year)
            ->selectRaw('id, course_id, count(*) as count')
            ->groupBy('course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();

        foreach ($completed as $key => $course) {

            $license =  UserLicense::where('course_id', $course->course_id)->get();

            $enrollments[] = [
                'name' => $course->course->name,
                'total' => count($license)
            ];
        }

        $data = collect($enrollments);
        return $data;
    }



    public function getYearlyTopCompletions($year,$launchYear)
    {
        $completions = array();
        $completed = CourseCompletion::whereYear('created_at', '>=', $launchYear)->whereYear('created_at', '<=', $year)->where('score', '>=', 0.80)
            ->selectRaw('id, course_id,score, count(*) as count')
            ->groupBy('course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();

        

        foreach ($completed as $key => $course) {
            $licenses =  CourseCompletion::where('course_id', $course->course_id)->get();
            $completions[] = [
                'name' => $course->courses->name,
                'total' => count($licenses)
            ];
        }
        $data = collect($completions);
        return $data;
    } 


    public function getTopEnrollments($month)
    {
        $enrollments = array();
        $courses = Course::where('published', 1)->get();
        foreach ($courses as $key => $course) {
            $userlicenses = UserLicense::where('course_id', $course->id)->whereMonth('created_at', '=', $month)->get();


            if (count($userlicenses) > 0) {
                $enrollments[] = [
                    'name' => $course->name,
                    'total' => count($userlicenses)
                ];
            } else {
                unset($courses[$key]);
            }
        }

        $data = collect($enrollments);
        return $data;
    }


    public function getTopCompletions($month)
    {
        $completions = array();
        $courses = Course::where('published', 1)->get();
        foreach ($courses as $key => $course) {
            $completed = CourseCompletion::where('course_id', '=', $course->id)->where('score', '>=', 0.80)->whereMonth('created_at', '=', $month)->get();

            if (count($completed) > 0) {
                $completions[] = [
                    'name' => $course->name,
                    'total' => count($completed)
                ];
            } else {
                unset($courses[$key]);
            }
        }
        $data = collect($completions);
        return $data;
    }


    public function getTopMonthEnrollments($month)
    {
        $enrollments = array();
        $courses = Course::where('published', 1)->get();
        foreach ($courses as $key => $course) {
            $userlicenses = UserLicense::where('course_id', $course->id)->whereMonth('created_at', '=', $month)->get();

            if (count($userlicenses) > 0) {
                $enrollments[] = [
                    'name' => $course->name,
                    'total' => count($userlicenses)
                ];
            } else {
                unset($courses[$key]);
            }
        }

        // $data = collect();
        return $enrollments;
    }


    public function getTopMonthCompletions($month)
    {
        $completions = array();
        $courses = Course::where('published', 1)->get();
        foreach ($courses as $key => $course) {
            $completed = CourseCompletion::where('course_id', '=', $course->id)->where('score', '>=', 0.80)->whereMonth('created_at', '=', $month)->get();

            if (count($completed) > 0) {
                $completions[] = [
                    'name' => $course->name,
                    'total' => count($completed)
                ];
            } else {
                unset($courses[$key]);
            }
        }
        // $data = collect($completions);
        return $completions;
    }

    
    public function getTopYearlyEnrollments($year)
    {

       
        $launchYear = "2022";
        $eyear = date('Y');
        // dd($launchYear);
        $enrollments = array();

        if($year == 0){
            $completed = UserLicense::whereYear('created_at', '>=', $launchYear)
            ->selectRaw('id, course_id, count(*) as count')
            ->groupBy('course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();
    
        }else{
            $completed = UserLicense::whereYear('created_at', '=', $launchYear)
            ->selectRaw('id, course_id,count(*) as count')
            ->groupBy('course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();

       

        }

        foreach ($completed as $key => $course) {

            $userlicenses = UserLicense::where('course_id', $course->course_id)->get();
           
            if (count($userlicenses) > 0) {
                $enrollments[] = [
                    'name' => $course->course->name,
                    'total' => count($userlicenses)
                ];
            } else {
                unset($completed[$key]);
            }
        }

       

        // $data = collect();
        return $enrollments;
    }


    public function getTopYearlyCompletions($year)
    {
       
        $launchYear = "2022";
        $eyear = date('Y');
      
        $completions = array();
        // dd($year);

        if($year == 0){

            // dd($year);
            $courses = CourseCompletion::whereYear('created_at', '>=', $launchYear)->where('score', '>=', 0.80)
            ->selectRaw('id, course_id,score, count(*) as count')
            ->groupBy('course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();

        }else{
            $courses = CourseCompletion::whereYear('created_at', '=', $launchYear)->where('score', '>=', 0.80)
            ->selectRaw('id, course_id,score, count(*) as count')
            ->groupBy('course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();

        }

       
       
        foreach ($courses as $key => $course) {
            $completed = CourseCompletion::where('course_id', '=', $course->course_id)->get();

            if (count($completed) > 0) {
                $completions[] = [
                    'name' => $course->courses->name,
                    'total' => count($completed)
                ];
            } else {
                unset($courses[$key]);
            }
        }
        
        // $data = collect($completions);
        return $completions;

        
    }


    public function getCompleteComplusoryModules()
    {
        $users = User::where('id', '!=', 1)->published()->get();
        $totalCount = $users->count();

        $completed = self::getCertified();

        //dd((count($completed)/$totalCount) * 100);


        $percentageComplete = (count($completed) / $totalCount) * 100;

        //$ditribution = ['incomplete'=>$percentageIncomplete, 'complete'=>(100 -$percentageIncomplete)];
        $ditribution = [
            'incomplete' => [
                'name' => 'Incomplete Compulsory',
                'value' => $percentageComplete,
                'total' => ($totalCount - count($completed)),
            ],
            'complete' => [
                'name' => 'Completed Compulsory',
                'value' => (100 - $percentageComplete),
                'total' => count($completed),

            ]
        ];
        $data = collect($ditribution);
        return $data;
    }




    public function getCompleteProfileGender()
    {
        $users = User::where('id', '!=', 1)->published()->get();
        $totalCount = $users->count();


        $female = User::where('id', '!=', 1)->where('gender_id', 1)->published()->get();
        $male = User::where('id', '!=', 1)->where('gender_id', 2)->published()->get();
        $undefined = User::where('id', '!=', 1)->where('gender_id', 3)->published()->get();

        $notundefined = User::where('id', '!=', 1)->where('gender_id', null)->published()->get();

        //dd((count($completed)/$totalCount) * 100);


        $femaleComplete = (count($female) / $totalCount) * 100;
        $maleComplete = (count($male) / $totalCount) * 100;
        $undefinedComplete = (count($undefined) / $totalCount) * 100;
        $notundefinedComplete = (count($notundefined) / $totalCount) * 100;

        //$ditribution = ['incomplete'=>$percentageIncomplete, 'complete'=>(100 -$percentageIncomplete)];
        $ditribution = [
            'female' => [
                'name' => 'Female Users',
                'value' => $femaleComplete,
                'total' => $female->count(),
            ],
            'male' => [
                'name' => 'Male Users',
                'value' => $maleComplete,
                'total' => $male->count(),

            ],

            'undefined' => [
                'name' => 'Undefined Users',
                'value' =>  $undefinedComplete,
                'total' =>  $undefined->count(),

            ],

          
        ];
        $data = collect($ditribution);
        return $data;
    }



    public static function getCertified()
    {

        $compulsories = array();
        $complete = array();
        $total = array();

        $users = User::where('published', '=', 1)->get();

        foreach ($users as $key => $user) {
            $compulsory = JobroleCourse::where('job_role_id', '=', $user->job_role_id)->get();
            $completed = CourseCompletion::where('user_id', '=', $user->id)->get();

            if (count($completed) > 0) {
                foreach ($compulsory as $comp) {
                    $compulsories[] = $comp->course_id;
                }
                foreach ($completed as $keys => $compl) {
                    if ($compl->score >= 0.80) {
                        $complete[] = $compl->course_id;
                    } else {
                        unset($compl[$keys]);
                    }
                }

                $difference = array_diff($compulsories, $complete);

                if (count($difference) > 0) {
                    unset($users[$key]);
                } else {

                    $total[] += 1;
                }
            } else {

                unset($users[$key]);
            }
        }

        return $total;
    }



    /** Get function for
     * Company Report with parameter*/


    public function getForCompany($with = [], $scopes = [], $orders = [], $perPage = 1000, $forcePagination = false)
    {
        $query = $this->model->where('company_id', Auth::user()->company_id)->with($with);

        $query = $this->filter($query, $scopes);
        $query = $this->order($query, $orders);

        if (!$forcePagination && $this->model instanceof Sortable) {
            return $query->ordered()->get();
        }

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    public function getCompanyCompleteProfileGender()
    {
        $users = User::where('id', '!=', 1)->published()->where('company_id', Auth::user()->company_id)->get();
        $totalCount = $users->count();


        $female = User::where('id', '!=', 1)->where('gender_id', 1)->where('company_id', Auth::user()->company_id)->published()->get();
        $male = User::where('id', '!=', 1)->where('gender_id', 2)->where('company_id', Auth::user()->company_id)->published()->get();
        $undefined = User::where('id', '!=', 1)->where('gender_id', 3)->where('company_id', Auth::user()->company_id)->published()->get();

        $notundefined = User::where('id', '!=', 1)->where('gender_id', null)->where('company_id', Auth::user()->company_id)->published()->get();

        //dd((count($completed)/$totalCount) * 100);


        $femaleComplete = (count($female) / $totalCount) * 100;
        $maleComplete = (count($male) / $totalCount) * 100;
        $undefinedComplete = (count($undefined) / $totalCount) * 100;
        $notundefinedComplete = (count($notundefined) / $totalCount) * 100;

        //$ditribution = ['incomplete'=>$percentageIncomplete, 'complete'=>(100 -$percentageIncomplete)];
        $ditribution = [
            'female' => [
                'name' => 'Female Users',
                'value' => $femaleComplete,
                'total' => $female->count(),
            ],
            'male' => [
                'name' => 'Male Users',
                'value' => $maleComplete,
                'total' => $male->count(),

            ],

            'undefined' => [
                'name' => 'Undefined Users',
                'value' =>  $undefinedComplete,
                'total' =>  $undefined->count(),

            ],

        ];
        $data = collect($ditribution);
        return $data;
    }


    public function getCompanyCompleteComplusoryModules()
    {
        $users = User::where('id', '!=', 1)->where('company_id', Auth::user()->company_id)->published()->get();
        $totalCount = $users->count();

        $completed = self::getCompanyCertified();

        //dd((count($completed)/$totalCount) * 100);


        $percentageComplete = (count($completed) / $totalCount) * 100;

        //$ditribution = ['incomplete'=>$percentageIncomplete, 'complete'=>(100 -$percentageIncomplete)];
        $ditribution = [
            'incomplete' => [
                'name' => 'Incomplete Compulsory',
                'value' => $percentageComplete,
                'total' => ($totalCount - count($completed)),
            ],
            'complete' => [
                'name' => 'Completed Compulsory',
                'value' => (100 - $percentageComplete),
                'total' => count($completed),

            ]
        ];
        $data = collect($ditribution);
        return $data;
    }


    public static function getCompanyCertified()
    {

        $compulsories = array();
        $complete = array();
        $total = array();

        $users = User::where('published', '=', 1)->get();

        foreach ($users as $key => $user) {
            $compulsory = JobroleCourse::where('job_role_id', '=', $user->job_role_id)->get();
            $completed = CourseCompletion::where('user_id', '=', $user->id)->where('company_id', Auth::user()->company_id)->get();

            if (count($completed) > 0) {
                foreach ($compulsory as $comp) {
                    $compulsories[] = $comp->course_id;
                }
                foreach ($completed as $keys => $compl) {
                    if ($compl->score >= 0.80) {
                        $complete[] = $compl->course_id;
                    } else {
                        unset($compl[$keys]);
                    }
                }

                $difference = array_diff($compulsories, $complete);

                if (count($difference) > 0) {
                    unset($users[$key]);
                } else {

                    $total[] += 1;
                }
            } else {

                unset($users[$key]);
            }
        }

        return $total;
    }



    public function getYearlyCompanyTopEnrollments($year)
    {

        $enrollments = array();

        $completed = CourseCompletion::whereYear('created_at', '=', $year)
             ->where('company_id', Auth::user()->company_id)
            ->selectRaw('id, course_id,score, count(*) as count')
            ->groupBy('course_id')
            ->orderBy('course_id', 'desc')
            ->take(5)
            ->get();

        foreach ($completed as $key => $course) {

            $license =  UserLicense::where('course_id', $course->course_id)->get();

            $enrollments[] = [
                'name' => $course->courses->name,
                'total' => count($license)
            ];
        }

        $data = collect($enrollments);
        return $data;
       
    }


    public function getYearlyCompanyTopCompletions($year)
    {
        $completions = array();
        $completed = CourseCompletion::whereYear('created_at', '=', $year)
            ->where('company_id', Auth::user()->company_id)
            ->selectRaw('id, course_id,score, count(*) as count')
            ->groupBy('course_id')
            ->orderBy('course_id', 'desc')
            ->take(5)
            ->get();


        foreach ($completed as $key => $course) {
            $licenses =  CourseCompletion::where('course_id', $course->course_id)->where('score', '>=', 0.80)->get();
            $completions[] = [
                'name' => $course->courses->name,
                'total' => count($licenses)
            ];
        }
        $data = collect($completions);
        return $data;
    }

    public function getTopCompanyEnrollments($month)
    {
        $enrollments = array();
        $courses = Course::where('published', 1)->get();
        foreach ($courses as $key => $course) {
            $userlicenses = UserLicense::where('course_id', $course->id)->whereMonth('created_at', '=', $month)->where('company_license_id', Auth::user()->company_id)->get();


            if (count($userlicenses) > 0) {
                $enrollments[] = [
                    'name' => $course->name,
                    'total' => count($userlicenses)
                ];
            } else {
                unset($courses[$key]);
            }
        }

        $data = collect($enrollments);
        return $data;
    }


    public function getTopCompanyCompletions($month)
    {
        $completions = array();
        $courses = Course::where('published', 1)->get();
        foreach ($courses as $key => $course) {
            $completed = CourseCompletion::where('course_id', '=', $course->id)->where('score', '>=', 0.80)->whereMonth('created_at', '=', $month)->where('company_id', Auth::user()->company_id)->get();

            if (count($completed) > 0) {
                $completions[] = [
                    'name' => $course->name,
                    'total' => count($completed)
                ];
            } else {
                unset($courses[$key]);
            }
        }
        $data = collect($completions);
        return $data;
    }


    public function getCompanyCompleteRegistrations()
    {

        $registrations = array();

        $months = [
            'Jan' => '1', 'Feb' => '2', 'Mar' => '3', 'Apr' => '4', 'May' => '5', 'Jun' => '6',
            'Jul' => '7', 'Aug' => '8', 'Sep' => '9', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12'
        ];

        foreach ($months as $key => $value) {

            $c = User::published()->whereNotNull('job_role_id')->whereMonth('registered_at', $value)->whereYear('registered_at', '=', date('Y'))->where('company_id', Auth::user()->company_id)->get();

            $registrations[$key] = count($c);
        }


        $data = collect($registrations);

        return $data;
    }


    public function getCompanyPendingRegistrations()
    {

        $pending = array();

        $months = [
            'Jan' => '1', 'Feb' => '2', 'Mar' => '3', 'Apr' => '4', 'May' => '5', 'Jun' => '6',
            'Jul' => '7', 'Aug' => '8', 'Sep' => '9', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12'
        ];

        foreach ($months as $key => $value) {

            $c = User::published()->whereNull('job_role_id')->orWhere('job_role_id', '=', '')->whereMonth('registered_at', $value)->whereYear('registered_at', '=', date('Y'))->where('company_id', Auth::user()->company_id)->get();

            $pending[$key] = count($c);
        }
        $data = collect($pending);

        return $data;
    }


    public function getTopCompanyYearlyEnrollments($year)
    {

       
        $enrollments = array();
        $completed = CourseCompletion::whereYear('created_at', '=', $year)
        ->where('company_id', Auth::user()->company_id)
            ->selectRaw('id, course_id,score, count(*) as count')
            ->groupBy('course_id')
            ->orderBy('course_id', 'asc')
            ->take(5)
            ->get();

       

        foreach ($completed as $key => $course) {

            $userlicenses = UserLicense::where('course_id', $course->course_id)->whereYear('created_at', '=', $year)->get();
           
            if (count($userlicenses) > 0) {
                $enrollments[] = [
                    'name' => $course->courses->name,
                    'total' => count($userlicenses)
                ];
            } else {
                unset($completed[$key]);
            }
        }

       

        // $data = collect();
        return $enrollments;
    }


    public function getTopCompanyYearlyCompletions($year)
    {
        $completions = array();
       
        $courses = CourseCompletion::whereYear('created_at', '=', $year)
        ->where('company_id', Auth::user()->company_id)
        ->selectRaw('id, course_id,score, count(*) as count')
        ->groupBy('course_id')
        ->orderBy('course_id', 'asc')
        ->take(5)
        ->get();

        foreach ($courses as $key => $course) {
            $completed = CourseCompletion::where('course_id', '=', $course->course_id)->where('score', '>=', 0.80)->whereYear('created_at', '=', $year)->get();

            if (count($completed) > 0) {
                $completions[] = [
                    'name' => $course->courses->name,
                    'total' => count($completed)
                ];
            } else {
                unset($courses[$key]);
            }
        }
       
        // $data = collect($completions);
        return $completions;
    }




    /** Get function for
     * Branch Report with parameter*/


    public function getForBranch($with = [], $scopes = [], $orders = [], $perPage = 1000, $forcePagination = false)
    {
        $query = $this->model->where('branch', Auth::user()->branch_id)->where('company_id', Auth::user()->company_id)->with($with);

        $query = $this->filter($query, $scopes);
        $query = $this->order($query, $orders);

        if (!$forcePagination && $this->model instanceof Sortable) {
            return $query->ordered()->get();
        }

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }



    /** Get function for
     * Job Role Report with parameter*/

    public function getForJobRole($with = [], $scopes = [], $orders = [], $perPage = 1000, $forcePagination = false)
    {
        $query = $this->model->where('job_role_id', Auth::user()->job_role_id)->where('branch', Auth::user()->branch_id)->where('company_id', Auth::user()->company_id)->with($with);

        $query = $this->filter($query, $scopes);
        $query = $this->order($query, $orders);

        if (!$forcePagination && $this->model instanceof Sortable) {
            return $query->ordered()->get();
        }

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    
    public function getFilterValues(){

        // dd(Auth::user());

        $filters= [
             'company_id'=> 'company_id',
             'branch_id' => 'branch_id',
             'job_role_id' => 'job_role_id',
             'course_id' => 'course_id',
             'status_type'=> 'status_type',
             'enrollment_date'=> 'enrollment_date',
             'completion_date'=> 'completion_date',
         ];
     
     return $filters;
 
     }


}
