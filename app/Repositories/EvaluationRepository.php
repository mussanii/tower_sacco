<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\Evaluation;

class EvaluationRepository extends ModuleRepository
{
    

    public function __construct(Evaluation $model)
    {
        $this->model = $model;
    }
}
