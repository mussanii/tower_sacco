<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Group;
use App\Models\Notification;
use App\Models\GroupUser;
use App\Models\User;
use Mail;

class GroupRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleRevisions;

    public function __construct(Group $model)
    {
        $this->model = $model;
    }

    public function afterSave($object, $fields)
{
  
    if(empty($object->user)){

    }else{
        $name = $object->user->first_name . ' ' . $object->user->last_name;

        $message = [
         'type'=> Notification::GROUP,
         'subject'=> 'Group Invite',
         'model_id'=>$object['id'],
         'message' => 'A group titled <b>'. $object['title'] .'</b> has been created to on the platform by '.$name.'<br> In order to accept the invite please click the link below <a href="'.route("group.activate",["id"=> $object['id']]).'" class="btn-drk-left" style=" background: #F87522;padding: 10px 15px;color: #fff;font-size: 18px;text-decoration: none;"> Accept Invite</a>
         <a href="'.route("group.decline",["id"=> $object['id']]).'" class="btn-drk-left" style=" background: #F2203E;padding: 10px 15px;color: #fff;font-size: 18px;text-decoration: none;"> Decline Invite</a>
         </p>',
        ];

          $groupus = new GroupUser();
          $groupus->twill_users_id = $object['created_by'];
          $groupus->groups_id = $object['id'];
          $groupus->joined = 1;
          $groupus->save();
   
        foreach($fields['users'] as $user){
          $groupuser = new GroupUser();
          $groupuser->twill_users_id = $user;
          $groupuser->groups_id = $object['id'];
          $groupuser->joined = 3;
          $groupuser->save();
   
   
          $users = User::where('id',$user)->first();
   
          $users->notify(new \App\Notifications\NewGroupUserNotification($message));
   
          Mail::send('emails.group_invite_email',['name' => $users->first_name.' '.$users->last_name, 'creater' => $name, 'title'=> $object['title'], 'url' => route('login'),'accept_url' => route("group.activate",["id"=> $object['id']]), 'decline_url' => route("group.decline",["id"=> $object['id']]) ],
          function ($message) use ($users) {
            $message->from('support@farwell-consultants.com', 'The Challenging Patriarchy Program');
            $message->to($users->email, $users->first_name);
            $message->subject('The Challenging Patriarchy Program Group Invite');
          });
   
        }
    }
  

     parent::afterSave($object, $fields);
}

}
