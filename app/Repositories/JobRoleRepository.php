<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use Illuminate\Support\Arr;
use App\Models\JobRole;
use App\Models\JobroleCourse;
use App\Models\Course;
use App\Models\User;
use DB;
use Mail;

class JobRoleRepository extends ModuleRepository
{
  use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleRevisions;

  public function __construct(JobRole $model)
  {
    $this->model = $model;
  }

  public function update($id, $fields)
  {

    DB::transaction(function () use ($id, $fields) {



      $object = $this->model->findOrFail($id);

      $this->beforeSave($object, $fields);

      $fields = $this->prepareFieldsBeforeSave($object, $fields);

      $object->fill(Arr::except($fields, $this->getReservedFields()));

      $object->save();

      if ($fields['courses']) {
        $previous = array();
        $new = array();
        $course_name = array();
        // delete records with job role 
        $exsisting = JobroleCourse::where('job_role_id', '=', $object->id)->get();

        //set exsisting courses in an array

        foreach ($exsisting as $e) {
          $previous[] = $e->course_id;
        }

        foreach ($fields['courses'] as $course) {
          $new[] = $course;
        }


        JobroleCourse::where('job_role_id', '=', $object->id)->delete();

        // Loop through the courses array 
        foreach ($fields['courses'] as $course) {

          //Save records to relationship table

          $job = new JobroleCourse();
          $job->job_role_id = $object->id;
          $job->course_id = $course;
          $job->save();
        }

        $result = array_diff($new, $previous);

        if (count($result) > 0) {
          foreach ($result as $key => $value) {
            $c = Course::where('id', $value)->first();
            $course_name[] = $c->name;
          }

          $users = User::where('job_role_id', $object->id)->get();

          foreach ($users as $user) {

            Mail::send(
              'emails.new_course',
              ['name' => $user->first_name . ' ' . $user->last_name,  'course_name' => $course_name, 'role' => $object->title, 'url' => route('login')],
              function ($message) use ($user) {
                $message->from('toweracademy@towersacco.co.ke', 'Tower Sacco Academy');
                $message->to($user->email, $user->first_name);
                $message->subject('The Tower Sacco Academy New Course Notification');
              }
            );
          }
        }
      }

      $this->afterSave($object, $fields);
    }, 3);
  }
}
