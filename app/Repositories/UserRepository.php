<?php

namespace App\Repositories;
use A17\Twill\Repositories\UserRepository as TwillUserRepository;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use A17\Twill\Models\Group;
use App;
use TaylorNetwork\UsernameGenerator\Generator;
use App\Models\Domain;
use App\Models\TwillUsersAuth;
use A17\Twill\Models\Enums\UserRole;
use App\Edx\EdxAuthUser;

class UserRepository extends TwillUserRepository
{

    public function __construct(User $model)
    {
        $this->model = $model;
	}

	    /**
     * @param string[] $fields
     * @return \A17\Twill\Models\Model
     */
    public function createForRegister($fields)
    {

		return DB::transaction(function () use ($fields) {
            $original_fields = $fields;

            $fields = $this->prepareFieldsBeforeCreate($fields);

            $generator = new Generator([ 'separator' => '_' ]);
            $username = $generator->generate($fields['first_name'] . ' ' . $fields['last_name']);

            
            $usern = EdxAuthUser::where('username', $username)->first();
            
            if(!empty($usern)){
              $username = $username.'_'.rand(0,7);
            }else{
              $username = $username;
            }
           
             
            $object = $this->model->create([
				        'name' => $fields['first_name'] . ' ' . $fields['last_name'],
				        'email' => $fields['registration_email'],
                'role' =>  UserRole::Learner,
                'username'=> $username,
                'password' => Hash::make($fields['password']),
                'company_id' => 1,
                'branch_id' => $fields['branch'],
                'job_role_id' => $fields['job_role'],
                'phone' => $fields['mobile_number'],
                'phone_locale' =>  'KE',
                'first_name' => $fields['first_name'],
                'last_name' => $fields['last_name'],
                'is_activated' => 0,
                'platform_information' =>$fields['register_receive_information'],
            
                
			]);
			
      $this->beforeSave($object, $original_fields);

			$this->afterSave($object, $fields);
            
			$object->save();


            return $object;
        }, 3);
	}


  public function createLocalForRegister($fields)
  {

  return DB::transaction(function () use ($fields) {
          $original_fields = $fields;

          $fields = $this->prepareFieldsBeforeCreate($fields);

          $generator = new Generator([ 'separator' => '_' ]);
          $username = $generator->generate($fields['first_name'] . ' ' . $fields['last_name']);

          
         // $usern = EdxAuthUser::where('username', $username)->first();
          
          
          
          $object = $this->model->create([
              'name' => $fields['first_name'] . ' ' . $fields['last_name'],
              'email' => $fields['registration_email'],
              'role' =>  UserRole::Learner,
              'username'=> $username,
              'password' => Hash::make($fields['password']),
              'company_id' => 1,
              'branch_id' => $fields['branch'],
              'job_role_id' => $fields['job_role'],
              'phone' => $fields['mobile_number'],
              'phone_locale' =>  'KE',
              'first_name' => $fields['first_name'],
              'last_name' => $fields['last_name'],
              'is_activated' => 0,
              'platform_information' =>$fields['register_receive_information'],
          
              
    ]);
    
    $this->beforeSave($object, $original_fields);

    $this->afterSave($object, $fields);
          
    $object->save();


          return $object;
      }, 3);
}


    public function createForValidation($fields)
    {
        $verify = TwillUsersAuth::where('user_id', $fields['user'])->where('token', $fields['user'])->first();

       if(!empty($verify)){
        $this->model->where('id',$verify->user_id)->update([
            'is_activated' => 1,
            'registered_at' => Carbon\Carbon::now(),
        ]);

        


       }


    }

    protected function changeCompany($email)
    {
      //Get domain
      $raw_domain = explode("@", $email)[1];
      //Get company with domain
      $domain = Domain::where('name', $raw_domain)->first();
      if ($domain) {
        $company_id = $domain->company_id;
      } else {
        $company_id = null;
      }

      return $company_id;
    }
}
