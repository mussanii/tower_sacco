<?php

return [
    'content'=> [
        'title' =>'Content',
         'route' => 'admin.content.pages.index',
         'primary_navigation' => [
             'pages' => [
                 'title' => 'Pages',
                  'module' => true,

             ],
            

            'events' => [
                'title' => 'Events',
                 'module' => true,

            ],

            'blogs' => [
                'title' => 'Blogs',
                 'module' => true,

            ],

             'socialmedia' => [
                'title' => 'Social Media',
                 'module' => true,

            ],
            
        ],
    ],
    
 'resources'=> [
    'title' =>'Resources',
        'route' => 'admin.resources.resources.index',
        'primary_navigation' => [
        
            'resources' => [
            'title' => 'Resources',
                'module' => true,

        ],
        'resourceTypes' => [
            'title' => 'Resource Types',
                'module' => true,

        ],
        
    ],
],
      
// 'sessions'=> [
//     'title' =>'Sessions',
//      'route' => 'admin.sessions.sessions.index',
//      'primary_navigation' => [
//          'sessions' => [
//              'title' => 'sessions',
//               'module' => true,

//          ],

//          'sessionResources' => [
//              'title' => 'Sessions Resources',
//               'module' => true,

//          ],
//      ]
//  ],

    'courses' => [
        'title' => 'Courses',
        'route' => 'admin.courses.index',
        
    ],

    'jobRoles' => [
        'title' => 'Job Roles',
        'module' => true
    ],
    
    
 
];


