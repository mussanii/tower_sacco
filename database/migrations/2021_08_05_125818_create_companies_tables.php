<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTables extends Migration
{
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->string('name')->nullable();
            $table->string('contact_first_name')->nullable();
            $table->string('contact_last_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('secondcontact_first_name')->nullable();
            $table->string('secondcontact_last_name')->nullable();
            $table->string('secondcontact_email')->nullable();
            $table->integer('position')->unsigned()->nullable();
        });

        Schema::create('company_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'company');
            $table->string('title', 200)->nullable();
            
        });

        Schema::create('company_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'company');
        });

        
    }

    public function down()
    {
        
        Schema::dropIfExists('company_translations');
        Schema::dropIfExists('company_slugs');
        Schema::dropIfExists('companies');
    }
}
