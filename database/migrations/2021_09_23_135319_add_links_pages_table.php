<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLinksPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('page_translations', function (Blueprint $table) {
            $table->string('link_text', 400)->nullable();
			$table->string('url', 800)->nullable();
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('page_translations', function (Blueprint $table) {
			$table->dropColumn('link_text');
			$table->dropColumn('url');
		
        });
    }
}
