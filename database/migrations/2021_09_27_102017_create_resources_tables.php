<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResourcesTables extends Migration
{
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('resource_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'resource');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
            $table->string('external_text', 200)->nullable();
            $table->string('external_link', 1000)->nullable();
        });

        Schema::create('resource_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'resource');
        });

        Schema::create('resource_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'resource');
        });
    }

    public function down()
    {
        Schema::dropIfExists('resource_revisions');
        Schema::dropIfExists('resource_translations');
        Schema::dropIfExists('resource_slugs');
        Schema::dropIfExists('resources');
    }
}
