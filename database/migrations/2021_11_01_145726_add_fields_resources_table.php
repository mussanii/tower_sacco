<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('resources', function (Blueprint $table) {
            $table->bigInteger('resource_type_id')->unsigned()->nullable();
            $table->bigInteger('resource_theme_id')->unsigned()->nullable();
			$table->string('resource_category')->nullable();
			$table->string('resource_country')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('resources', function (Blueprint $table) {
			$table->dropColumn('resource_type_id');
			$table->dropColumn('resource_category');
			$table->dropColumn('resource_country');
            $table->dropColumn('resource_theme_id');
        });
    }
}
