<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupForumsTables extends Migration
{
    public function up()
    {
        Schema::create('group_forums', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->string('title')->nullable();
			$table->text('description')->nullable();
            $table->string('forum_type')->nullable();
			$table->text('forum_topic')->nullable();
            $table->text('group_id')->nullable();
            $table->string('created_by')->nullable();
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('group_forum_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'group_forum');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('group_forum_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'group_forum');
        });

        Schema::create('group_forum_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'group_forum');
        });
    }

    public function down()
    {
        Schema::dropIfExists('group_forum_revisions');
        Schema::dropIfExists('group_forum_translations');
        Schema::dropIfExists('group_forum_slugs');
        Schema::dropIfExists('group_forums');
    }
}
