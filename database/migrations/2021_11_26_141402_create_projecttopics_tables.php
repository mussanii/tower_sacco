<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjecttopicsTables extends Migration
{
    public function up()
    {
        Schema::create('projecttopics', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('projecttopic_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'projecttopic');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('projecttopic_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'projecttopic');
        });

        Schema::create('projecttopic_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'projecttopic');
        });
    }

    public function down()
    {
        Schema::dropIfExists('projecttopic_revisions');
        Schema::dropIfExists('projecttopic_translations');
        Schema::dropIfExists('projecttopic_slugs');
        Schema::dropIfExists('projecttopics');
    }
}
