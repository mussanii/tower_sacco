<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBranchTwillusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $twillUsersTable = config('twill.users_table', 'twill_users');

        Schema::table($twillUsersTable, function (Blueprint $table) {
            $table->string('branch_id')->nullable();
            $table->string('job_role_id')->nullable();
            
            
		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        $twillUsersTable = config('twill.users_table', 'twill_users');
        Schema::table($twillUsersTable, function (Blueprint $table) {

            $table->dropColumn('branch_id');
            $table->dropColumn('job_role_id');
			
        });
    }
}
