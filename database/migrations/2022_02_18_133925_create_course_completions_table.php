<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseCompletionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_completions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->string('course_id');
            $table->foreign('user_id')->references('id')->on('twill_users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->string('username');
            $table->string('email');
            $table->string('branch');
            $table->string('score');
            $table->string('max_score');
            $table->string('completion_date');
            $table->string('enrollment_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_completions');
    }
}
