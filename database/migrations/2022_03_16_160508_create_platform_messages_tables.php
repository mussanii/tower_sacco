<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlatformMessagesTables extends Migration
{
    public function up()
    {
        Schema::create('platform_messages', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->string('role')->nullable();
            $table->text('subject')->nullable();
            $table->text('message')->nullable();
            $table->integer('position')->unsigned()->nullable();
        });

        Schema::create('platform_message_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'platform_message');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('platform_message_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'platform_message');
        });

        Schema::create('platform_message_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'platform_message');
        });
    }

    public function down()
    {
        Schema::dropIfExists('platform_message_revisions');
        Schema::dropIfExists('platform_message_translations');
        Schema::dropIfExists('platform_message_slugs');
        Schema::dropIfExists('platform_messages');
    }
}
