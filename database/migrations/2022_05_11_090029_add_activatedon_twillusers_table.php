<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddActivatedonTwillusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $twillUsersTable = config('twill.users_table', 'twill_users');
        Schema::table($twillUsersTable, function (Blueprint $table) {
            $table->timestamp('activated_at')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->text('published_by')->nullable();
		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        $twillUsersTable = config('twill.users_table', 'twill_users');
        Schema::table($twillUsersTable, function (Blueprint $table) {
            $table->dropColumn('activated_at');
            $table->dropColumn('published_at');
            $table->dropColumn('published_by');
			
        });
    }
}
