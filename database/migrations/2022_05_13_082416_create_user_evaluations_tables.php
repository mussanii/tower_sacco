<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserEvaluationsTables extends Migration
{
    public function up()
    {
        Schema::create('user_evaluations', function (Blueprint $table) {
                    // this will create an id, a "published" column, and soft delete and timestamps columns
                    createDefaultTableFields($table);
            
                    // feel free to modify the name of this column, but title is supported by default (you would need to specify the name of the column Twill should consider as your "title" column in your module controller if you change it)
                    $table->string('username', 200);
                    $table->bigInteger('user_id')->unsigned();
                    $table->string('course_id');
                    $table->foreign('user_id')->references('id')->on('twill_users')->onUpdate('CASCADE')->onDelete('CASCADE');
                    $table->string('question1');
                    $table->string('answer1');
                    $table->string('question2');
                    $table->string('answer2');
                    $table->string('question3');
                    $table->string('answer3');
                    $table->string('question4');
                    $table->string('answer4');
                    $table->string('question5');
                    $table->string('answer5');
                    $table->string('question6');
                    $table->string('answer6');
                    $table->string('question7');
                    $table->string('answer7');
                    $table->string('question8')->nullable();
                    $table->string('answer8')->nullable();
                    $table->string('question9')->nullable();
                    $table->string('answer9')->nullable();
                    $table->string('question10')->nullable();
                    $table->string('answer10')->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        

        

        
    }

    public function down()
    {
        
        Schema::dropIfExists('user_evaluations');
    }
}
