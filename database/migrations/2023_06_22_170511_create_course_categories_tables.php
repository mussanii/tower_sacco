<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCourseCategoriesTables extends Migration
{
    public function up()
    {
        Schema::create('course_categories', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('course_category_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'course_category');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('course_category_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'course_category');
        });

        Schema::create('course_category_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'course_category');
        });
    }

    public function down()
    {
        Schema::dropIfExists('course_category_revisions');
        Schema::dropIfExists('course_category_translations');
        Schema::dropIfExists('course_category_slugs');
        Schema::dropIfExists('course_categories');
    }
}
