<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePoliciesTables extends Migration
{
    public function up()
    {
        Schema::create('policies', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('policy_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'policy');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
            $table->text('long_description')->nullable();
        });

        Schema::create('policy_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'policy');
        });

        Schema::create('policy_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'policy');
        });
    }

    public function down()
    {
        Schema::dropIfExists('policy_revisions');
        Schema::dropIfExists('policy_translations');
        Schema::dropIfExists('policy_slugs');
        Schema::dropIfExists('policies');
    }
}
