import AppForm from '../app-components/Form/AppForm';

Vue.component('session-meeting-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {

                topic:  '' ,
                start_time:  '' ,
                agenda:  '' ,
                duration: '',
                session_image:'',
                session_video: '',
                
            },
            
        }
    }

});