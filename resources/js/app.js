require('./bootstrap');

const AOS = require('aos/dist/aos')
var Promise = require('es6-promise').Promise;
const ClassicEditor = require('@ckeditor/ckeditor5-build-classic');

window.Vue = require('vue');

let axios = require('axios');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('nav-search', require('./components/NavSearch.vue').default);
Vue.component('toggle-corporate', require('./components/ToggleCorporate.vue').default);
Vue.component('site-message', require('./components/SiteMessage.vue').default);
Vue.component('share-action', require('./components/ShareAction.vue').default);
Vue.component('share-course-action', require('./components/ShareCourseAction.vue').default);
Vue.component('share-modal', require('./components/ShareModal.vue').default);
Vue.component('course-filter', require('./components/CourseFilter.vue').default);
Vue.component('my-course-filter', require('./components/MyCourseFilter.vue').default);
Vue.component('my-course', require('./components/MyCourse.vue').default);
Vue.component('my-course-mobile', require('./components/MyCourseMobile.vue').default);
Vue.component('my-achievements', require('./components/MyAchievements.vue').default);
Vue.component('my-achievements-mobile', require('./components/MyAchievementsMobile.vue').default);
Vue.component('my-wishlist', require('./components/MyWishlist.vue').default);
Vue.component('wishlist-action', require('./components/WishlistAction.vue').default);
Vue.component('purchase-action', require('./components/PurchaseAction.vue').default);
Vue.component('purchase-confirm', require('./components/PurchaseConfirm.vue').default);
Vue.component('purchase-course', require('./components/PurchaseCourse.vue').default);
Vue.component('ppic-form', require('./components/PpicForm.vue').default);
Vue.component('ppic', require('./components/ppic.vue').default);
Vue.component('client-carousel', require('./components/ClientCarousel.vue').default);
Vue.component('courses-carousel', require('./components/CoursesCarousel.vue').default);
Vue.component('training-group', require('./components/TrainingGroups.vue').default);
Vue.component('training-group-mobile', require('./components/TrainingGroupsMobile.vue').default);
Vue.component('my-community', require('./components/MyCommunity.vue').default);
Vue.component('my-community-filter', require('./components/MyCommunityFilter.vue').default);
Vue.component('my-group-filter', require('./components/MyGroupFilter.vue').default);
Vue.component('training-feed', require('./components/TrainingFeed.vue').default);
Vue.component('training-feed-mobile', require('./components/TrainingFeedMobile.vue').default);
Vue.component('course-progress', require('./components/CourseProgress.vue').default);
Vue.component('progress-bar', require('./components/ProgressBar.vue').default);
Vue.component('comment', require('./components/Comments.vue').default);
Vue.component('sub-comments', require('./components/SubComments.vue').default);
Vue.component('sub-comment', require('./components/SubComment.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

window.onload = function () {
  const app = new Vue({
    el: '#app'
  })

  var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
  }
  
  ready(() => { 
      ClassicEditor
          .create(document.querySelector('.wysiwyg'))
          .then( editor => {
            editor.ui.view.editable.element.style.height = '200px';
        } )
          .catch(error => {
              console.log("error", error)
          });

      ClassicEditor
          .create(document.querySelector('.current-wysiwyg'))
          .then( editor => {
            editor.ui.view.editable.element.style.height = '200px';
        } )
          .catch(error => {
              console.log("error", error)
          });
  });

}
