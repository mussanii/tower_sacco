<?php

return [
    'user-license' => [
        'title' => 'User Licenses',

        'actions' => [
            'index' => 'User Licenses',
            'create' => 'New User License',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'course_id' => 'Course',
            'user_id' => 'User',
            'company_license_id' => 'Company license',
            'enrolled_at' => 'Enrolled at',
            'expired_at' => 'Expired at',
            
        ],
    ],

    'course' => [
        'title' => 'Courses',

        'actions' => [
            'index' => 'Courses',
            'create' => 'New Course',
            'edit' => 'Edit :name',
            'sync' => 'Sync Courses',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'short_description' => 'Short description',
            'overview' => 'Overview',
            'more_info' => 'More info',
            'effort' => 'Effort',
            'start' => 'Start',
            'end' => 'End',
            'enrol_start' => 'Enrol start',
            'enrol_end' => 'Enrol end',
            'price' => 'Price',
            'course_image_uri' => 'Course image uri',
            'course_video_uri' => 'Course video uri',
            'job_group_id' => 'Job group',
            'status' => 'Status',
            'course_category_id' => 'Course category',
            'slug' => 'Slug',
            'order_id' => 'Order',
            'course_video' => 'Course video',
           
            
        ],
    ],

    'session-meeting' => [
        'title' => 'Session Meetings',

        'actions' => [
            'index' => 'Session Meetings',
            'create' => 'New Session Meeting',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'meeting_id' => 'Meeting',
            'host_email' => 'Host email',
            'topic' => 'Topic',
            'status' => 'Status',
            'start_time' => 'Start time',
            'agenda' => 'Agenda',
            'join_url' => 'Join url',
            'meeting_password' => 'Meeting password',
            'session_image' => 'Session image',
            'session_video' => 'Session video',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];