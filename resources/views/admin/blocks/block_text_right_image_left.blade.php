@formField('input', [
    'name' => 'title',
    'label' => 'Title',
    'translated' => true,
])

@formField('wysiwyg', [
    'name' => 'description',
	'label' => 'Description',
	'maxlength' => 1000,
    'translated' => true,
])

@formField('medias', [
    'name' => 'image_block',  //role
    'label' => 'Image',
	'withVideoUrl' => false,
	'max' => 1,
])

@formField('input', [
    'name' => 'block_background_color',
    'label' => 'Block Background Color',
    
])

@formField('input', [
    'name' => 'block_text_color',
    'label' => 'Block text Color',
    
])

@formField('select', [
    'name' => 'block_icon',
    'label' => 'Block Icon',
    'placeholder' => 'Select icon',
    'options' => [
        [
            'value' => 'fa-address-book',
            'label' => '&#xf2b9; fa-address-book'
        ],
        [
            'value' => 'fa-users',
            'label' => '&#xf0c0; fa-users'
        ],
        [
            'value' => 'fa-user-o',
            'label' => '&#xf2c0; fa-user-o'
        ]
    ]
])

@formField('input', [
    'name' => 'link_text',
    'label' => 'Link Text',
    'maxlength' => 200,
    'translated' => true,
])
@formField('input', [
    'name' => 'url',
    'label' => 'URL',
	'translated' => true,
])

@formField('select', [
    'name' => 'block_variant',
    'label' => 'Variant',
    'placeholder' => 'Select a variant',
    'options' => [
        [
            'value' => 'blue_bg',
            'label' => 'Blue Background'
        ],
        [
            'value' => 'white_bg_blue_border',
            'label' => 'White Background blue Border'
        ]
    ]
])