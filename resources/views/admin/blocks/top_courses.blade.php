@php 
use App\Repositories\ContentBucketRepository;

@endphp

@formField('multi_select', [
	'name' => 'content_bucket',
	'label' => 'Content Bucket',
	'placeholder' => 'Select Content Bucket',
	'options' => collect(app(ContentBucketRepository::class)->listAll('title') ?? ''),
])

@formField('input', [
    'name' => 'title',
    'label' => 'Title',
    'translated' => true,
])

@formField('wysiwyg', [
    'name' => 'description',
	'label' => 'Description',
	'maxlength' => 1000,
    'translated' => true,
])