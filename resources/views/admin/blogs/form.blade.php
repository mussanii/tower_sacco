@extends('twill::layouts.form')

@section('contentFields')
@formField('wysiwyg', [
    'name' => 'description',
    'label' => 'Short Description',
    'placeholder' => 'Introduction Text',
    'maxlength' => 2000,
    'translated' => true,
])

@formField('medias',[
'name' => 'blog_image',
'label' => 'cover image',
])

@formField('medias',[
'name' => 'inner_image',
'label' => 'Article inner image',
])

@formField('files', [
    'name' => 'download_resource',
    'label' => 'Downloadable Resource',
])
@stop
