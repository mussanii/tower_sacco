@extends('twill::layouts.form')

@section('contentFields')
@formField('select', [
	'name' => 'course_id',
	'label' => 'Related course',
	'placeholder' => 'Select related course',
	'options' => collect($courseList ?? ''),
])

@formField('select', [
	'name' => 'option_id',
	'label' => 'Answer Type',
	'placeholder' => 'Select Answer Type',
	'options' => collect($optionList ?? ''),
])
@stop
