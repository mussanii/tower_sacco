@extends('twill::layouts.form')

@section('contentFields')

@formField('select', [
	'name' => 'group_type_id',
	'label' => 'Group Type',
	'placeholder' => 'Select Group Type',
	'options' => collect($groupTypeList ?? ''),
])

@formField('select', [
	'name' => 'group_format',
	'label' => 'Group Format',
	'placeholder' => 'Select Group Format',
	'options' => collect($groupFormatList ?? ''),
])

@formField('select', [
	'name' => 'group_theme',
	'label' => 'Group Theme',
	'placeholder' => 'Select Group Theme',
	'options' => collect($groupThemeList ?? ''),
])

@formField('wysiwyg', [
        'name' => 'description',
        'label' => 'Short Description',
        'placeholder' => 'Introduction Text',
		'maxlength' => 2000,
		'translated' => true,
])

@formField('medias',[
    'name' => 'cover_image',
    'label' => 'Group image',
])

@formField('multi_select', [
    'name' => 'users',
    'label' => 'Invite Users',
    'placeholder' => 'Select Users',
    'unpack' => false,
    'options' => $users
])


@formField('input', [
		'name' => 'created_by',
		'label'=> 'Creator',
		'default' => Auth::user()->id,
		'readonly' =>true,
])
@stop