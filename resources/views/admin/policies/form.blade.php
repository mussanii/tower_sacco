@extends('twill::layouts.form')

@section('contentFields')

@formField('select', [
	'name' => 'resource_type_id',
	'label' => 'Resource Category',
	'placeholder' => 'Select Resource Category',
	'options' => collect($resourceTypeList ?? ''),
])


@formField('wysiwyg', [
        'name' => 'description',
        'label' => 'Short Description',
        'placeholder' => 'Introduction Text',
		'maxlength' => 2000,
		'translated' => true,
])
@formField('wysiwyg', [
    'name' => 'long_description',
    'label' => 'File Contents',
    'toolbarOptions' => [
      ['header' => [2, 3, 4, 5, 6, false]],
      'bold',
      'italic',
      'underline',
      'strike',
      ["script" => "super"],
      ["script" => "sub"],
      "blockquote",
      "code-block",
      ['list' => 'ordered'],
      ['list' => 'bullet'],
      ['indent' => '-1'],
      ['indent' => '+1'],
      ["align" => []],
      ["direction" => "rtl"],
      'link',
      "clean",
    ],
    'placeholder' => 'Paste your file contents',

	'translated' => true,
 ])

@formField('medias',[
    'name' => 'resource_image',
    'label' => 'Resources image',
])


@formField('files', [
	'name' => 'resource_video',
	'label' => 'Video',
	'note' => 'This applies for video resources',
])

@formField('input', [
        'name' => 'video_length',
        'label' => 'Resource Video length',
		'note' => 'The length of the video in minutes',
        'maxlength' => 100
    ])

@formField('repeater', ['type' => 'policy-document'])
@stop