@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.user-license.actions.edit', ['name' => $userLicense->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <user-license-form
                :action="'{{ $userLicense->resource_url }}'"
                :data="{{ $userLicense->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.user-license.actions.edit', ['name' => $userLicense->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.user-license.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </user-license-form>

        </div>
    
</div>

@endsection