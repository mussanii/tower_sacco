@extends('layouts.email')

@section('content')


<p style="font-family:poppins, sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#21AA55;text-align:center;">New Course Notification</p>
<hr class="line-footer">
<p class="bigger-bold" style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">Hello {{ ucwords(strtolower($name)) ?: '' }},</p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">

    Kindly note the following course(s) have been launched for your job role.</p>

    <ul style="font-family: poppins, sans-serif;font-size: 18px;">
      
        @foreach ($course_name as $name )

         <li> {{ $name }}</li>
            
        @endforeach

    </ul>

  <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
      In order to access the course(s) please login to the platform by clicking on the link below .</p>
  
      <p style="font-family: poppins, sans-serif;margin: 30px 0 30px; text-align:center;color:#21AA55;font-size:16px;font-weight: bold;">
        <a href="{{$url}}" class="btn-drk-left" style=" background: #21AA55;
        padding: 10px 15px;
        color: #fff;
        font-size: 18px;
        text-decoration: none;"> Login
        </a>
      </p>


<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 20px 0 0;padding: 0;color:#404040;text-align:center;">
Best Regards,
</p>
<p style="font-family:poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#404040;text-align:center;">
    The Tower Sacco Academy
</p>

@endsection
