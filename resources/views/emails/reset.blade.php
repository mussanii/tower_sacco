@extends('layouts.email')

@section('content')


<p style="font-family:poppins, sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#21AA55;text-align:center;">Reset Password</p>
<hr class="line-footer">
<p class="bigger-bold" style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">Hello {{ ucwords(strtolower($name)) ?: '' }},</p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
    You are receiving this email because we received a password reset.</p>

  <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
      In order to complete this action click on the link below .</p>
  
      <p style="font-family: poppins, sans-serif;margin: 30px 0 30px; text-align:center;color:#23245F;font-size:16px;font-weight: bold;">
        <a href="{{$url}}" class="btn-drk-left" style=" background: #21AA55;
        padding: 10px 15px;
        color: #fff;
        font-size: 18px;
        text-decoration: none;"> Reset Password
        </a>
      </p>
 

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
 Kindly ignore this email incase you did not make this request.
</p>
<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 20px 0 0;padding: 0;color:#404040;text-align:center;">
Best Regards,
</p>
<p style="font-family:poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#404040;text-align:center;">
  The Tower Sacco Academy
</p>


@endsection
