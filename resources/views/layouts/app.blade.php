<?php
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
     
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">
    
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <script src="{{ asset('js/app.js') }}"></script>
    <script>(function(d){var s = d.createElement("script");s.setAttribute("data-account", "kyz0QDqj5Z");s.setAttribute("src", "https://cdn.userway.org/widget.js");(d.body || d.head).appendChild(s);})(document)</script><noscript>Please ensure Javascript is enabled for purposes of <a href="https://userway.org">website accessibility</a></noscript>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>

    <!-- Styles -->


    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/owl-carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/owl-carousel/assets/owl.theme.default.min.css') }}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />

    <link href="{{ asset('emojis/emoji-picker-main/lib/css/emoji.css') }}" rel="stylesheet">
    <link href="{{ asset('emojis/emoji-picker-main/lib/css/emoji.css') }}" rel="stylesheet">

    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <link href="{{ asset('css/mobile.css') }}" rel="stylesheet" media="screen and (max-width: 840px)">
    @endif

    <!-- Google Tag Manager -->
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'G-H8TMVGQ4CN');
    </script>

    @yield('css')
</head>

<body oncontextmenu="return false;" >
    <div id="app">



        @include('layouts.partials._header')

        @if (Session::has('course_success'))
            <script>
                jQuery(document).ready(function($) {

                    $("#CourseSuccess").addClass('show');
                });
            </script>
        @elseif(Session::has('course_errors'))
            <script>
                jQuery(document).ready(function($) {
                    $("#CourseErrors").addClass('show');
                });
            </script>
        @endif

        @include('site.includes.components.modals.courseerrors')
        @include('site.includes.components.modals.coursemessages')
        @include('site.includes.components.modals.contact-form')
        @yield('content')

        <!-- Back to top button -->

        @include('layouts.partials._footer')
    </div>
    <script src="{{ asset('assets/jquery-ui-1.12.1/external/jquery/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/jquery-ui-1.12.1/jquery-ui.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/owl-carousel/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


    <script src="{{ asset('emojis/emoji-picker-main/lib/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/util.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/jquery.emojiarea.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/emoji-picker.js') }}" type="text/javascript"></script>

    @yield('js')




    <script>
        function myFunction() {
            var elmnt = document.getElementById("home-scroll");
            elmnt.scrollIntoView();
        }


        $(function() {

            $('#navbarSupportedContent')
                .on('shown.bs.collapse', function() {
                    $('.navbar-toggler-icon').addClass('hidden');
                    $('#navbar-close').removeClass('hidden');
                })
                .on('hidden.bs.collapse', function() {
                    $('.navbar-hamburger').removeClass('hidden');
                    $('#navbar-close').addClass('hidden');
                });

        });
    </script>
</body>

</html>
