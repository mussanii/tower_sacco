<?php
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
     
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script>(function(d){var s = d.createElement("script");s.setAttribute("data-account", "kyz0QDqj5Z");s.setAttribute("src", "https://cdn.userway.org/widget.js");(d.body || d.head).appendChild(s);})(document)</script><noscript>Please ensure Javascript is enabled for purposes of <a href="https://userway.org">website accessibility</a></noscript>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <!-- Fonts -->
   
    <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/css/star-rating.min.css" media="all"
        rel="stylesheet" type="text/css" />

    <link href="{{ asset('emojis/lib/css/nanoscroller.css') }}" rel="stylesheet">
    <link href="{{ asset('emojis/lib/css/emoji.css') }}" rel="stylesheet">


    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
  
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{ asset('css/emojionearea.css') }}">

    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <link href="{{ asset('css/mobile.css') }}" rel="stylesheet" media="screen and (max-width: 840px)">
    @endif
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
    <!-- Google Tag Manager -->

    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'G-H8TMVGQ4CN');
    </script>
    @yield('css')
</head>

<body oncontextmenu="return false;" >
    <div id="app">

        @include('layouts.partials._header')

        @include('site.includes.components.modals.courseerrors')
        @include('site.includes.components.modals.coursemessages')
         @include('site.includes.components.modals.contact-form')
        @yield('content')

        <!-- Back to top button -->

        @include('layouts.partials._footer')
    </div>
    <script src="{{ asset('assets/jquery-ui-1.12.1/external/jquery/jquery.js') }}"></script>
    <script src="{{ asset('assets/jquery-ui-1.12.1/jquery-ui.js') }}"></script>
    <script src="{{ asset('assets/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    <script src="{{ asset('emojis/lib/js/nanoscroller.min.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/tether.min.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/config.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/util.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/jquery.emojiarea.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/emoji-picker.js') }}"></script>

    <script src="{{ asset('js/upload.js') }}"></script>
    <script src="{{ asset('js/side-menu.js') }}"></script>

    @yield('js')

    <script>
        function makePayment() {

            $('#channelsModal').modal('show');

        }
    </script>
    <script>
        $(document).on("click", ".confrimpayment", function(e) {

            e.preventDefault();

            var _self = $(this);

            var url = _self.data('val');
            _self.css('display', 'none');
            window.open(url, '_self');
        });
    </script>
    <script>
        function confirmTransaction() {
            $('#img').show();
            setTimeout(function() {
                $('#img').hide();
                /*submit the form after 5 secs*/
                $('#transaction').submit();
            }, 15000)


        }



        $(function() {

            $('#navbarSupportedContent')
                .on('shown.bs.collapse', function() {
                    $('.navbar-toggler-icon').addClass('hidden');
                    $('#navbar-close').removeClass('hidden');
                })
                .on('hidden.bs.collapse', function() {
                    $('.navbar-hamburger').removeClass('hidden');
                    $('#navbar-close').addClass('hidden');
                });

        });
    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PRD4JKF" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
</body>

</html>
