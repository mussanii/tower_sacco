@php
use App\Models\Socialmedia;
use App\Models\Menus;
use App\Models\FooterText;

// $facebook = 'facebook';
// $twitter = 'twitter';
// $youtube = 'youtube';

// $socialfacebook = Socialmedia::where('key',$facebook)->published()->first();
// $socialtwitter = Socialmedia::where('key',$twitter)->published()->first();
// $socialyoutube = Socialmedia::where('key',$youtube)->published()->first();

// $mainmenus = Menus::where('menu_type',1)->published()->orderBy('position', 'asc')->get();

$mainoffice = FooterText::where('id', 1)
    ->published()
    ->first();

$fieldoffice = FooterText::where('id', 2)
    ->published()
    ->first();

$touch = FooterText::where('id', 3)
    ->published()
    ->first();

@endphp
@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <footer>
        <div class="container-fluid">
            <div class="row ">
                <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-3 offset-md-1 ">
                            @if ($mainoffice)
                                <div class="footer-heading">
                                    <span>{{ $mainoffice->title }}</span>
                                </div>
                                <div class="footer-description">
                                    {!! $mainoffice->description !!}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-3 offset-md-1">
                            @if ($fieldoffice)
                                <div class="footer-heading">
                                    <span>{{ $fieldoffice->title }}</span>
                                </div>
                                <div class="footer-description">
                                    {!! $fieldoffice->description !!}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-3 ">
                            <div class="footer-heading">

                            </div>
                            <div>
                                <div class="row">
                                    <div class="form-group col-md-5 ml-4">
                                        <button class="btn btn-overall btn_get_touch" title="Support"
                                            data-toggle="modal"
                                            data-target="#ModalCoporateForm">{{ $touch->title }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>


            </div>
        </div>

        {{-- <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="footer-text">
                    <ul>
                        <li><small>Copyright &copy; <?php echo date('Y'); ?> Erevuka | All Rights Reserved</small>&nbsp;&nbsp;&nbsp;<small class="bordered-left">Powered By <a href="https://farwell-consultants.com"  aria-label="Farwell Innovations(opens in new tab)"  target="_blank">Farwell Innovations Ltd</a></small></li>
                    </ul>
                </div>
            </div>
        </div> --}}
        </div>
    </footer>
@endif

@if ((new \Jenssegers\Agent\Agent())->isMobile())


    <footer>

        <div class="col-md-12">
            <div class="row">
                <div class="col-6 ">
                    @if ($mainoffice)
                        <div class="footer-heading">
                            <span>{{ $mainoffice->title }}</span>
                        </div>
                        <div class="footer-description">
                            {!! $mainoffice->description !!}
                        </div>
                    @endif


                </div>

                <div class="col-6">
                    @if ($fieldoffice)
                        <div class="footer-heading">
                            <span>{{ $fieldoffice->title }}</span>
                        </div>
                        <div class="footer-description">
                            {!! $fieldoffice->description !!}
                        </div>
                    @endif
                </div>

            </div>


        </div>



    </footer>

@endif
