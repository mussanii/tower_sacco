@php
$activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
    <div class="container">
        <ul class="nav__list">
            <li class="nav__item {{ $activeRoute == 'admin.courses.index' ? 's--on' : '' }}">
                <a href="{{ route('admin.courses.index') }}" class="">
                    Courses
                </a>
            </li>
            <li class="nav__item {{ $activeRoute == 'admin.courseCategories.index' ? 's--on' : '' }}">
                <a href="{{ route('admin.courseCategories.index') }}" class="">
                    Course Categories
                </a>
            </li>
        </ul>
    </div>
</nav>
