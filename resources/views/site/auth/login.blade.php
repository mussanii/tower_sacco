
@extends('layouts.auth')

@section('js')
<script src="{{asset('js/auth.js')}}"></script>
@endsection


@section('content')
<section class="auth-page ">
<div class="bg-overlay">
    <div class="row justify-content-center">
        <div class="col-lg-6 auth-login w-100 d-none d-md-block">
            {{-- <div class="card">
            <h1 class="text-white">Register</h1>
            <div class="card-body"> 
                    @include('site.includes.auth.register')
            </div>
            </div> --}}
        </div>
        <div class="col-lg-6">
            <div class="card login">

                <div class="card-body">
                     <div class="col-md-12">
                      <div class="row">

                       <div class="col-md-1"></div>

                       <div class="col-md-10">
                        <h1>Welcome</h1>
                        <div class="form-login">
                        @include('site.includes.auth.login')
                       </div>
                       </div>
                      

                       <div class="col-md-1"></div>

                      </div>

                     </div>
                  
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection


@section('js')
<script>
    $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
               $('#course_information').val('1');
            }
            else if($(this).prop("checked") == false){
                $('#course_information').val(' '); 
            }
        });
    });
</script>
<script>
$(document).on('click', '.pass-view', function(event){
	console.log("clicked");
	var $open = $(this).children('.fa-eye');
	var $close = $(this).children('.fa-eye-slash');
	var $pass = $(this).siblings('.pass');
	if($open.is(':visible')){
		$close.show();
		$open.hide();
		$pass.attr('type', 'text');
	} else {
		$close.hide();
		$open.show();
		$pass.attr('type', 'password');
	}

	
});


</script>

@endsection