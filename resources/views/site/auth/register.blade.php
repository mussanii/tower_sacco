
@extends('layouts.app_no_js')

@section('content')
<section class="auth-page ">
<div class="bg-overlay">
    <div class="row justify-content-center">
        <div class="col-lg-5 auth-login auth-register w-100 d-none d-md-block">
            {{-- <div class="card">
            <h1 class="text-white">Register</h1>
            <div class="card-body"> 
                    @include('site.includes.auth.register')
            </div>
            </div> --}}
        </div>
        <div class="col-lg-7">
            <div class="card login register top-general">

                <div class="card-body">
                    <h1>Register</h1>
                     <div class="col-md-12">
                     
                        <div class="form-login">
                            @include('site.includes.auth.register',['roles'=>$roles])
                       </div>
                      

                     </div>
                  
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection


@section('js')
<script>
    function getNewVal(item)
   {

    if(item.value == 1 || item.value == 2 || item.value == 3  || item.value == 4){
        document.getElementById("parent").style.display = "block";
        document.getElementById("partners").style.display = "none"; 
    }
    else{
        document.getElementById("parent").style.display = "none";
        document.getElementById("partners").style.display = "block"; 
    }
    
   }
   </script>

<script>
$(document).on('click','#checkbox_info',function(){
  var isChecked = $(this).is(':checked');
  if(isChecked == true){
       $('#platforminfo').val('1');
    }
    else if($(this).prop("checked") == false){
        $('#platforminfo').val('0'); 
    }
  
});



$(document).on('click','#checkbox_contact',function(){
  var isChecked = $(this).is(':checked');
  if(isChecked == true){
       $('#consentinfo').val('1');
    }
    else if($(this).prop("checked") == false){
        $('#consentinfo').val('0'); 
    }
  
});
</script>


<script>
    $(document).on('click', '.pass-view', function(event){
        console.log("clicked");
        var $open = $(this).children('.fa-eye');
        var $close = $(this).children('.fa-eye-slash');
        var $pass = $(this).siblings('.pass');
        if($open.is(':visible')){
            $close.show();
            $open.hide();
            $pass.attr('type', 'text');
        } else {
            $close.hide();
            $open.show();
            $pass.attr('type', 'password');
        }
    
        
    });
    
    
    </script>

@endsection