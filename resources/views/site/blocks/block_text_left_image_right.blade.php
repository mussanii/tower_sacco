@php
$variant = $block->input('block_variant');
$icon = $block->input('block_icon');
$background = $block->input('block_background_color');
$text_color = $block->input('block_text_color');

$image = $block->imageObject('image_block');
$ratio = $image->ratio ?? 2/3;
$ratioPercentage = ($ratio * 100);
@endphp

@if($background)
<section class="main-page" style="background-color:<?php echo $background;?>; margin-top:2%; padding:2% 0" >
@else
<section class="main-page" style="margin-top:2%;padding:2% 0">
@endif
    <div class="row ">
        <div class="col-md-7">
            @if($text_color)
            <div class="header-section" style="color: <?php echo $text_color;?> !important; text-align:left">
            <h1 style="color: <?php echo $text_color;?> !important"> <i class="fa <?php echo $icon;?>"></i>{!!$block->translatedinput('title')!!}</h1>
            {!!$block->translatedinput('description')!!}
          </div>
          @else 
          <div class="header-section" style="text-align:left;">
            <h1> <i class="fa <?php echo $icon;?>"></i>{!!$block->translatedinput('title')!!}</h1>
            {!!$block->translatedinput('description')!!}
          </div>
        @endif
           
        </div>

        <div class="col-md-4 mr-3">
            {!!  (new \App\Helpers\Front)->responsiveImage([
                'options' => [
                  'src' => $block->image('image_block', 'default') ?? '',
                  'w' => $image->width ?? 600,
                  'h' => $image->height ?? 400,
                  'ratio' => $ratio,
                  'fit' => 'crop',
                ],
                'html' => [
                  'alt' => $image->altText ?? ''
                ],
                'sizes' => [
                  [ 'media' => 'small', 'w' => 730 ],
                  [ 'media' => 'medium',  'w' => 426 ],
                  [ 'media' => 'large', 'w' => 500 ],
                  [ 'media' => 'xlarge',  'w' => 600 ],
                  [ 'media' => 'xxlarge',  'w' => 680 ]
                ]
              ]) !!}
  
          
        </div>
    </div>
</section>