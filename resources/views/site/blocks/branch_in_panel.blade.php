@php
use App\Models\Branch;

$panels = $block->children;

if (!empty($jobRole)) {
    $branches = Branch::whereHas('translations', function ($query) use ($jobRole) {
        $query->where('title', 'LIKE', '%' . $jobRole . '%');
    })
        ->published()
        ->orderBy('position', 'asc')
        ->get();
} else {
    $branches = Branch::published()
        ->orderBy('position', 'asc')
        ->get();
}
@endphp

@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <section class="general-section">
        <div class="container-fluid">
            <div class="row alignment-class">

                <div class="col-md-12 ">
                    <div role="tabpanel ">
                        <div class="col-md-12">

                            <div class="card progress-filter mb-3 mt-5">
                                <div class="col-12">
                                    <div class="row justify-content-center">

                                        <div class="col-md-5">
                                            <ul class="nav nav-tabs mt-4 mb-4 " role="tablist">
                                                @foreach ($panels as $key => $item)
                                                    @if ($key === $panels->keys()->last())
                                                        <li role="presentation"
                                                            class="{{ $item->id == 1 ? 'active' : '' }} tabs-spacing">
                                                            <a href="#home{{ $item->id }}" aria-controls="home"
                                                                role="tab"
                                                                data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                                                        </li>
                                                    @elseif($key === $panels->keys()->first())
                                                        <li role="presentation" class="active border-right">
                                                            <a href="#home{{ $item->id }}" aria-controls="home"
                                                                role="tab"
                                                                data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                                                        </li>
                                                    @else
                                                        <li role="presentation"
                                                            class="{{ $item->id == 1 ? 'active' : '' }} border-right tabs-spacing">
                                                            <a href="#home{{ $item->id }}" aria-controls="home"
                                                                role="tab"
                                                                data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>

                                        <div class="col-7">

                                            <form action="{{ route('pages', ['key' => 'our-progress']) }}"
                                                method="POST">
                                                @csrf
                                                <input type="hidden" value="{{ 'our-progress' }}" placeholder="search"
                                                    name="key">
                                                <div class="row justify-content-center">
                                                    <div class="col-8 no-padding-right ">

                                                        <input type="text" name="branch" class="form-control mt-4 mb-4 "
                                                            placeholder="Search by name" required>
                                                    </div>
                                                    <div class="col-4 no-padding-left">
                                                        <button type="submit"
                                                            class="btn btn-overall btn_black_bg mt-4 mb-4">Search</button>
                                                        <button type="reset"
                                                            class="btn btn-overall btn_cancel mt-4 mb-4"
                                                            onClick="window.location.href=window.location.href">Reset</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>


                                    </div>


                                </div>
                            </div>


                        </div>
                        <div class="tab-content">


                            @foreach ($panels as $key => $item)
                                <div role="tabpanel"
                                    class="tab-pane {{ $key === $panels->keys()->first() ? 'active' : '' }}"
                                    id="home{{ $item->id }}" class="active">
                                    @if ($key === $panels->keys()->first())
                                        <div class="row">
                                            @foreach ($branches as $branch)
                                                <div class="col-lg-2 col-md-2 col-sm-6 col-12">
                                                    @include(
                                                        'site.includes.components.branch-card',
                                                        ['branch' => $branch]
                                                    )
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>


                </div>

            </div>

        </div>

    </section>
@else
    <section class="general-section">


        <div class="col-md-12 ">
            <div role="tabpanel ">
                <div class="col-md-12">
                    <div class="card progress-filter mb-3 mt-5">
                        <div class="col-12">
                            <div class="row justify-content-center">

                                <div class="col-12">
                                    <ul class="nav nav-tabs mt-2 mb-2" role="tablist">
                                        @foreach ($panels as $key => $item)
                                            @if ($key === $panels->keys()->last())
                                                <li role="presentation"
                                                    class="{{ $item->id == 1 ? 'active' : '' }} tabs-spacing">
                                                    <a href="#home{{ $item->id }}" aria-controls="home" role="tab"
                                                        data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                                                </li>
                                            @elseif($key === $panels->keys()->first())
                                                <li role="presentation" class="active border-right">
                                                    <a href="#home{{ $item->id }}" aria-controls="home" role="tab"
                                                        data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                                                </li>
                                            @else
                                                <li role="presentation" class="active border-right tabs-spacing">
                                                    <a href="#home{{ $item->id }}" aria-controls="home" role="tab"
                                                        data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>

                                <div class="col-12">

                                    <form action="{{ route('pages', ['key' => 'our-progress']) }}" method="POST">
                                        <input type="hidden" value="{{ 'our-progress' }}" placeholder="search"
                                            name="key">
                                        @csrf
                                        <div class="row">
                                            <div class="col-12 ">

                                                <input type="text" name="branch" class="form-control  mb-2 "
                                                    placeholder="Search by name" required>
                                            </div>
                                            <div class="col-12 ">
                                                <button type="submit"
                                                    class="btn btn-overall btn_black_bg mb-3">Search</button>
                                                <button type="reset" class="btn btn-overall btn_cancel  mb-3"
                                                    onClick="window.location.href=window.location.href">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>


                            </div>


                        </div>
                    </div>


                </div>
                <div class="tab-content">


                    @foreach ($panels as $key => $item)
                        <div role="tabpanel" class="tab-pane {{ $key === $panels->keys()->first() ? 'active' : '' }}"
                            id="home{{ $item->id }}" class="active">
                            @if ($key === $panels->keys()->first())
                                <div class="row">
                                    @foreach ($branches as $branch)
                                        <div class="col-lg-2 col-md-2 col-sm-6 col-12">
                                            @include(
                                                'site.includes.components.branch-card',
                                                ['branch' => $branch]
                                            )
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </section>
@endif
