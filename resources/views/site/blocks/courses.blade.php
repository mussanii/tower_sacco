@php 
use App\Models\Course;
use App\Models\JobroleCourse;
use App\Models\JobRole;

if(!empty($jobRole)){

$courses = JobroleCourse::where('job_role_id', $jobRole)->with('courses')->paginate(12);
$jobr = JobRole::where('id', $jobRole)->first();

}else{

$courses = Course::published()->where('status','=',1)->orderBy('position','asc')->paginate(12);
}



@endphp

@if (Route::current()->getName() === 'home')
<section class="general-section no-margin-top">
    <div class="container-fluid mobile-container-fluid">
    <div class="col-12 ">
        <div class="row alignment-class ">
		<div class="hrHeading">
			<h2 class="line-header child_header">
				{{ $block->translatedinput('title') }}
			</h2>
		</div>
    </div>
	</div>
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="row alignment-class mt-5">
        @if(count($courses) > 0)

        @if(!empty($jobRole))
        @foreach($courses as $related)
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            @include('site.includes.components.course-card',['course'=>$related->courses])
        </div>
       @endforeach

        @else 

        @foreach($courses as $related)
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            @include('site.includes.components.course-card',['course'=>$related])
        </div>
       @endforeach

        @endif
        @else
        <p class="text-center"> There are no Courses </p>
         @endif
    </div>

    @if(count($courses) > 12)
    <div class="row justify-content-center  pagination-spacing">{!! $courses->links() !!}</div>
     @endif
    @else 
    <div class="row mt-3">
        @if(count($courses) > 0)
        @if(!empty($jobRole))
        @foreach($courses as $related)
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            @include('site.includes.components.course-card',['course'=>$related->courses])
        </div>
       @endforeach

        @else 

        @foreach($courses as $related)
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            @include('site.includes.components.course-card',['course'=>$related])
        </div>
       @endforeach

        @endif
        @else
        <p class="text-center"> There are no Courses </p>
         @endif
    </div>

    @if(count($courses) > 12)
    <div class="row justify-content-center  pagination-spacing">{!! $courses->links() !!}</div>
     @endif
    @endif

</div>
</section>
@else 

<section class="general-section no-margin-top" >
    <div class="container-fluid mobile-container-fluid">
    
        @if((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="row alignment-class ">
            @if(count($courses) > 0)
            @if(!empty($jobRole))
            @foreach($courses as $related)
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                @include('site.includes.components.course-card',['course'=>$related->courses])
            </div>
           @endforeach
    
            @else 
    
            @foreach($courses as $related)
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                @include('site.includes.components.course-card',['course'=>$related])
            </div>
           @endforeach
    
            @endif
            @else
            <p class="text-center"> There are no Courses</p>
             @endif
        </div>
        @if(count($courses) > 12)
        <div class="row justify-content-center  pagination-spacing">{!! $courses->links() !!}</div>
         @endif
        @else 
        <div class="row mt-3">
            @if(count($courses) > 0)
            @if(!empty($jobRole))
            @foreach($courses as $related)
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                @include('site.includes.components.course-card',['course'=>$related->courses])
            </div>
           @endforeach
    
            @else 
    
            @foreach($courses as $related)
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                @include('site.includes.components.course-card',['course'=>$related])
            </div>
           @endforeach
    
            @endif
            @else
            <p class="text-center"> There are no Courses </p>
             @endif
        </div>
        @if(count($courses) > 12)
        <div class="row justify-content-center  pagination-spacing">{!! $courses->links() !!}</div>
         @endif
        @endif
    
</div>
</section>
@endif

