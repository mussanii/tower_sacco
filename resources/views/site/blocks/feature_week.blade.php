@php 
 use App\Models\FeatureWeek;

 $feature = FeatureWeek::published()->first();


@endphp

@if((new \Jenssegers\Agent\Agent())->isDesktop())
<section class="general-section business-general">
    <div class="container-fluid">
        <div class="col-md-12 ">
            <div class="hrHeading " style="padding-top: 2%;">
                <h2 class="line-header text-center block-headers">
                    {{ $block->translatedinput('title') }}
                </h2>
            </div>
                <div class="col-lg-12">
                    <div class="row justify-content-center mt-3 mb-5" >
                        <div class="col-8 ">
                                    <div class="card upcoming-card upcoming-side mb-3 p-5" >
                                        <div class="row g-0">
                                          <div class="col-md-12 ">
                                             <div class="row">
                                                <div class="col-6">
                                                    <video controls playsinline muted id="weekid">
                                                        <source src="{{ $feature->file("resource_video")}}" type="video/mp4">
                                                    </video>

                                                </div>
    
                                                <div class="col-6 feature_week_right">
                                                    {{$feature->title}}

                                                    <p class="feature_week_date">{{Carbon\Carbon::parse($feature->created_at)->isoFormat('Do MMMM YYYY')}} </p>
                                                    
                                                    {!!$feature->description!!}

                                                </div>

                                             </div>
                                            

                                           
                                          </div>
                                          
                                        </div>
                                      </div>
                       
                                
                           
                        </div>
                        
                           
                </div>
            </div>
        </div>
    
    </div>
    
</section>
@endif