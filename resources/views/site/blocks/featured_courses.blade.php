
@php
use App\Models\Course;

$variant = $block->input('block_variant');
$icon = $block->input('block_icon');
$background = $block->input('block_background_color');
$text_color = $block->input('block_text_color');
$number = $block->input('course_number');
$url =$block->input('url');
$linkText = $block->input('link_text');
$courses = Course::orderBy('created_at', 'ASC')->take($number)->get();

@endphp
<section class="main-page ">
    <div class="row justify-content-center">
        <div class="col">
            <div class="header-section">
                <h1 class="text" style="color: #23245f">{!!$block->translatedinput('title')!!}</h1><br>
                    <div class="row">                                            
                             @foreach($courses as $course)
                             <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                            @include('site.includes.components.course-card',['course'=>$course])
                            </div>
                            @endforeach 
                    </div>
                    <div class="body-section">
                       <div class="call-to-action" >
                @if ($variant && ($variant === 'orange_bg'))
           
                <a href="{{ $url }}" class="btn btn-overall btn_register">
                  {{ $linkText }}
                  <i class="fa fa-arrow-right "></i>
                </a>
            @elseif ($variant && ($variant === 'white_bg_orange_border'))
                <a href="{{ $url }} " class="btn btn-overall btn_login">
                  {{ $linkText }}
                  <i class="fa fa-arrow-right "></i>
                </a>
            @else 
            <a href="{{ $url }}" class="btn btn-overall btn_register">
              {{ $linkText }}
              <i class="fa fa-arrow-right "></i>
            </a>

            @endif 
                       </div>
                    </div>
            </div>
        </div>
    </div>    
</section> 


                    