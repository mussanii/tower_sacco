@php 
use App\Models\Course;

$number = $block->translatedinput('number');

$courses = Course::getNotMandatory();

@endphp

@if (Route::current()->getName() === 'home')
<section class="general-section no-margin-top">
    <div class="container-fluid mobile-container-fluid">
    <div class="col-12 ">
        <div class="row alignment-class ">
            <div class="col-12 ">
		<div class="hrHeading">
			<h2 class="line-header child_header">
				{{ $block->translatedinput('title') }}
			</h2>
		</div>
            </div>
    </div>
	</div>
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="row alignment-class mt-5">
        @if(count($courses) > 0)
        @foreach($courses as $related)
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            @include('site.includes.components.course-card',['course'=>$related])
        </div>
       @endforeach
        @else
        <p class="text-center" style="margin-left:40%"> There are no Courses </p>
         @endif
    </div>

    @if(count($courses) > 0)
    <div class="row justify-content-center  pagination-spacing">{!! $courses->links() !!}</div>
     @endif
    @else 
    <div class="row mt-3">
        @if(count($courses) > 0)
        @foreach($courses as $related)
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            @include('site.includes.components.course-card',['course'=>$related])
        </div>
       @endforeach
        @else
        <p class="text-center" style="margin-left:40%"> There are no Courses </p>
         @endif
    </div>

    @if(count($courses) > 0)
    <div class="row justify-content-center  pagination-spacing">{!! $courses->links() !!}</div>
     @endif
    @endif

</div>
</section>
@else 

<section class="general-section mt-5">
    <div class="container-fluid mobile-container-fluid">
        <div class="col-12 ">

            <div class="hrHeading">
                <h2 class="line-header child_header">
                    {{ $block->translatedinput('title') }}
                </h2>
            </div>

        </div>

        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="col-12 c_categories-tabs ">
            <ul class="nav course-category-tab c_category-tabs" id="myOtherCourses" role="tablist">
                @foreach ($courseCategories as $key => $parent)
                    @if ($key == 0)
                    @elseif($key == 1)
                        <li class="nav-item first-nav-item">
                            <a class="faq-link {{ $key == 0 ? 'active' : '' }}" id="course-{{ $parent->id }}-tab"
                                data-toggle="tab" href="#course-{{ $parent->id }}" role="tab"
                                aria-controls="course-{{ $parent->id }}" aria-selected="true">
                                {{ $parent->title }}
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="faq-link {{ $key == 0 ? 'active' : '' }}" id="course-{{ $parent->id }}-tab"
                                data-toggle="tab" href="#course-{{ $parent->id }}" role="tab"
                                aria-controls="course-{{ $parent->id }}" aria-selected="true">
                                {{ $parent->title }}
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
            <div class="tab-content" id="myOtherCoursesContent">
                @foreach ($courseCategories as $key => $parent)
                @if ($key == 0)
                    <div class="tab-pane fade {{ $key == 0 ? 'show active' : '' }}" id="course-{{ $parent->id }}"
                        role="tabpanel" aria-labelledby="course-{{ $parent->id }}-tab">

                        <div class="row alignment-class mt-3">
                            @if (count($courses) > 0)
                                @foreach ($courses as $related)
                                    <div class="col-sm-6 col-md-3 col-12 flex-col">
                                        @include('site.includes.components.course-card', [
                                            'course' => $related,
                                        ])
                                    </div>
                                @endforeach
                            @else
                                <p class="text-center no-courses" style="margin-left:40%"> There are no courses</p>
                            @endif
                        </div>
                        @if (count($courses) > 0)
                            <div class="row justify-content-center  pagination-spacing">{!! $courses->links() !!}</div>
                        @endif

                    </div>

                @else 
                <div class="tab-pane fade {{ $key == 0 ? 'show active' : '' }}" id="course-{{ $parent->id }}"
                    role="tabpanel" aria-labelledby="course-{{ $parent->id }}-tab">

                    <div class="row alignment-class mt-3">
                        @if (count($parent->courses) > 0)
                            @foreach ($parent->courses as $related)
                           
                                <div class="col-sm-6 col-md-3 col-12 flex-col">
                                    @include('site.includes.components.course-card', [
                                        'course' => $related,
                                    ])
                                </div>
                            
                            @endforeach
                        @else
                            <p class="text-center no-courses" style="margin-left:40%" > There are no courses for this category</p>
                        @endif
                    </div>
                    @if (count($parent->courses) > 0)
                        <div class="row justify-content-center  pagination-spacing">{!! $courses->links() !!}</div>
                    @endif

                </div>


                @endif
                @endforeach
            </div>
        @else
        <div class="col-12 c_categories-tabs ">
            <ul class="nav course-category-tab c_category-tabs" id="myOtherCourses" role="tablist">
                @foreach ($courseCategories as $key => $parent)
                
                    @if ($key == 0)
                    @elseif($key == 1)
                        <li class="nav-item first-nav-item">
                            <a class="faq-link {{ $key == 0 ? 'active' : '' }}" id="course-{{ $parent->id }}-tab"
                                data-toggle="tab" href="#course-{{ $parent->id }}" role="tab"
                                aria-controls="course-{{ $parent->id }}" aria-selected="true">
                                {{ $parent->title }}
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="faq-link {{ $key == 0 ? 'active' : '' }}" id="course-{{ $parent->id }}-tab"
                                data-toggle="tab" href="#course-{{ $parent->id }}" role="tab"
                                aria-controls="course-{{ $parent->id }}" aria-selected="true">
                                {{ $parent->title }}
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
            <div class="tab-content" id="myOtherCoursesContent">
            <div class="row mt-3">
                @if (count($courses) > 0)
                    @foreach ($courses as $related)
                        <div class="col-sm-6 col-md-3 col-12">
                            @include('site.includes.components.course-card', ['course' => $related])
                        </div>
                    @endforeach
                @else
                    <p class="text-center no-courses" style="margin-left:40%"> There are no courses </p>
                @endif
            </div>
           
                <div class="row justify-content-center  pagination-spacing">{!! $courses->links() !!}</div>
            </div>
        @endif
    </div>
</section>
@endif

