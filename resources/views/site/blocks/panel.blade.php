@php
use App\Models\Session;
use Carbon\Carbon;
@endphp

<div class="container-fluid">
    <div role="tabpanel " class="session-panel">
        <div class="col-md-12">
            <div class="row">
                @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                    <div class="col-md-10">
                        <ul class="nav nav-tabs" role="tablist">
                            @foreach ($panels as $key => $item)
                                @if ($key === $panels->keys()->last())
                                    <li role="presentation" class="{{ $item->id == 1 ? 'active' : '' }} tabs-spacing">
                                        <a href="#home{{ $item->id }}" aria-controls="home" role="tab"
                                            data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                                    </li>
                                @elseif($key === $panels->keys()->first())
                                    <li role="presentation" class="active border-right">
                                        <a href="#home{{ $item->id }}" aria-controls="home" role="tab"
                                            data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                                    </li>
                                @else
                                    <li role="presentation"
                                        class="{{ $item->id == 1 ? 'active' : '' }} border-right tabs-spacing">
                                        <a href="#home{{ $item->id }}" aria-controls="home" role="tab"
                                            data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @else
                    <div class="col-md-10">
                        <ul class="nav nav-tabs" role="tablist">
                            @foreach ($panels as $key => $item)
                                @if ($key === $panels->keys()->last())
                                    <li role="presentation" class="{{ $item->id == 1 ? 'active' : '' }} tabs-spacing">
                                        <a href="#home{{ $item->id }}" aria-controls="home" role="tab"
                                            data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                                    </li>
                                @elseif($key === $panels->keys()->first())
                                    <li role="presentation" class="active border-right">
                                        <a href="#home{{ $item->id }}" aria-controls="home" role="tab"
                                            data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                                    </li>
                                @else
                                    <li role="presentation" class="active border-right tabs-spacing">
                                        <a href="#home{{ $item->id }}" aria-controls="home" role="tab"
                                            data-toggle="tab">{{ $item->translatedinput('panel_title') }}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="tab-content">


            @foreach ($panels as $key => $item)
                @if ($key === $panels->keys()->first())
                    @php $first = $item->id; @endphp
                    @php $sessions = Session::where('start_time', '>=', Carbon::now())->get(); @endphp
                @else
                    @php
                        $sessions = Session::where('start_time', '<', Carbon::today()->toDateString())
                            ->orderBy('created_at', 'desc')
                            ->get();
                    @endphp
                @endif
                <div role="tabpanel" class="tab-pane {{ $key === $panels->keys()->first() ? 'active' : '' }}"
                    id="home{{ $item->id }}" class="active">

                    @if (count($sessions) > 0)
                        <div class="row">

                            @foreach ($sessions as $session)
                                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                    @include(
                                        'site.includes.components.session-card',
                                        [
                                            'session' => $session,
                                        ]
                                    )
                                </div>
                            @endforeach
                        </div>
                    @else
                        <p class="mt-4 empty-sessions">Currently no sessions are scheduled. Keep checking this space for
                            upcoming
                            sessions. For now feel welcome to interact with what we have covered so far on Past
                            Sessions..
                        </p>
                    @endif

                </div>
            @endforeach
        </div>
    </div>


</div>
