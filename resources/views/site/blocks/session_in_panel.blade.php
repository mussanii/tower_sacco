@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <section class="general-section">
        <div class="container-fluid">
            <div class="row justify-content-center alignment-class-sessions">
                <div class="col-md-12  mt-5">
                    @include('site.blocks.panel', [
                        'panels' => $block->children,
                    ])

                </div>
            </div>

        </div>

    </section>
@else
    <section class="general-section">

        <div class="col-md-12 ">
            <div class="banner-section-text text-center">

                <br> <br>

            </div>
        </div>



        @include('site.blocks.panel', [
            'panels' => $block->children,
        ])




    </section>
@endif
