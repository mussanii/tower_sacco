@php 

use App\Models\JobroleCourse;
use App\Models\JobRole;

if (Route::current()->getName() === 'home'){
if(!empty($jobRole)){
$role = JobroleCourse::where('job_role_id', $jobRole)->with('courses')->limit(4)->get()->sortByDesc('courses.position');
$jobr = JobRole::where('id', $jobRole)->first();

}else{

    $role = JobroleCourse::where('job_role_id', Auth::user()->job_role_id)->with('courses')->limit(4)->get()->sortByDesc('courses.position');
}

}else{

if(!empty($jobRole)){
$role = JobroleCourse::where('job_role_id', $jobRole)->with('courses')->paginate(8);
$jobr = JobRole::where('id', $jobRole)->first();

}else{

    $role = JobroleCourse::where('job_role_id', Auth::user()->job_role_id)->with('courses')->paginate(8);
}

   

}
@endphp

@if (Route::current()->getName() === 'home')
<section class="general-section section-filter">
    <div class="container-fluid mobile-container-fluid">
    <div class="col-12 ">
        
            @if((new \Jenssegers\Agent\Agent())->isDesktop())
            <div class="row alignment-class ">
            <div class="col-10">
                <div class="hrHeading">
                    <h2 class="line-header child_header">
                        @if(!empty($jobRole))
                      {{'Courses for '. $jobr->title }}
                        @else
                        {{ $block->translatedinput('title') }}
                        @endif
                    </h2>
                </div>
            </div>
            @if(!empty($jobRole))
            <div class="col-2">
            <form action="{{route('pages',['key' => 'courses'])}}" method="POST">
                @csrf
                    <input type="hidden" value="{{$jobr->id}}" name="category">
                    <button type="submit" class="text-grey full-list full-list-btn">See Full List ></button>
              </form>
            </div>
            @else
                <div class="col-2">
                    @if (Route::current()->getName() === 'home')
                <a href="{{route('pages',['key'=>'courses'])}}" class="text-grey full-list">See Full List > </a> 
                @endif
              </div>
            
          @endif
            </div>
        @else 
        <div class="row ">
        <div class="col-12">
            <div class="hrHeading">
                <h2 class="line-header child_header">
                    @if(!empty($jobRole))
                  {{'Courses for '. $jobr->title }}
                    @else
                    {{ $block->translatedinput('title') }}
                    @endif
                </h2>
            </div>
        </div>
        @if(!empty($jobRole))
        <div class="col-12">
        <form action="{{route('pages',['key' => 'courses'])}}" method="POST">
            @csrf
                <input type="hidden" value="{{$jobr->id}}" name="category">
                <button type="submit" class="text-grey full-list full-list-btn">See Full List ></button>
          </form>
        </div>
        @else
            <div class="col-12">
                @if (Route::current()->getName() === 'home')
            <a href="{{route('pages',['key'=>'courses'])}}" class="text-grey full-list">See Full List > </a> 
            @endif
          </div>
      @endif
    </div>
        @endif
   
	</div>
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="row alignment-class mt-5">
        @if(count($role) > 0)
        @foreach($role as $related)
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            @include('site.includes.components.course-card',['course'=>$related->courses])
        </div>
       @endforeach
        @else
        @if(!empty($jobRole))
        <p class="text-center"> {{'There are no Top Courses for ' . $jobr->title}} </p>
        @else 
        <p class="text-center"> There are no Top Courses for your Job role</p>
        @endif
         @endif
    </div>
    @else 
    <div class="row mt-3">
        @if(count($role) > 0)
        @foreach($role as $related)
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            @include('site.includes.components.course-card',['course'=>$related->courses])
        </div>
       @endforeach
        @else
        @if(!empty($jobRole))
        <p class="text-center"> {{'There are no Top Courses for ' . $jobr->title}} </p>
        @else 
        <p class="text-center"> There are no Top Courses for your Job role</p>
        @endif
         @endif
    </div>
    @endif

</div>
</section>
@else 

<section class="general-section" >
    <div class="container-fluid mobile-container-fluid">
        <div class="col-12 ">
            <div class="row alignment-class ">
                
                    <div class="hrHeading">
                        <h2 class="line-header child_header">
                           
                            @if(!empty($jobRole))
                            {{'Courses for '. $jobr->title }}
                              @else
                              {{ $block->translatedinput('title') }}
                              @endif
                           
                        </h2>
                    </div>
        </div>
        </div>
        @if((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="row alignment-class mt-5">
            @if(count($role) > 0)
            @foreach($role as $related)
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                @include('site.includes.components.course-card',['course'=>$related->courses])
            </div>
           @endforeach
            @else
            <p class="text-center"> There are no Top Courses for your Job role</p>
             @endif
        </div>
        @if(count($role) > 12)
    <div class="row justify-content-center  pagination-spacing">{!! $role->links() !!}</div>
     @endif
    
        @else 
        <div class="row mt-3">
            @if(count($role) > 0)
            @foreach($role as $related)
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                @include('site.includes.components.course-card',['course'=>$related->courses])
            </div>
           @endforeach
            @else
            <p class="text-center"> There are no Top Courses for your Job role</p>
             @endif
        </div>
        @endif
    
</div>
</section>
@endif

