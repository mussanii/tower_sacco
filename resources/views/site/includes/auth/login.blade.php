
@if(session()->has('login_error'))
<div class="form-group row">
    <div class="col-md-12">
        <div class="form-check">
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {!! session()->get('login_error') !!}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        </div>
    </div>
</div>
@php session()->forget('login_error') @endphp
@endif

@if(session()->has('login_success'))
@php
if(session()->get('login_success')){
    $flashMessage = session()->get('login_success');
  }else{
      $flashMessage = '';
  }
    
@endphp
    <div class="form-group row">
        <div class="col-md-12">
            <div class="form-check">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                @if($flashMessage)
                {!! $flashMessage['content'] !!}
                @endif
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            </div>
        </div>
    </div>
    @php session()->forget('login_success') @endphp
@endif
<form method="POST" action="{{ route('user.login') }}" id="loginForm">
{{-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}
@csrf
<div class="form-group form-group row justify-content-center">
    <div class="col-md-10">
        <div class="form-check">
            <label>Username or Email  </label>
        <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username"  autocomplete="off" autofocus>
        @error('username')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>
    </div>
</div>

<div class="form-group form-group row justify-content-center form-password">
    <div class="col-md-10">
        <div class="form-check ">
            <label> Password  </label>
        <div class="input-group mb-3">
        <input id="login_password" type="password" class="pass form-control @error('login_password') is-invalid @enderror"  name="login_password">
        <div class="input-group-append pass-view">
        <i class="far fa-eye"></i>
        <i class="far fa-eye-slash" style="display: none;"></i>
        </div>
        @error('login_password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>
        </div>
    </div>
</div>
<div class="form-group row justify-content-center">
    <div class="col">
        <div class="form-check remember-me">
            <input class="form-check-input" type="checkbox" name="remember" id="remember" />
            <label class="form-check-label text-small" for="remember">Remember me</label>
          </div>
    </div>
    <div class="col">
        <a class="form-check-label text-green text-small reset-pass" href="/password/reset">Reset Password</a>
    </div>
  </div>
<div class="form-group row justify-content-center">
    <div class="col-10">
        <button type="submit" class="btn btn-overall btn_login_page float-right">Login <i aria-hidden="true" class="fas fa-long-arrow-alt-right"></i></button>
    </div>
</div>
<div class="form-group row justify-content-center ">
    <div class="col-md-10">
        <p class="want-account ml-4">Don’t have an account? <a class="form-check-label text-green " href="{{route('register')}}">Register</a><p>
    </div>
</div>
</form>
