@if(session()->has('register_error'))
    <div class="form-group row">
        <div class="col-md-12">
            <div class="form-check">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session()->get('register_error') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            </div>
        </div>
    </div>
    @php session()->forget('register_error') @endphp
@endif
@if(session()->has('register_success'))
    <div class="form-group row">
        <div class="col-md-12">
            <div class="form-check">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session()->get('register_success') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            </div>
        </div>
    </div>
    @php session()->forget('register_success') @endphp
@endif
<form method="POST" action="{{ route('user.create') }}" id="registerForm">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="col-md-12">
        <div class="row">

             <div class="col-md 6">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> First Name <span style="color:red">*</span> </label>
                        <input id="first_name" type="text" class="form-control" name="first_name" autocomplete="off" value="{{ old('first_name') }}" >
                        @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
             </div>

            <div class="col-md-6">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Last Name <span style="color:red">*</span> </label>
                        <input id="last_name" type="text" class="form-control" name="last_name"  autocomplete="off" value="{{ old('last_name') }}" >
                        @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

     <div class="col-md-12">
       <div class="row">
          
         <div class="col-md-6">
            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                    <div class="form-check">
                        <label> Phone Number <span style="color:red">*</span></label>
                    <div class="input-group">
                        {{-- <div class="input-group-prepend" style="width: 40%;">
                            <select class="form-control downArrow no-radius-right" aria-label="Country Code" name="mobile_number_country" id="mobile_number_country" data-behavior="customSelect" placeholder="{{__('forms.general.select_country')}}">
                                <option value="">{{__('Select Country')}}</option>
                                @foreach ((new \App\Helpers\Front)->getCountriesList() as $key => $value)
                                    <option value="{{$key}}">{{$value['label']}}</option>
                                @endforeach
                            </select>
                            @error('mobile_number_country')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div> --}}
            
                        <input id="mobile_number" type="text"
                            class="form-control @error('mobile_number') border-red-500 @enderror no-radius-left" name="mobile_number"
                            value="{{ old('mobile_number') }}"   autocomplete="mobile_number" >
                            @error('mobile_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    </div>
                </div>
            </div>

         </div>
           <div class="col-md-6">
            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                    <div class="form-check">
                        <label> Email Address <span style="color:red">*</span></label>
                    <input id="email" type="email" class="form-control" name="registration_email"  autocomplete="off" value="{{ old('registration_email') }}" >
                    @error('registration_email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
            </div>

           </div>
       </div>
     </div>

     <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
              <div class="form-group row justify-content-center">
                  <div class="col-md-12">
                      <div class="form-check">
                          <label> Branch <span style="color:red">*</span></label>
                          <div class="input-group mb-3">
                            <select class="form-control" name="branch" id="branch" >
                                <option value="">{{__('Select Branch')}}</option>
                                @foreach ($branches as $branch)
                                    <option value="{{$branch->id}}">{{$branch->title}}</option>
                                @endforeach
                            </select>
                      @error('branch')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                      </div>
                  </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group row justify-content-center">
                  <div class="col-md-12">
                      <div class="form-check ">
                           <label> Job Role <span style="color:red">*</span></label>
                           <div class="input-group mb-3">
                            <select class="form-control " name="job_role" id="job_role" >
                                <option value="">{{__('Select Job Role')}}</option>
                                @foreach ($roles as $role)
                                    <option value="{{$role->id}}">{{$role->title}}</option>
                                @endforeach
                            </select>
                      @error('job_role')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                  </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
  
      <div class="col-md-12">
          <div class="row">
              <div class="col-md-6">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Password <span style="color:red">*</span></label>
                            <div class="input-group mb-3">
                        <input id="password" type="password"  class="pass form-control" name="password"  autocomplete="new-password" >
                        <div class="input-group-append pass-view">
                        <i class="far fa-eye"></i>
                        <i class="far fa-eye-slash" style="display: none;"></i>
                        </div>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                        </div>
                    </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check ">
                             <label> Confirm Password <span style="color:red">*</span></label>
                          <div class="input-group mb-3">
                        <input id="password-confirm" type="password"  class="pass form-control" name="password_confirmation"  autocomplete="new-password" >
                        <div class="input-group-append pass-view">
                        <i class="far fa-eye"></i>
                        <i class="far fa-eye-slash" style="display: none;"></i>
                        </div>
                        @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                    </div>
                </div>
              </div>
          </div>
      </div>
      <div class="col-md-12">
        <div class="form-group  recieve-information">
            <input type="hidden" id="platforminfo"name="register_receive_information" />
            <input type="checkbox"  name="register_receive_course_information" id="checkbox_info" >
            <label for="exampleCheck1" class="form-check-label" style="display: initial">I wish to receive course information and program updates from this site.</label>
            <br>
           
        </div>
      </div>
  
      @if((new \Jenssegers\Agent\Agent())->isDesktop())

      <div class="col-md-12">
          <div class="row mt-5">
              <div class="col-md-6">
                <p class="have-account ml-4" >Already have an account? <a class="form-check-label text-green " href="{{route('login')}}">Log in</a><p>
                    <p class="have-account ml-4" >Already registered but did not activate your account? Click  <a class="form-check-label text-green " href="{{route('resend.code')}}">here</a> to request afresh activation code<p>
              </div>

              <div class="col-md-6">
                <button type="submit" class="btn btn-overall btn_register mr_btn"> Register <i aria-hidden="true" class="fas fa-long-arrow-alt-right"></i></button>
              </div>
          </div>
      </div>
      @endif
    
      @if((new \Jenssegers\Agent\Agent())->isMobile())
      <div class="form-group form-group row">
        <div class="col ">
            <button type="submit" class="btn btn-overall btn_mobile_register mr_btn"> Register <i aria-hidden="true" class="fas fa-long-arrow-alt-right"></i></button>
        </div>
    </div>
    <div class="form-group form-group row justify-content-center ">
        <div class="col-md-10">
            <p class="have-account ml-4" >Already have an account? <a class="form-check-label text-green " href="{{route('login')}}">Log in</a><p>
            <p class="have-account ml-4" >Already registered but did not activate your account? Click  <a class="form-check-label text-green " href="{{route('resend.code')}}">here</a> to request afresh activation code<p>
               
        </div>
    </div>

      @endif
     
    
</form>
