<div class="course-card mt-3 sessions-card">
    <article>
      <div class="thumbnail">
        @if( $item->hasImage('blog_image'))
          <img src="{{$item->image("blog_image", "default")}}" alt="{{$item->title}}" />
          <div class="overlay">
            <div class="overlay-inside" data-toggle="tooltip" data-placement="top" title="{{$item->title}}">
              <i class="fas fa-share"></i>
            </div>
          </div>
          @endif
      </div>
  
      <div class="course-card-content session-card-content blog-content">
       

        <p class=" text-grey session-card-calendar">
          
          <span class="session-calendar"> <i class="fa fa-calendar" aria-hidden="true"></i> {{Carbon\Carbon::parse($item->published_at)->isoFormat('MMMM Do YYYY')}} </span>
          
         
        </p>
        <p class="title ">{{ $item->title}}</p>
          <?php
          $str = $item->description;
          if (strlen($str) > 120) {
            $str = substr($str, 0, 120) . '...';
          }
       
          ?>

          {!! $str !!}
        
       
        <p class="readmore ">
          <a href="{{route('blog.details',[$item->id])}}" class="btn btn-overall btn_more_details">Read More </a>
        </p>
       
      </div>

    </article>
  </div>
  

  