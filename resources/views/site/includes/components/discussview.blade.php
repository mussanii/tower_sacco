{{-- discussion index --}}
<div id="reloadComments">
@foreach ($pageItem->comments->take(4) as $comments)
<div class="container" style="margin-top:1rem" >
    <div class="main d-flex">
        <div class="card" style="width: 100%;border: 0px;">
            <div class="row no-gutters">
                <div class="col-2 col-md-1">
                    @if($comments->user->profile_pic !=null)
                    @php $ppic = $comments->user->profile_pic; @endphp
                  @else
                    @php $ppic = 'images.jpg'; @endphp
                  @endif
                    <img class="card-img avatar-small rounded-circle disc-img" src="{{ asset('uploads/'.$ppic) }}" alt="avatar">
                </div>
                <div class="col-10">
                    <div class="card-body card-small" style="padding-bottom: 0;">
                        <div class="card-text">
                            <p style="color:#424242; font-weight:bold;margin-bottom:7px;">{{$comments->user->name}} &nbsp; <small style="font-size:11px">{{$comments->created_at->diffForHumans()}}</small></p>
                            <p>{{$comments->comment}}</p>
                        </div>
                    </div>
                </div>
                <div class="replies" style="">
                    <small class="float-left">
                        <span title="Likes"  onClick="saveLikeDislike({{$comments->id}},'like')" class="mr-2  mr-1-sm" >
                            <i class="fa fa-thumbs-up"></i> <span class="like-count" id="like-{{$comments->id}}" style="font-size:14px">{{ $comments->likes() }}</span>
                        </span>
                        <span title="Dislikes"  onClick="saveLikeDislike({{$comments->id}},'dislike')" class="mr-2  mr-1-sm" >
                           <i class="fa fa-thumbs-down"></i> <span class="dislike-count" id="dislike-{{$comments->id}}" style="font-size:14px">{{ $comments->dislikes() }}</span>
                        </span>
                        <a href="javascript:;" class="replylink" onclick="ShowAndHide({{$comments->id}})">Reply</a>
                    </small>
                </div>
            </div>
            <div class="row">                
                @if(count($comments->replies) > 0)
                <div class="replieslink mt-2"  id="replies-link-{{$comments->id}}">
                    <small class="float-left" style="margin-left: 2rem">
                        <a href="javascript:;" onclick="showReplies({{$comments->id}})" id="showRepliesLink"> View Replies</a>    
                    </small>
                </div>
                @endif
            </div>  
            {{-- reply form--}}
            
            <div class="container-fluid" id="SectionName-{{$comments->id}}" style="display: none;">
                <div class="main  d-flex" >
                    <div class="card " style="border: 0px;width:100%">
                        <div class="row no-gutters">
                            <div class="col-2 col-md-1" >
                                {{-- kutakua na if statement ya kama mtu ako na image kwa db --}}
                                <img class="card-img avatar-small rounded-circle disc-img" src="{{ asset('uploads/images.jpg') }}" alt="avatar">
                            </div>
                            <div class="col-10">
                                <form method="POST" action="{{ route('replies.store')}}" id="reply-discussion">
                                   
                                    <div class="card-body card-small">
                                        {{-- <h5 class="card-title">TITLE</h5> --}}
                                        <div class="card-text">
                                            <input type="text" name="id" value="{{$comments->id}}" hidden id="replycomment_id">
                                            {{-- <input type="text"  class="form-control text-input" placeholder="What are your thoughts?" name="reply"> --}}
            
                                            <textarea id="replycomment_reply" class="form-control text-input" name="reply" placeholder="What are your thoughts?" data-emojiable="true" data-emoji-input="unicode"></textarea>
                                            {{-- <textarea class="form-control "name="reply" id="comment" rows="1" cols="76" placeholder="What are your thoughts?" style="border-top:0px;border-left:0px;border-right:0px;"></textarea>                         --}}
                                        </div>
                                        <div class="submitbuttons">
                                            <button type="button" class="btn btn-outline-dark btn-cancel" onclick="ShowAndHide({{$comments->id}})">Cancel</button>
                                            <a href="javascript:;" onclick="submitReply()"  class="button button-submit"> Submit Comment</a>    
                                            {{-- <a href="javascript: submitform()">
                                            <button type="button" class="button button-submit" </button> --}}
                                        </div>                                       
                                    </div>                    
                                </form>                  
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--end reply form --}}
            {{-- @include('site.includes.components.replies',['comment'=>$comments]) --}}
            
            <div id="replySection-{{$comments->id}}">
            @unless(empty($comments->replies))
            <div class="container" id="sectionReplies-{{$comments->id}}" style="display: none; margin-left:50px;">
            @foreach($comments->replies as $reply)

                <div class="main  d-flex" >
                    <div class="card " style="width:100%;border: 0px;">
                        <div class="row no-gutters">
                            <div class="col-2 col-md-1" >
                                @if($reply->user->profile_pic !=null)
                                @php $ppic = $reply->user->profile_pic; @endphp
                              @else
                                @php $ppic = 'images.jpg'; @endphp
                              @endif
                                {{-- kutakua na if statement ya kama mtu ako na image kwa db --}}
                                <img class="card-img avatar-small rounded-circle disc-img" src="{{ asset('uploads/'.$ppic) }}" alt="avatar">
                            </div>
                            <div class="col-10">         
                                    <div class="card-body card-small" style="padding-bottom:0">            
                                        <div class="card-text">
                                            <p style="color:#424242; font-weight:bold;margin-bottom:7px">{{$reply->user->name}} &nbsp; <small style="font-size:11px">{{$reply->created_at->diffForHumans()}}</small></p>  
                                            <p>{{$reply->reply}}</p>                                                         
                                        </div>                            
                                    </div>                                    
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

            @endunless  
    </div>         
        </div>
    </div>
    
</div>


@endforeach

@if(count($pageItem->comments) > 4)

<div class="col-12">
    
        <a href="javascript:;" onclick="loadMore({{$pageItem->id}})"  class="button button-submit" style="margin-left:50%;margin-top: 50px; padding-left:30px;padding-right:30px"> Load More</a>
   
     
</div>

@endif
</div>



