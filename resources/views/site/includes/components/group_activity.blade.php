<div class="col-md-12 no-padding-left">
    <form action="{{ route('group.activity.create') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card shadow connectCard">
            <div class="card-body connectCardBody">
                <div class="">
                    <div class="connectImgSection">
                        <div class="connectImgLeft">
                            @if (Auth::user()->profile_pic)
                                <img src="{{ asset('uploads/' . Auth::user()->profile_pic) }}"
                                    class="img-fluid connectImg" />
                            @else
                                <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid connectImg" />
                            @endif
                        </div>
                        <div class="connectRight" style="margin-left:10px">
                            <div class="">
                                <input type="hidden" value="{{ $group->id }}" name="group">

                                <p class="lead emoji-picker-container"> <textarea type="text" name="body"
                                        class="connectInput connect-font form-control" placeholder="Whats new?"
                                        id="example1" data-emojiable="true" data-emoji-input="unicode"></textarea></p>
                            </div>
                            <div class="card-footer">
                                <div>
                                    <label for="imageInput"><img src="{{ asset('svgs/images.svg') }}"
                                            class="img-fluid inputImg" width="20" /></label>
                                    <input type="file" name="images[]" id="imageInput" multiple>
                                    <label for="fileInput"><img src="{{ asset('svgs/attach.svg') }}"
                                            class="img-fluid inputImg" width="20" /></label>
                                    <input type="file" name="files[]" id="fileInput" multiple>

                                    <div id="file-upload-filename" style="font-size: 12px; color:#6bc7b8"></div>
                                </div>
                                <div>
                                    <span><button type="submit" class="btn connect-button">Post</button></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @unless(empty($activities))
        @foreach ($activities as $activity)
            <div class="card shadow connectCard mt-3">
                <div class="card-body d-flex connectCardBody">
                    <div class="d-flex">
                        <div class="commentImg">
                            @if ($activity->user->profile_pic)
                                <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                    class="img-fluid connectImg" />
                            @else
                                <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid connectImg" />
                            @endif
                        </div>
                        <div class="upperRow">
                            <div class="commentsContainer">
                                <div class="upperCommentRow">
                                    <span class="name">
                                        {{ $activity->user->username }}
                                        <small class="time">
                                            {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }}
                                        </small>
                                    </span>
                                    {{-- <span class="time">
                                        {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }}
                                    </span> --}}
                                </div>
                                <div class="commentMain">
                                    <p class="connect-font">{{ $activity->body }} </p>

                                    @unless(empty($activity->images))
                                        <div class="commentImgInner">

                                            @foreach (json_decode($activity->images) as $image)
                                                <img src="{{ asset('Connect_Groups/activity/' . $image) }}"
                                                    class="img-fluid main-Commentimg" />
                                            @endforeach
                                        </div>
                                    @endunless
                                    @unless(empty($activity->attachments))
                                        <div class="commentAttachInner">
                                            @foreach (json_decode($activity->attachments) as $attachment)
                                                <a href="{{ asset('Connect_Groups/activity/' . $attachment) }}"
                                                    target="_blank">Download Attachment</a>
                                            @endforeach
                                        </div>
                                    @endunless
                                    <p></p>
                                </div>


                                <div class="d-flex align-items-center" style="">
                                    <small class="float-left">
                                        <span title="Likes" class=""
                                            onclick="sendLike({{ $activity->id }}, 'like')">
                                            <i class="fa fa-thumbs-up" aria-hidden="true"></i> <span
                                                class="activity-like-count " id="like-{{ $activity->id }}"
                                                style="font-size:14px">{{ $activity->reactions->sum('likes') }}</span>
                                        </span>
                                        <span title="Dislikes" class=""
                                            onclick="sendLike({{ $activity->id }}, 'dislike')">
                                            <i class="fa fa-thumbs-down" aria-hidden="true"></i> <span
                                                class="activity-like-count " id="dislike-{{ $activity->id }}"
                                                style="font-size:14px">{{ $activity->reactions->sum('dislikes') }}</span>
                                        </span>
                                        {{-- <a href="javascript:;"onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');" class="replylink" >Reply</a> --}}
                                        <span>
                                            <img src="{{ asset('svgs/comment.svg') }}" id="" class="img-fluid inputImg"
                                                onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');"
                                                width="20" /><span
                                                class="like-count">{{ $activity->replies->count() }}</span>
                                        </span>

                                        </form>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('group.reply.create', [$activity->id]) }}" method="POST"
                        enctype="multipart/form-data" style="width: 100%">
                        @csrf
                        <div class="container-fluid mt-3" id="reply-{{ $activity->id }}" style="display: none;">
                            <div class="main  d-flex">
                                <div class="card " style="border: 0px;width:100%">
                                    <div class="row no-gutters">
                                        <div class="col-2 col-md-1">
                                            @if ($activity->user->profile_pic)
                                                <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                                    class="card-img avatar-small rounded-circle disc-img" />
                                            @else
                                                <img src="{{ asset('uploads/images.jpg') }}"
                                                    class="card-img avatar-small rounded-circle disc-img" />
                                            @endif
                                        </div>
                                        <div class="col-10">

                                            <div class="card-body card-small">
                                                <div class="card-text">
                                                    <input type="text" name="id" value="5" hidden="" id="replycomment_id">
                                                    <textarea id="replycomment_reply" class="form-control text-input"
                                                        name="comment" placeholder="Reply" data-emojiable="true"
                                                        data-emoji-input="unicode"></textarea>
                                                </div>
                                                <label for="imageInput"><img src="{{ asset('svgs/images.svg') }}"
                                                        class="img-fluid inputImg" width="20" /></label>
                                                <input name="images[]" type="file" id="imageInput" multiple>
                                                <label for="gifInput"><img src="{{ asset('svgs/gif.svg') }}"
                                                        class="img-fluid inputImg" width="20" /></label>
                                                <input name="gifs[]" type="file" id="gifInput" multiple>

                                                <div class="submitbuttons">
                                                    <button type="button" class="btn btn-outline-dark btn-cancel"
                                                        onclick="showHide('reply-{{ $activity->id }}')">Cancel</button>
                                                    <button type="submit" class="btn connect-button">Reply</button>
                                                    {{-- <a href="javascript:;" onclick="submitReply()"
                                                        class="button button-submit"> Submit Comment</a> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card-body" id="response-{{ $activity->id }}" style="display: none">
                        {{-- <summary>Show Replies</summary> --}}
                        <span class="showReplies">
                            @include('site.pages.connect.activity_replies')
                        </span>
                    </div>
                </div>
            </div>
        @endforeach
    @endunless
</div>
