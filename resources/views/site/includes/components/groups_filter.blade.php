@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="container-fluid ">
        <div class="row justify-content-center">
            <div class="card resource-filter group-filter mb-3 mt-4">
                <div class="col-12">
                    <div class="row justify-content-center">

                        <div class="col-12">
                            <div class="row">
                                <div class="col-2">
                                    <a href="{{ route('group.form') }}"
                                        class="btn btn-overall btn_join_community btn_create_group "> <i
                                            class="fa fa-plus"></i> Create Group </a>
                                </div>
                                <div class="col-10">
                                    <form action="{{ route('pages', ['key' => $key]) }}" method="POST">
                                        @csrf
                                        <div class="row justify-content-center">
                                            <div class="col-8 no-padding-right ">

                                                <input type="hidden" value="{{ $key }}" placeholder="search"
                                                    name="key">

                                                <div class="input-group">
                                                    <span class="input-group-append">
                                                        <button
                                                            class="btn btn-outline-secondary border-right-0 border mt-4 mb-4"
                                                            type="button">
                                                            <i class="fa fa-search"></i>
                                                        </button>
                                                    </span>
                                                    <input class="form-control py-2 border-left-0 border mt-4 mb-4"
                                                        type="text" value="" placeholder="search"
                                                        id="example-search-input" name="search">

                                                </div>
                                            </div>
                                            <div class="col-4 ">
                                                <button type="submit"
                                                    class="btn btn-overall btn_black_bg mt-4 mb-4">Search</button>
                                                <button type="reset" class="btn btn-overall btn_cancel mt-4 mb-4"
                                                    onClick="window.location.href=window.location.href">Reset</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@else
    <div class="container-fluid">

        <div class="card resource-filter mb-3 mt-3">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <a href="{{ route('group.form') }}"
                            class="btn btn-overall btn_join_community btn_create_group "> <i class="fa fa-plus"></i>
                            Create Group </a>
                    </div>
                    <div class="col-12">
                        <form action="{{ route('pages', ['key' => $key]) }}" method="POST">
                            @csrf

                            <input type="hidden" value="{{ $key }}" placeholder="search" name="key">
                            <div class="row">

                                <div class="col-12 ">

                                    <input class="form-control mt-3 mb-3" name="search">
                                </div>
                                <div class="col-12 ">

                                    <button type="submit"
                                        class="btn btn-overall btn_black_bg mb-3 no-left">Search</button>
                                    <button type="reset" class="btn btn-overall btn_cancel mb-3"
                                        onClick="window.location.href=window.location.href">Reset</button>
                                </div>

                                <div class="col-12">

                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>

        </div>
    </div>
@endif
