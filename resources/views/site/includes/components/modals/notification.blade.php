@php 
    $flashMessage = Session::get('redirectMessage');
@endphp    
<div class="modal fade bd-example-modal-lg" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content message-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="notificationModalLabel">{{ $flashMessage['title'] }}</h5>
            </div>
            <div class="clearfix">
                <div class="divider div-dark div-dot-arsenic"></div>
            </div>
            <div class="modal-body">
                {!! $flashMessage['content'] !!}
            </div>
            <div class="modal-footer">
                @if(isset($flashMessage['action']))
                <a href="{{ $flashMessage['link'] }}" class="btn btn-action">
                    {{ $flashMessage['action'] }}
                </a>
                @else
                <button type="button" class="btn dismiss-modal modal-accept"  data-dismiss="modal"  onclick="document.getElementById('notificationModal').style.display='none'">Dismiss</button>
                @endif
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
