
<div class="modal fade bd-example-modal-lg" id="ticketModal" tabindex="-1" role="dialog" aria-labelledby="ticketModal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content ticket-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="ticketModalLabel">Need Help?</h5>
            </div>
            <div class="clearfix">
                <div class="divider div-dark div-dot-arsenic"></div>
            </div>
            <div class="modal-body">
            <iframe src="https://www.farwell-consultants.com/ticket/index.php/customers/new_customer/Erevuka" style="width:100%;height:100%;"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dismiss-modal modal-accept"  data-dismiss="modal" onclick="document.getElementById('ticketModal').style.display='none'">Dismiss</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->