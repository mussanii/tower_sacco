<div class="container-fluid">
<div class="video-wrapper">
               
    <video controls playsinline muted id="bgresource">
        <source src="@if(!empty($item->file("resource_video"))){{$item->file("resource_video")}} @else {{asset('resources/video_file/'.$item->video_file)}} @endif" type="video/mp4">
    </video>

</div>
</div>