      @php 
 use App\Models\Menus;

$sidemenus = Menus::where('menu_type',2)->published()->orderBy('position', 'asc')->get();
    @endphp
      
      
      <ul class="nav nav-pills course-tabs" role="tablist">
        @foreach($sidemenus as $el)
       

        <li class="nav-item">
          @if (\Route::current()->getName() == $el['key'] || (strpos(\Request::url(), $el['key']) !== false))
        <a class="nav-link active first-tab" href="{{route($el['key'])}}/">{{$el['title']}}</a>
        @else
        <a class="nav-link first-tab" href="{{route($el['key'])}}/">{{$el['title']}}</a>
        @endif
      </li>



        @endforeach

   
      </ul>