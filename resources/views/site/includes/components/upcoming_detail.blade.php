@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <section class="general-section">
        <div class="col-md-12 ">

            <div class="row alignment-class-sessions">
                <div class="col-2  no-padding-right">
                    <b>Topic </b>
                </div>

                <div class="col-8 no-padding-left">
                    <p>{{ $pageItem->topic }} </p>
                </div>
            </div>

            <div class="row alignment-class-sessions">
                <div class="col-2  no-padding-right">
                    <b>Time </b>
                </div>

                <div class="col-8 no-padding-left">
                    <p>{{ Carbon\Carbon::parse($pageItem->start_time)->isoFormat('MMMM Do YYYY H A') }} </p>
                </div>
            </div>

            <div class="row alignment-class-sessions">
                <div class="col-12 col-md-2  no-padding-right">
                    <b>Description </b>
                </div>

                <div class="col-12 col-md-8 no-padding-left">
                    <p>{!! $pageItem->agenda !!}</p>
                </div>
            </div>

            <div class="row alignment-class-sessions">
                <div class="col-2  no-padding-right">
                    <b>Meeting ID </b>
                </div>

                <div class="col-7 no-padding-left">
                    <p>{{ $pageItem->meeting_id }}</p>
                </div>
            </div>

            <div class="clearfix">
                <br />
            </div>


            <div class="row">
                <div class="col-12 col-md-10 offset-md-2">
                    <h4> To Join the Session </h4>
                    <p>Join from a PC, Mac, iPhone or Android device:</p>

                    <a href="{{ $pageItem->join_url }}" class="btn btn-overall btn_register" target="_blank">
                        Join the Meeting
                    </a>


                </div>
            </div>
        </div>

    </section>
@else
    <section class="general-section">
        <div class="col-md-12 ">

            <div class="row alignment-class-sessions">
                <div class="col-2  no-padding-right">
                    <b>Topic </b>
                </div>

                <div class="col-8 no-padding-left">
                    <p>{{ $pageItem->topic }} </p>
                </div>
            </div>

            <div class="row alignment-class-sessions">
                <div class="col-2  no-padding-right">
                    <b>Time </b>
                </div>

                <div class="col-8 no-padding-left">
                    <p>{{ Carbon\Carbon::parse($pageItem->start_time)->isoFormat('MMMM Do YYYY H A') }} </p>
                </div>
            </div>

            <div class="row alignment-class-sessions">
                <div class="col-12 col-md-2  no-padding-right">
                    <b>Description </b>
                </div>

                <div class="col-12 col-md-8 no-padding-left">
                    <p>{!! $pageItem->agenda !!}</p>
                </div>
            </div>

            <div class="row alignment-class-sessions">
                <div class="col-5  no-padding-right">
                    <b>Meeting ID </b>
                </div>

                <div class="col-6 no-padding-left">
                    <p>{{ $pageItem->meeting_id }}</p>
                </div>
            </div>

            <div class="clearfix">
                <br />
            </div>


            <div class="row">
                <div class="col-12 col-md-10 offset-md-2">
                    <h4> To Join the Session </h4>
                    <p>Join from a PC, Mac, iPhone or Android device:</p>

                    <a href="{{ $pageItem->join_url }}" class="btn btn-overall btn_register" target="_blank">
                        Join the Meeting
                    </a>

                </div>
            </div>
        </div>

    </section>
@endif
