@extends('layouts.app')

@section('title','My Courses')

@section('content')
<div class="courses-page">
 
  @include('site.includes.components.parallax',[
  'image'=>asset("images/banners/profile_header.png"),
  'text'=>'Connect'
  ])

  @component('site.includes.components.breadcrumbs')
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{{ route('community') }}">Our Reach</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
      <a href="{{ url()->current() }}" class="active">{{$name}}</a>
    </li>
  </ol>
  @endcomponent

  @if(Session::has('course_success'))
    <script>
    jQuery(document).ready(function($){
      console.log("it has");
     $("#CourseSuccess").addClass('show');
  });
  </script>
  @elseif(Session::has('course_errors'))
  <script>
  jQuery(document).ready(function($){
   $("#CourseErrors").addClass('show');
  });
  </script>
  @else
      {{-- <script>
  window.addEventListener('load', function() {
      if(!window.location.hash) {
          window.location = window.location + '#/';
          window.location.reload();
      }
  });
  </script> --}}
  @endif
  <div class="clearfix">
    <br /> 
</div>

@if((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="col-lg-12  col-md-12 col-sm-12 col-12" >
      
  <div class="row">

    <div class="col-9">

    </div>

    <div class="col-2 pr-5">
    

    </div>
  </div>
</div>
<div class="col-md-12">     
    <div class="row alignment-class-connect">
      <div class="col-12">
        <div class="d-flex flex-row-reverse" >
          <my-community-filter
          action="{{route('myCommunity', ['name'=>$name])}}/"
          requests = "{{json_encode($receive)}}"
          >
          </my-community-filter>

        </div>
      
      </div>
      <div class="col-3">
          @include('site.includes.components.connect.side_menu')
      </div>
        <div class="col-md-9 col-12">
         
          <div class="card connect-card">
            <article>
             <h1><b>Participants from {{$name}}</b> &nbsp; &nbsp; <img src="{{asset('images/'.$flag)}}" width="40px"></h1>
             <p>You are welcome to connect with all participants from {{$name}} on this page. View their individual profiles, explore what each participant is working on, see which groups they are part of and their activities in the project. 
               You are welcome to connect by messaging each other on the platform or contacting each other by phone or Whatsapp.</p>
               
               <p class="country-numbers"><i class="fa fa-users"></i> {{sprintf("%02d",count($licenses))}} participants</p>

              @if(!count($licenses))
      
              <div class="text-center col-12">
                  <br /><br /><br />
                  <p>
                      There are no participants from this country.
                  </p>
                  
                
              </div>
              @endif

            <div class="row my-course">
              @foreach($licenses as $license)

              @php

              if(isset($license->first_name)){  
              $name = ucwords($license->first_name .' '.$license->last_name);
              }else if(isset($license->username)){
              $name = $license->username;
              }else{
              $name = 'name';
              }

               if(empty($license->profile_pic)){
                $image = '/images/profile/images.jpg';
              }else{
                $image = '/uploads/'.$license->profile_pic;
              }
              if(empty($license->affiliation)){
                $position = 'Affiliation';
              }else{
                $position = $license->affiliation;
              }

              $u_agent = $_SERVER['HTTP_USER_AGENT'];
              $phone_code = $license->phone_locale;
              
              $phone_number = '+'.$phone_code.''.$license->phone;

              if(preg_match('/Firefox/i',$u_agent))
              {
                  $whatsapp = "https://web.whatsapp.com/send?phone=".$phone_number;
              }else{
                $whatsapp = "https://api.whatsapp.com/send?phone=".$phone_number;
              }

             @endphp
            <my-community title="{{$name}}" image="{{$image}}"  position= "{{$position}}" status="{{$license->getCommunityStatus($license->id)}}" action="{{$license->getCommunityAction($license->id)}}"  whatsapp="{{$whatsapp}}" email={{$license->email}} superpower="{{$license->super_power}}" contactconsent="{{$license->contact_consent}}" connectprofile="{{route('connect.profile',[$license->id])}}"></my-community>

            @endforeach
            </div>
            </article>
        </div>
         
        
    </div>
    
</div>


@else
<div class="col-md-12">     
    <div class="row ">
      <div class="col-12">
        @include('site.includes.components.connect.side_menu')
      </div>

      <div class="card connect-card">
        <article>
         <h1><b>Participants from {{$name}}</b> &nbsp; &nbsp; <img src="{{asset('images/'.$flag)}}" width="40px"></h1>
         <p>You are welcome to connect with all participants from {{$name}} on this page. View their individual profiles, explore what each participant is working on, see which groups they are part of and their activities in the project. 
           You are welcome to connect by messaging each other on the platform or contacting each other by phone or Whatsapp.</p>
           
           <p class="country-numbers"><i class="fa fa-users"></i> {{sprintf("%02d",count($licenses))}} participants</p>

          @if(!count($licenses))
  
          <div class="text-center col-12">
              <br /><br /><br />
              <p>
                  There are no participants from this country.
              </p>
              
            
          </div>
          @endif

        <div class="row my-course">
          @foreach($licenses as $license)

          @php

          if(isset($license->first_name)){  
          $name = ucwords($license->first_name .' '.$license->last_name);
          }else if(isset($license->username)){
          $name = $license->username;
          }else{
          $name = 'name';
          }

           if(empty($license->profile_pic)){
            $image = '/images/profile/images.jpg';
          }else{
            $image = '/uploads/'.$license->profile_pic;
          }
          if(empty($license->affiliation)){
            $position = 'Affiliation';
          }else{
            $position = $license->affiliation;
          }

          $u_agent = $_SERVER['HTTP_USER_AGENT'];
          $phone_code = $license->phone_locale;
          
          $phone_number = '+'.$phone_code.''.$license->phone;

          if(preg_match('/Firefox/i',$u_agent))
          {
              $whatsapp = "https://web.whatsapp.com/send?phone=".$phone_number;
          }else{
            $whatsapp = "https://api.whatsapp.com/send?phone=".$phone_number;
          }

         @endphp
        <my-community title="{{$name}}" image="{{$image}}"  position= "{{$position}}" status="{{$license->getCommunityStatus($license->id)}}" action="{{$license->getCommunityAction($license->id)}}"  whatsapp="{{$whatsapp}}" email={{$license->email}} superpower="{{$license->super_power}}" contactconsent="{{$license->contact_consent}}" connectprofile="{{route('connect.profile',[$license->id])}}"></my-community>

        @endforeach
        </div>
        </article>
    </div>
        
        
    </div>
    
</div>
@endif

</div>
<div class="clearfix"><br /></div>
@endsection

@section('js')
<script>
  $(document).ready(function() {

// Get current page URL
var url = window.location.href;

// remove # from URL
url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

// remove parameters from URL
url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

// select file name
url = url.split('/')[4];


// console.log(url);

// Loop all menu items
$('.list-group .list-nav').each(function(){

 // select href
 var href = $(this).find('a').attr('href');

 link = href.split('/')[4];


 console.log(link);
 
 // Check filename
 if(link === 'community'){

  // Add active class
  $(this).addClass('active');
 }
});
});
</script>

@endsection