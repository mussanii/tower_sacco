@unless(empty($activity->replies))
    @foreach ($activity->replies as $reply)
        <div class="card-body d-flex flex-row pt-0" id="response-{{ $activity->id }}">
            <div class="commentImg">
                @if ($reply->user->profile_pic)
                    <img src="{{ asset('uploads/' . $reply->user->profile_pic) }}" class="img-fluid connectImg" />
                @else
                    <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid connectImg" />
                @endif
            </div>
            <div class="upperRow">
                <div class="commentsContainer">
                    <div class="responseCommentRow">
                        <span class="name">
                            {{ $reply->user->username }}
                            <small class="time">
                                {{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}
                            </small>
                        </span>
                        @if ($reply->user_id == Auth::user()->id)
                            <div class="dropdown">
                                <i id="dropdownMenuButton" class="fa fa-ellipsis-h" data-toggle="dropdown"></i>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item edit-item" href="#"><i class="fas fa-edit"
                                            data-toggle="modal" data-target="#practice_modal-{{ $reply->id }}"><span
                                                class="icon-text">Edit Post</span></i></a>
                                    <a class="dropdown-item delete-item"
                                        href="{{ route('connect.reply.update', [$reply->id]) }}"
                                        onclick="event.preventDefault();
                                                                            document.getElementById('delete-form-{{ $reply->id }}').submit();">
                                        @if (count($reply->replyReplies) == 0)
                                            <i class="fas fa-trash" data-toggle="tooltip" data-placement="bottom"
                                                title="Delete">
                                                <span class="icon-text">Delete Post</span>
                                            </i>
                                        @endif
                                    </a>
                                    <form id="delete-form-{{ $reply->id }}" +
                                        action="{{ route('connect.reply.delete', [$reply->id]) }}" method="post">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        @endif
                        {{-- <span class="time">
        {{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}
        </span> --}}
                    </div>
                    <div class="commentMain">
                        <p>{!! $reply->comment !!}</p>
                    </div>


                    @unless(empty($reply->images))
                        <div class="commentImgInner">
                            @foreach (json_decode($reply->images) as $image)
                                <img src="{{ asset('images/forum/' . $image) }}" class="img-fluid main-Commentimg" />
                            @endforeach
                        </div>
                    @endunless


                    @unless(empty($reply->attachments))
                        <div class="commentAttachInner">
                            @foreach (json_decode($reply->attachments) as $attachment)
                                <a href="{{ asset('images/forum/' . $attachment) }}" target="_blank">Download
                                    Attachment</a>
                            @endforeach
                        </div>
                    @endunless
                    <div class="" style="">
                        <small class="float-left">
                            <span title="Likes" class="mr-1-sm"
                                onclick="sendReplyLike({{ $activity->id }}, {{ $reply->id }},'like')">
                                <i class="fa fa-thumbs-up" aria-hidden="true"></i> <span class="activity-like-count "
                                    id="like-reply-{{ $reply->id }}"
                                    style="font-size:14px">{{ $reply->replyReactions->sum('likes') }}</span>
                            </span>
                            <span title="Dislikes" class="mr-1-sm"
                                onclick="sendReplyLike({{ $activity->id }}, {{ $reply->id }},'dislike')">
                                <i class="fa fa-thumbs-down" aria-hidden="true"></i> <span class="activity-like-count "
                                    id="dislike-reply-{{ $reply->id }}"
                                    style="font-size:14px">{{ $reply->replyReactions->sum('dislikes') }}</span>
                            </span>
                            {{-- <a href="javascript:;"onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');" class="replylink" >Reply</a> --}}
                            <img src="{{ asset('svgs/comment.svg') }}" id="" class="img-fluid inputImg"
                                onclick="showHide('response-reply-{{ $reply->id }}')" width="20" /><span
                                class="like-count">{{ $reply->replyReplies->count() }}</span>
                            </form>
                        </small>
                    </div>
                    {{-- <div class="lowerCommentRow">
        <input type="text" name="like" id="likeInput" class="d-none">
        <label for="likeInput">  <img src="{{ asset('svgs/like.svg') }}" class="img-fluid inputImg" width="20"/><span class="like-count">{{ $activity->reactions->sum('likes') }}</span></label>
        <input type="text" name="like" id="DislikeInput" class="d-none">
        <label for="DislikeInput">  <img src="{{ asset('svgs/dislike.svg') }}" class="img-fluid inputImg" width="20"/><span class="dislike-count">{{ $activity->reactions->sum('dislikes') }}</span></label>
        <img src="{{ asset('svgs/comment.svg') }}" id="" class="img-fluid inputImg"onclick="showHide('response-{{ $reply->id }}')"  width="20"/><span class="like-count">{{ $reply->replyReplies->count() }}</span>
    </div> --}}
                </div>
            </div>

        </div>
        @include('site.pages.connect.forum_reply_replies')




        <form id="update-form-{{ $reply->id }}" + action="{{ route('connect.reply.update', [$reply->id]) }}"
            method="post">
            @csrf
            <div class="modal fade edit-modal" id="practice_modal-{{ $reply->id }}" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header edit-modal-header">
                            <p class="modal-title name" id="exampleModalLabel">Edit Post</p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <textarea type="text" name="body" id="name" value="" class="form-control"
                                placeholder="">{!! $reply->comment !!}</textarea>
                        </div>
                        <div class="modal-footer justify-content-end">
                            {{-- <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">Close</button> --}}
                            <button type="submit" class="btn btn-primary">Post</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @endforeach
@endunless
