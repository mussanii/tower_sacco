@extends('layouts.app_no_js')

@section('content')
    @php

    $variant = $pageItem->variant;
    $url = $pageItem->url;
    $linkText = $pageItem->link_text;

    @endphp


    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        @if ($pageItem->hasImage('hero_image'))
            <div class="overlay">
                @if (Auth::check())
                    <section class='device-banner-auth' style="background:url('{{ $pageItem->image('hero_image', 'default') }}');height: auto;
                                        background-position: center;
                                        background-repeat: no-repeat;
                                        background-size: cover;">
                    @else
                        <section class='device-banner' style="background:url('{{ $pageItem->image('hero_image', 'default') }}');height: auto;
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover;">
                @endif
            @else
                <section class='device-banner'>
        @endif
        <div class="row ">
            <div class="col-md-8">
                <div class='device-banner home-banner'>
                    <div class="banner-section-text">
                        <h1><strong>{!! $pageItem->header_title !!}</strong></h1>
                        <p>{!! $pageItem->description !!}</p>
                        @if ($variant && $variant === 'green_bg')
                            @guest
                                <a href="{{ route('login') }}" class="btn btn-overall btn_green_bg">
                                    {{ $linkText }}
                                </a>
                            @else
                                <a href="{{ $url }}" class="btn btn-overall btn_green_bg">
                                    {{ $linkText }}
                                </a>
                            @endif
                        @elseif ($variant && $variant === 'black_bg')
                            @guest
                                <a href="{{ route('login') }}" class="btn btn-overall btn_black_bg">
                                    {{ $linkText }}

                                </a>
                            @else
                                <a href="{{ $url }} " class="btn btn-overall btn_black_bg">
                                    {{ $linkText }}

                                </a>
            @endif
        @elseif ($variant && $variant === 'white_bg_green_border')
            @guest
                <a href="{{ route('login') }}" class="btn btn-overall btn_black_bg">
                    {{ $linkText }}

                </a>
            @else
                <a href="{{ $url }} " class="btn btn-overall btn_white_bg_green_border">
                    {{ $linkText }}

                </a>
                @endif
            @elseif ($variant && $variant === 'white_bg_black_border')
                @guest
                    <a href="{{ route('login') }}" class="btn btn-overall btn_black_bg">
                        {{ $linkText }}

                    </a>
                @else
                    <a href="{{ $url }} " class="btn btn-overall btn_white_bg_black_border">
                        {{ $linkText }}

                    </a>
                    @endif
                @else
                    <a href="{{ $url }}" class="btn btn-overall btn_green_bg">
                        {{ $linkText }}

                    </a>

                    @endif
                    </div>
                    </div>
                    </div>

                    {{-- <div class="col-md-6">
        <div style="margin-right: 2rem;">
          @php $images = $pageItem->files @endphp
          
            @if ($images)
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="false">
              <ol class="carousel-indicators">
                @foreach ($images as $key => $item)
                <li data-target="#carousel-example" data-slide-to="{{$key}}" class="{{ $key == 0 ? ' active' : '' }}"></li>
                
                @endforeach
              </ol> 
              <div class="carousel-inner">

                @foreach ($images as $key => $item)
                @php 
                 $input2 = $item->filename;
                 $type = explode('.', $input2 , 2);
                @endphp 

                 <div class="carousel-item item {{ $key == 0 ? ' active' : '' }}">
                 @if ($type[1] === 'mp4')

                 <video controls playsinline autoplay id="bgvid">
                  <source src="{{asset('storage/uploads/'.$item->uuid)}}" type="video/mp4">
                 </video>

                 @elseif($type[1] === "png" || $type[1] === "jpg")
                  <img src="{{asset('storage/uploads/'.$item->uuid)}}" alt="{{$pageItem->title}}"/>
                 @endif
                </div>
              
                 <div class="carousel-item active">
                  <img src="..." class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                  <img src="..." class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                  <img src="..." class="d-block w-100" alt="...">
                </div> 
                @endforeach
              </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            @else 

           
            @endif
            
        </div>
    </div> --}}

                    </div>
                    {{-- <button onclick="myFunction()" title="" class="btn-arrow arrow bounce d-none d-md-block" aria-hidden="true" style="" id="arrow-button">
    <i class="fas fa-chevron-down"></i>
  </button> --}}
                    </section>
                    </div>
                @else
                    @if ($pageItem->hasImage('hero_image'))
                        <div class="overlay">
                            <section class='device-banner'
                                style="background:url('{{ $pageItem->image('hero_image', 'default') }}');height: auto;
                                                                                                                                                                                                                              background-position: center;
                                                                                                                                                                                                                              background-repeat: no-repeat;
                                                                                                                                                                                                                              background-size: cover;">
                            @else
                                <section class='device-banner'>
                    @endif
                    <div class="row ">
                        <div class="col-sm-10 col-xs-12">
                            <div class='device-banner home-banner'>
                                <div class="banner-section-text">
                                    <h1><strong>{!! $pageItem->header_title !!}</strong></h1>
                                    <p>{!! $pageItem->description !!}</p>
                                    @if ($variant && $variant === 'green_bg')
                                        @guest
                                            <a href="{{ route('login') }}" class="btn btn-overall btn_green_bg">
                                                {{ $linkText }}
                                            </a>
                                        @else
                                            <a href="{{ $url }}" class="btn btn-overall btn_green_bg">
                                                {{ $linkText }}
                                            </a>
                                        @endif
                                    @elseif ($variant && $variant === 'black_bg')
                                        @guest
                                            <a href="{{ route('login') }}" class="btn btn-overall btn_black_bg">
                                                {{ $linkText }}

                                            </a>
                                        @else
                                            <a href="{{ $url }} " class="btn btn-overall btn_black_bg">
                                                {{ $linkText }}

                                            </a>
                                            @endif
                                        @elseif ($variant && $variant === 'white_bg_green_border')
                                            @guest
                                                <a href="{{ route('login') }}" class="btn btn-overall btn_black_bg">
                                                    {{ $linkText }}

                                                </a>
                                            @else
                                                <a href="{{ $url }} " class="btn btn-overall btn_white_bg_green_border">
                                                    {{ $linkText }}

                                                </a>
                                                @endif
                                            @elseif ($variant && $variant === 'white_bg_black_border')
                                                @guest
                                                    <a href="{{ route('login') }}" class="btn btn-overall btn_black_bg">
                                                        {{ $linkText }}

                                                    </a>
                                                @else
                                                    <a href="{{ $url }} " class="btn btn-overall btn_white_bg_black_border">
                                                        {{ $linkText }}

                                                    </a>
                                                    @endif
                                                @else
                                                    <a href="{{ $url }}" class="btn btn-overall btn_green_bg">
                                                        {{ $linkText }}

                                                    </a>

                                                    @endif
                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                    {{-- <button onclick="myFunction()" title="" class="btn-arrow arrow bounce d-none d-md-block" aria-hidden="true" style="" id="arrow-button">
    <i class="fas fa-chevron-down"></i>
  </button> --}}
                                    </section>
                                    </div>
                                    @endif


                                    @if (Auth::user())
                                        <section class="general-section business-general home-section-filter">
                                            <div class="container-fluid">
                                                @include(
                                                    'site.includes.components.home_filter',
                                                    ['jobRoles' => $jobRoles]
                                                )
                                            </div>
                                        </section>
                                    @endif
                                    @if (!Auth::check())

                                    
                                    @else
                                
                                    {!! $pageItem->renderBlocks(true, [], ['jobRole' => $jobRole, 'courseCategories'=> $courseCategories,  'search'=>$search]) !!}

                                    @endif
                                @endsection
                                @section('js')
                                    <script>
                                        function resetForm() {
                                            document.getElementById("homeForm").reset();
                                        }
                                    </script>
                                @endsection
