@extends('layouts.app')

@section('title','Background')

@section('content')
<div class="background-page">
    <section class="general-section business-general top-general" style="margin-bottom:0">
     
        <div class="container-fluid">
            <div class="row alignment-class">
        <div class="col-12 ">
            <div class="hrHeading">
                <h2 class="line-header network_header_inner">
                    {{ $pageItem->title }}
                </h2>
            </div>
                <div class="col-12">
                    <div class="whatYouGetListing ">
                       {!!$pageItem->description!!}
                        
                    </div>
                </div>
             
        </div>
        </div>
        </div>
        
    </section>
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="container-fluid p-0">
    @else 
    <div class="container-fluid">
    @endif
 
        {!! $pageItem->renderBlocks(false) !!}

</div>

</div>
@endsection

@section('js')

<script>
    $(document).ready(function() {
  
  // Get current page URL
  var url = window.location.href;
  
  
  
  // remove # from URL
  url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
  
  // remove parameters from URL
  url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
  
  // select file name
  url = url.split('/')[4];
  
  
  // console.log(url);
  
  // Loop all menu items
  $('.navbar-nav .nav-item').each(function(){
  
   // select href
   var href = $(this).find('a').attr('href');
  
   link = href.split('/')[4];
   
   // Check filename
   if(link === 'managing-my-network'){
  
    // Add active class
    $(this).addClass('active');
   }
  });
  });
  </script>



@endsection