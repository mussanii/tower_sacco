@extends('layouts.app')

@section('title', 'Background')

@section('content')
    <div class="background-page">
        @if ($pageItem->hasImage('hero_image'))
            @php $image = $pageItem->image('hero_image', 'default');
                $text = $pageItem->header_title;
            @endphp
            @include('site.includes.components.parallax', [
                'image' => $image,
                'text' => $text,
            ])
        @endif
        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ url()->current() }}" class="active">{!! $pageItem->title !!}</a>
                </li>
            </ol>
        @endcomponent
        <div class="container-fluid p-0 ">
            {!! $pageItem->renderBlocks(false, [], ['jobRole' => $jobRole]) !!}

        </div>
    @endsection
