<style>

.card-img {
      height: 230px; /* Adjust the height of the image */
      object-fit: cover; /* Ensure the image covers the entire area */
    }
    .date{
      font-size: 13px;
      line-height:19.5px
    }
    .resource-card{
      background: #fff 0 0 no-repeat padding-box;
    box-shadow: 4px 4px 8px #0000004d;
    border: none;
    }
    .card-title{
      font-size: 20px;
    }
    p.card-text{
      font-size: 14px;
    }

</style>

@php
  use Carbon\Carbon;
@endphp
@extends('layouts.app')

@section('title','Background')

@section('content')
<div class="background-page">
    @if( $pageItem->hasImage('hero_image'))
    @php $image = $pageItem->image("hero_image", "default") ; 
    $text = $pageItem->header_title;
  
    @endphp
    @include('site.includes.components.parallax',[
    'image'=> $image,
    'text'=>$text
    ])
    @endif
    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">{!! $pageItem->title !!}</a>
        </li>
    </ol>
    @endcomponent

    
    @if(Auth::user())
    <section class="general-section business-general section-filter courses-filter">
      <div class="container-fluid">
        @include('site.includes.components.policies_filter',['jobRoles' =>$jobRoles, 'key'=>$pageItem->key])
      </div>
    </section>
    @endif
  

        

   
        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        
        <div class="container-fluid ps-5 pl-5 pr-5 ">


        @if($policies->count() > 0)

        @foreach ( $policies as $policy )

       
        <div class="row mb-3">
        <div class="col-md-12">
          <div class="card resource-card">
            <div class="row no-gutters">
              <div class="col-md-3">
                @if($policy->hasImage('resource_image'))
                <img src="{{$policy->image('resource_image','default')}}" class="card-img" alt="{{$policy->title}}">
                @else
                <img src="https://via.placeholder.com/200" class="card-img" alt="Placeholder Image">
    
                @endif
              </div>
              <div class="col-md-9">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-8">
                    <h5 class="card-title"> <a href="{{route('policy.details', $policy->id)}}">{{$policy->title}}</a></h5>
                    </div>
                    <div class="col-md-4">
                    <div class="d-flex justify-content-end">
                    <a href="mailto:support@towersacco.co.ke"  class="btn  btn-overall btn_black_bg mr-5" >Email</a>
                    <a href="{{route('policy.details', $policy->id)}}" target="_blank" class="btn btn-overall btn_cancel" style="background: #EB1B8A; border-color:#EB1B8A">Read More</a>

           
                    {{--@foreach ($policy->documents as $document)
                            @foreach ($document->files as $file)
                                <div class="resource-downloadable-block">
                                <a href="{{ url('storage/uploads/'. $file->uuid ) }}" target="_blank" class="btn btn-overall btn_cancel">Read more</a>

                                </div>
                            @endforeach
                        @endforeach--}}
                
                   <!-- <a href="https://www.towersacco.co.ke/sacco-downloads/" target="_blank" class="btn btn-overall btn_cancel" style="background: #EB1B8A; border-color:#EB1B8A">Visit website</a> -->

                   
                </div>

                    </div>
                  </div>
               
                  <h6 class="date">Uploaded: {{Carbon::parse($policy->created_at)->format('jS F Y')}}</h6>
                  <hr>

                  <?php
        $str = $policy->description;
        if (strlen($str) > 50) {
          $str = truncateString($str, 150);
        }
        
        ?>
                  <p class="card-text hoverable">{!! $str!!}<a href="{{route('policy.details', $policy->id)}}" >Read more</a>
                </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
          
        @endforeach
        @else
        <div class="row mb-3">
        <div class="col-md-12">
          <h3 class="text-center"> There are no resources at the moment. </h3>
        </div>
        </div>
        @endif

        
       



          
    
          
        

        </div>


    
        

        @else

        @endif
        
          
         

     
@endsection
