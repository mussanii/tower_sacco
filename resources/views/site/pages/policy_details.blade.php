
@php
use Carbon\Carbon;
@endphp

@extends('layouts.app_no_js')

@section('title','Blog')


@section('content')
<div class="background-page">

    @php
    $text = $policy->title;
    @endphp

    @include('site.includes.components.parallax',[
    'image'=> asset("images/banners/session_details.png"),
    'text'=> $text
    ])

    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/policies">Policies</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">{{$policy->title}}</a>
        </li>
    </ol>
    @endcomponent

    <div class="clearfix">
        <br /> <br />
    </div>
    <div class="col-md-12">
        <div class="row alignment-class-sessions">
            <div class="col-md-8 col-sm-7 ">
                <div class="row">
                    <div class="col-md-12 col-sm-7  offset-2">

                        <h3 class="event_title">{{$policy->title}}</h3>

                        <p class=" text-grey session-card-calendar">
                         <span class="session-calendar"> <i class="fa fa-calendar" aria-hidden="true"></i> {{Carbon::parse($policy->published_at)->isoFormat('MMMM Do YYYY')}} </span>
                        </p>

                        @if( $policy->hasImage('inner_image'))
                        <img src="{{$policy->image("inner_image", "default")}}" alt="{{$policy->title}}" />

                        @endif

                                                <div id="long-description">
                            <!-- Initially show only the first part of the description -->
                            <div id="long-description-content">
                                {!! substr($policy->long_description, 0, 200) !!} <!-- Display the first 200 characters -->
                            </div>
                            <!-- If the description is longer than 200 characters, show "Read More" button -->
                            @if(strlen($policy->long_description) > 200)
                                <button class="btn btn-overall btn_cancel" id="read-more-btn" onclick="toggleDescription()">Read More</button>
                            @endif
                        </div>

               
                      

                        <div class="clearfix">
                            <br /> <br />
                            <br />
                        </div>
                    </div>
                </div>

            </div>
            
        </div>

    </div>



@endsection

@section('js')
<script>
    function toggleDescription() {
        var longDescription = document.getElementById("long-description-content");
        var btn = document.getElementById("read-more-btn");
        var fullDescription = {!! json_encode($policy->long_description) !!};

        // If description is currently truncated, show full description
        if (longDescription.innerHTML.length < fullDescription.length) {
            longDescription.innerHTML = fullDescription;
            btn.innerText = "Read Less";
           
            btn.className="btn  btn-overall btn_black_bg ";
        } else {
            // If description is currently showing full text, truncate it again
            longDescription.innerHTML = fullDescription.substring(0, 200) + '...'; // Adjust the character limit as needed
            btn.innerText = "Read More";
            btn.className="btn btn-overall btn_cancel";
        }
    }
</script>

@endsection




