@extends('layouts.app')

@section('title', 'My Profile')

@section('css')
    <style>
        @media (max-width:767px) {
            .jarallax-img {
                width: 100%;
                height: 310px;
                object-fit: cover;
            }

            .inline-video-jarallax .content,
            .jarallax .content {
                margin-top: -51%;
                margin-left: 12px;
            }

            .breadcrumb {
                margin-left: 0px;
                margin-top: 0px;
                background-color: transparent;
                padding-left: 0;
            }
        }

        @media (max-width:320px) {

            .inline-video-jarallax .content,
            .jarallax .content {
                margin-top: -58%;
                margin-left: 12px;
            }


        }

    </style>
@endsection

@section('content')
    <div class="courses-page">
        @include('site.includes.components.parallax', [
            'image' => asset('images/banners/course_header.png'),
            'text' => 'My Profile',
        ])
        <div class="col-12">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    @component('site.includes.components.breadcrumbs')
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('home') }}">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ url()->current() }}" class="active">My Profile</a>
                            </li>
                        </ol>
                    @endcomponent
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                    @php
                        $value = (count($completed_complusory) * 100) / count($compulsory);
                        $string = sprintf('%.2f', $value) . '%';
                    @endphp

                    <course-progress class=" d-lg-flex" compulsory="{{ count($compulsory) }}"
                        completed="{{ count($completed_complusory) }}" size="Medium" value="{{ $value }}"
                        string="{{ $string }}" position="inside" color="#ffffff"
                        image="{{ asset('/images/icons/fire.jpg') }}" role="{{ Auth::user()->role }}"></course-progress>

                </div>
            </div>

        </div>



        <div class="profile-row">

            <div class="row">

                <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                    @include('site.includes.components.timeline')

                </div>
                @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                    <div class="offset-md-2 col-lg-10  col-md-10 col-sm-12 col-12">
                        @include(
                            'site.includes.components.user-courses-filter'
                        )
                    </div>
                @endif
            </div>
            @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                <div class="row no-gutters">


                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                        <div class="container-fluid">

                            <!-- Filter section -->
                            <!-- End of filter section -->
                            <!-- Course list -->
                            <div class="tab-content">
                                <div id="home" class="container-fluid tab-pane active">
                                    <div class="course-card detail-card">
                                        <article>
                                            <div class="course-content">
                                                <div class="help">
                                                </div>
                                                @if (!count($licenses))
                                                    <div class="row justify-content-md-center no-gutters">
                                                        <div class="text-center col-12">
                                                            <p>
                                                                Your list of courses is currently empty.
                                                            </p>

                                                        </div>
                                                    </div>
                                                @endif
                                                @php
                                                    $count = 0;

                                                    $today = Carbon\Carbon::today()->format('d-m-y');

                                                @endphp
                                                @foreach ($licenses as $license)
                                                  @php $count ++; 
                                                     $configLms = config()->get("settings.lms.live");   

                                                     
                                                    @endphp
 
                                                    <my-course class="d-none d-lg-flex"
                                                        cert_title="{{ $license->course->name }}"
                                                        title="{{ $license->course->name }}"
                                                        image="{{ $license->course->course_image_uri }}"
                                                        date="{{ Carbon\Carbon::parse($license->enrolled_at)->isoFormat('Do MMMM YYYY') }}"
                                                        status="{{ $license->status }}"
                                                        progress="{{ $license->grade * 100 }}%" prev-score="0"
                                                        current-score="{{ $license->grade }}"
                                                        action="{{ $license->action }}"
                                                        link="{{ $configLms['LMS_BASE'] . '/courses/' . $license->course->course_id . '/courseware' }}"
                                                        start="{{ Carbon\Carbon::parse($license->course->start)->isoFormat('Do MMMM YYYY') }}"
                                                        end="  {{ ($license->completion_date) ? Carbon\Carbon::parse($license->completion_date)->isoFormat('Do MMMM YYYY') : 'In progress '}}  "
                                                        enrolled="{{ $license->getEnrollments($license->course->id) }}"
                                                        owner="{{ Auth::user()->name }}" count="{{ $count }}"
                                                        evaluate="{{ $license->getEvaluationStatus($license->course->id, $license->user->id) }}"
                                                        evalaction="{{ route('course.evaluate', ['courseid' => $license->course->id]) }}"
                                                        today="{{ $today }}"></my-course>
                                                @endforeach

                                            </div>
                                        </article>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            @else
                <div class="row no-gutters">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                        <div class="course-card detail-card">
                            <article>
                                <div class="course-content">
                                    <div class="help">
                                    </div>
                                    @if (!count($licenses))
                                        <div class="row justify-content-md-center no-gutters">
                                            <div class="text-center col-12">
                                                <p>
                                                    Your list of courses is currently empty.
                                                </p>

                                            </div>
                                        </div>
                                    @endif
                                    @php
                                        $count = 0;

                                        $today = Carbon\Carbon::today()->format('d-m-y');

                                    @endphp
                                    @foreach ($licenses as $license)
                                        @php $count ++;
                                         $configLms = config()->get("settings.lms.live");
                                        @endphp

                                        <my-course-mobile class="d-inline-block d-lg-none "
                                            cert_title="{{ $license->course->name }}"
                                            title="{{ $license->course->name }}"
                                            image="{{ $license->course->course_image_uri }}"
                                            date="{{ Carbon\Carbon::parse($license->enrolled_at)->isoFormat('Do MMMM YYYY') }}"
                                            status="{{ $license->status }}" progress="{{ $license->grade * 100 }}%"
                                            prev-score="0" current-score="{{ $license->grade }}"
                                            action="{{ $license->action }}"
                                            link="{{ $configLms['LMS_BASE'] . '/courses/' . $license->course->course_id . '/courseware' }}"
                                            start="{{ Carbon\Carbon::parse($license->course->start)->isoFormat('Do MMMM YYYY') }}"
                                            end="{{ Carbon\Carbon::parse($license->course->end)->isoFormat('Do MMMM YYYY') }}"
                                            enrolled="{{ $license->getEnrollments($license->course->id) }}"
                                            owner="{{ Auth::user()->name }}" count="{{ $count }}"
                                            evaluate="{{ $license->getEvaluationStatus($license->course->id, $license->user->id) }}"
                                            evalaction="{{ route('course.evaluate', ['courseid' => $license->course->id]) }}"
                                            today="{{ $today }}"></my-course-mobile>
                                    @endforeach

                                </div>
                            </article>
                        </div>

                    </div>
                </div>
            @endif

        </div>
    </div>
    <!-- End of future courses list -->

    <div class="clearfix"><br /></div>
@endsection
