@extends('layouts.evaluate')


@section('title','Evaluate')


@section('content')
<div class="courses-page">
  @include('site.includes.components.parallax',[
  'image'=>asset("images/banners/course_header.png"),
  'text'=>'Course Evaluation'
  ])

  @component('site.includes.components.breadcrumbs')
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{{ route('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
      <a href="{{route('course.evaluate',['courseid'=> $id])}}" class="active">Course Evaluation</a>
    </li>
  </ol>
  @endcomponent
  @if(Session::has('redirectMessage'))
      
        <script>
        jQuery(document).ready(function($){
         $("#notificationModal").addClass('show');
      });
        </script>
        @php Session::forget('redirectMessage') @endphp
      @else
            {{-- <script>
        window.addEventListener('load', function() {
            if(!window.location.hash) {
                window.location = window.location + '#/';
                window.location.reload();
            }
        });
      </script> --}}
      @endif
<div class="evaluate-section">
    <div class="row justify-content-center about-row">

      <div class="col-md-12">
      <p style="padding-top:20px; ">Thank you for you for participating in this 5 minute evaluation.  Your responses will directly affect how we undertake similar initiatives in future so thoughtful responses are appreciated.</a></p></div>
        <div class="col-md-12">
          
          <form action="{{route('course.evaluate.store')}}" method="post" id="editEvaluationForm" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <input type="hidden" name="course_id" value="{{$id}}" id="course_ev">
            <section>
                
               @foreach($sections as $section)
                 @if($section->sectionid == 1)
                 <fieldset style="padding:10px;">
                   <label ><b>{{$section->question}}</b></label><br>
                  @if($section->evaluation_options->type === 'bar')
                  <div class="star-rating">
                     <span class="fa fa-star-o fa-4x" data-rating="1"></span>
                     <span class="fa fa-star-o" data-rating="2"></span>
                     <span class="fa fa-star-o" data-rating="3"></span>
                     <span class="fa fa-star-o" data-rating="4"></span>
                     <span class="fa fa-star-o" data-rating="5"></span>
                     <span class="fa fa-star-o" data-rating="6"></span>
                     <span class="fa fa-star-o" data-rating="7"></span>
                     <span class="fa fa-star-o" data-rating="8"></span>
                     <span class="fa fa-star-o" data-rating="9"></span>
                     <span class="fa fa-star-o" data-rating="10"></span>
                     <input type="hidden" name="{{$section->id}}" class="rating-value" value="">
                   </div>

                  @elseif($section->evaluation_options->type === 'text')

                   <textarea class="form-control" rows="3" id="comment" name="{{$section->id}}"></textarea>

                   @else
                   <div class="radio">

                     <label><input type="radio" name="{{$section->id}}" value="{{$section->evaluation_options->value1}}">{{$section->evaluation_options->value1}}</label>
                     </div>
                     <div class="radio">
                     <label><input type="radio" name="{{$section->id}}" value="{{$section->evaluation_options->value2}}">{{$section->evaluation_options->value2}}</label>
                     </div>
                     <div class="radio ">
                     <label><input type="radio" name="{{$section->id}}" value="{{$section->evaluation_options->value3}}">{{$section->evaluation_options->value3}}</label>
                   </div>
                   <div class="radio ">
                   <label><input type="radio" name="{{$section->id}}" value="{{$section->evaluation_options->value4}}" >{{$section->evaluation_options->value4}}</label>
                 </div>
                 @if($section->evaluation_options->value5 != NULL)
                 <div class="radio ">
                 <label><input type="radio" name="{{$section->id}}" value="{{$section->evaluation_options->value5}}">{{$section->evaluation_options->value5}}</label>
               </div>
                @endif

                  @endif
                 @endif
                  </fieldset>
              @endforeach
            </section>

            
           <br>
           <div class="form-group form-group row justify-content-center">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-overall purple">Submit <i aria-hidden="true" class="fas fa-long-arrow-alt-right"></i></button>
        </div>
    </div>
            
          </form>
        </div>
    </div>
</div>

</div>
</div>
<!-- End of future courses list -->

<div class="clearfix"><br /></div>
@endsection
