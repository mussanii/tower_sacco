@extends('layouts.app')

@section('title', 'My Profile')

@section('content')
    <div class="courses-page">
        @include('site.includes.components.parallax', [
            'image' => asset('images/banners/course_header.png'),
            'text' => 'My Profile',
        ])
        <div class="col-12">
            <div class="row">
                <div class="col-6">
                    @component('site.includes.components.breadcrumbs')
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('home') }}">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ url()->current() }}" class="active">My Community</a>
                            </li>
                        </ol>
                    @endcomponent

                    @if (Session::has('course_success'))
                        <script>
                            jQuery(document).ready(function($) {
                                console.log("it has");
                                $("#CourseSuccess").addClass('show');
                            });
                        </script>
                    @elseif(Session::has('course_errors'))
                        <script>
                            jQuery(document).ready(function($) {
                                $("#CourseErrors").addClass('show');
                            });
                        </script>
                    @endif
                </div>

            </div>

        </div>



        <div class="profile-row">

            <div class="row">

                <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                    @include('site.includes.components.timeline')

                </div>

            </div>

            <div class="row no-gutters">


                <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                    <div class="container-fluid">
                        <div class="col-11">
                            <div class="d-flex flex-row-reverse">
                                <my-community-filter action="{{ route('profile.myCommunity') }}/"
                                    requests="{{ json_encode($receive) }}">
                                </my-community-filter>

                            </div>

                        </div>
                        <!-- Filter section -->
                        <!-- End of filter section -->
                        <!-- Course list -->
                        <div class="tab-content">


                            @if (!count($licenses))
                                <div class="row justify-content-md-center no-gutters">
                                    <div class="text-center col-12">
                                        <p>
                                            Your list of courses is currently empty.
                                        </p>

                                    </div>
                                </div>
                            @endif
                            <div class="row my-course" style="border-bottom:none">
                                @foreach ($licenses as $license)
                                    @php
                                        
                                        if (isset($license->user->first_name)) {
                                            $name = ucwords($license->user->first_name . ' ' . $license->user->last_name);
                                        } elseif (isset($license->user->username)) {
                                            $name = $license->user->username;
                                        } else {
                                            $name = 'name';
                                        }
                                        
                                        if (empty($license->user->ppicFilename)) {
                                            $image = '/images/profile/images.jpg';
                                        } else {
                                            $image = '/' . $license->user->ppicFilename;
                                        }
                                        if (empty($license->user->roles->title)) {
                                            $position = 'Position';
                                        } else {
                                            $position = $license->user->roles->title;
                                        }
                                        
                                        $branch = ucfirst(strtolower($license->user->branches->title));
                                        
                                    @endphp
                                    <my-community title="{{ $name }}" image="{{ $image }}"
                                        email="{{ $license->user->email }}" position="{{ $position }}"
                                        status="{{ $license->getCommunityStatus($license->user->id) }}"
                                        action="{{ $license->getCommunityAction($license->user->id) }}"
                                        block="{{ $license->getCommunityBlockAction($license->user->id) }}"
                                        branch="{{ $branch }}"></my-community>
                                @endforeach
                            </div>


                        </div>

                    </div>


                </div>
            </div>


        </div>
    </div>
    <!-- End of future courses list -->

    <div class="clearfix"><br /></div>
@endsection
