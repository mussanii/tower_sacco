@extends('layouts.app')

@section('title', 'My Profile')
@section('css')
    <style>
        @media (max-width:767px) {
            .jarallax-img {
                width: 100%;
                height: 310px;
                object-fit: cover;
            }

            .inline-video-jarallax .content,
            .jarallax .content {
                margin-top: -51%;
                margin-left: 12px;
            }

            .breadcrumb {
                margin-left: 0px;
                margin-top: 0px;
                background-color: transparent;
                padding-left: 0;
            }
        }

        @media (max-width:320px) {

            .inline-video-jarallax .content,
            .jarallax .content {
                margin-top: -57%;
                margin-left: 12px;
            }
        }

    </style>
@endsection
@section('content')
    <div class="courses-page">
        @include('site.includes.components.parallax', [
            'image' => asset('images/banners/course_header.png'),
            'text' => 'My Profile',
        ])
        <div class="col-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    @component('site.includes.components.breadcrumbs')
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('home') }}">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ url()->current() }}" class="active">My Profile</a>
                            </li>
                        </ol>
                    @endcomponent
                </div>
                <div class="col-lg-8 col-md-8 col-12">


                    @php
                        
                        $value = (count($completed_complusory) * 100) / count($compulsory);
                        $string = sprintf('%.2f', $value) . '%';
                    @endphp

                    <course-progress class=" d-lg-flex" compulsory="{{ count($compulsory) }}"
                        completed="{{ count($completed_complusory) }}" size="Medium" value="{{ $value }}"
                        string="{{ $string }}" position="inside" color="#ffffff"
                        image="{{ asset('/images/icons/fire.jpg') }}" role="{{ Auth::user()->role }}">
                    </course-progress>

                </div>
            </div>

        </div>



        <div class="profile-row">

            <div class="row">

                <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                    @include('site.includes.components.timeline')

                </div>

            </div>
            @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                <div class="row no-gutters">
                    <div class="col-12 mt-3 ml-5">
                        <div class="row alignment-class ">
                            <div class="col-12">
                                <h5 class="progress-header"> Compulsory modules based on my job role</h5>

                                <ol class="compul">


                                    @foreach ($required as $compulsory)
                                        <li>{{ $compulsory->courses->name }}</li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>

                        <div class="col-12 " style="margin-top:20px">

                            <div class="row alignment-class ">
                                <h5 class="progress-header"> My completed modules</h5>
                                <table width="100%">
                                    <tr>
                                        <th></th>
                                        <th> Module Name</th>
                                        <th> Score</th>
                                        <th> Completion Date</th>
                                    </tr>
                                    @foreach ($completed as $key => $compulsory)
                                        <tr>
                                            <td>{{ $key + 1 }}.</td>
                                            <td>{{ $compulsory->courses->name }}</td>
                                            <td>{{ round(($compulsory->score / $compulsory->max_score) * 100) }} %</td>
                                            <td>{{ $compulsory->completion_date }}</td>


                                        </tr>
                                    @endforeach

                                </table>
                            </div>
                        </div>

                    </div>


                </div>
            @else
                <div class="row no-gutters">
                    <div class="col-12 mt-3 ">
                        <div class="row alignment-class ">
                            <div class="col-12">
                                <h5 class="progress-header"> Compulsory modules based on my job role</h5>

                                <ol class="compul">


                                    @foreach ($required as $compulsory)
                                        <li>{{ $compulsory->courses->name }}</li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>

                        <div class="col-12 " style="margin-top:20px">


                            <h5 class="progress-header"> My completed modules</h5>
                            <table width="100%">
                                <tr>
                                    <th></th>
                                    <th> Module Name</th>
                                    <th> Score</th>
                                    <th> Completion Date</th>
                                </tr>
                                @foreach ($completed as $key => $compulsory)
                                    <tr>
                                        <td>{{ $key + 1 }}.</td>
                                        <td>{{ $compulsory->courses->name }}</td>
                                        <td>{{ round(($compulsory->score / $compulsory->max_score) * 100) }} %</td>
                                        <td>{{ $compulsory->completion_date }}</td>


                                    </tr>
                                @endforeach

                            </table>
                        </div>

                    </div>


                </div>


            @endif
        </div>


    </div>
    </div>
    <!-- End of future courses list -->

    <div class="clearfix"><br /></div>
@endsection
