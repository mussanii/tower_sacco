<?php

use Illuminate\Support\Facades\Route;

// Register Twill routes here eg.
// Route::module('posts');

if (config('twill.enabled.users-management')) {
    Route::module('users', ['except' => ['sort', 'feature']]);
    Route::module('roles', ['except' => ['sort', 'feature']]);
    Route::get('/upload', 'UploadUserController@usersUpload')->name('usersUpload');
    Route::post('/upload','UploadUserController@uploadStore')->name('upload.store');

}

Route::group(['prefix' => 'content'], function(){
    Route::module('pages');
    Route::module('events');
    Route::module('testimonials');
    Route::module('clients');
    Route::group(['prefix' => 'menuses'], function () {
		Route::module('menuses');
        Route::module('menuTypes');
    });
    Route::module('menuses');
    Route::module('socialmedia');
    Route::module('projecttopics');
    Route::module('footerTexts');
});
Route::module('jobRoles');
Route::module('branches');
Route::module('sessions');
Route::module('courseCategories');



Route::group(['prefix' => 'resources'], function(){

    Route::module('resources');
    Route::module('resourceTypes');
    Route::module('resourceThemes');
    Route::module('policies');

});

Route::group(['prefix' => 'groups'], function(){
    Route::module('groups');
    Route::module('groupFormats');

});
Route::group(['prefix' => 'reports'], function(){
	Route::group(['prefix' => 'graph'], function () {
        Route::name('graph.dashboard')->get('dashboard','CourseCompletionController@dashboard');
        Route::name('graph.eresults')->get('eresults','CourseCompletionController@eresults');
        Route::name('graph.cresults')->get('cresults','CourseCompletionController@cresults');
        
        Route::name('graph.yearlyEResults')->get('yearlyEResults','CourseCompletionController@yearlyEResults');
        Route::name('graph.yearlyCRresults')->get('yearlyCRresults','CourseCompletionController@yearlyCRresults');
    });
    Route::group(['prefix' => 'branch'], function () {
        Route::name('branch.completion')->any('branchCompletion','CourseCompletionController@branchCompletion');

    });
Route::module('courseCompletions');
Route::module('courseReflections');
Route::module('userLicenses');

});

Route::group(['prefix' => 'evaluation'], function(){
    Route::module('userEvaluations');
    Route::module('evaluationOptions');
    Route::module('evaluations');
});

Route::group(['prefix' => 'messages'], function(){
    Route::module('generalMessages');
    Route::module('platformMessages');
});

 Route::get('/courses', 'CoursesController@index')->name('courses.index');
 Route::get('/courses/create', 'CoursesController@create')->name('courses.create');
 Route::post('/courses','CoursesController@store')->name('courses.store');
 Route::get('/courses/{course}/edit', 'CoursesController@edit')->name('courses.edit');
 Route::post('/courses/bulk-destroy','CoursesController@bulkDestroy')->name('courses.bulk-destroy');
 Route::post('/courses/{course}','CoursesController@update')->name('courses.update');
 Route::post('/courses/{course}/delete', 'CoursesController@destroy')->name('courses.destroy');



Route::get('/sessionmeetings','SessionMeetingsController@index')->name('sessionmeetings.index');
Route::get('/sessionmeetings/create','SessionMeetingsController@create')->name('sessionmeetings.create');
Route::post('/sessionmeetings','SessionMeetingsController@store')->name('sessionmeetings.store');
Route::get('/sessionmeetings/{sessionMeeting}/edit','SessionMeetingsController@edit')->name('sessionmeetings.edit');
Route::post('/sessionmeetings/bulk-destroy','SessionMeetingsController@bulkDestroy')->name('sessionmeetings.bulk-destroy');
Route::post('/sessionmeetings/{sessionMeeting}','SessionMeetingsController@update')->name('sessionmeetings.update');
Route::delete('/sessionmeetings/{sessionMeeting}','SessionMeetingsController@destroy')->name('sessionmeetings.destroy');


Route::get('/upload/download', 'UploadUserController@downloadExcel')->name('downloadExcel');

Route::post('courseCompletion/export', 'CourseCompletionController@export')->name('courseCompletion.export');
Route::post('courseReflection/export', 'CourseReflectionController@export')->name('courseReflection.export');
Route::get('userLicenses/export', 'UserLicenseController@export')->name('userLicenses.export');
