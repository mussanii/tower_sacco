<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\SessionController;
use App\Http\Controllers\Admin\SessionResourceController;
use App\Http\Controllers\Front\PageController;
use App\Http\Controllers\Front\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Front\CourseController;
use App\Http\Controllers\Front\UserLicenseController;
use App\Http\Controllers\Front\CommentController;
use App\Http\Controllers\Front\BlogCommentController;
use App\Http\Controllers\Front\GroupController;
use App\Http\Controllers\Front\ConnectController;
use App\Http\Controllers\Front\UserActivityReactionsController;
use App\Http\Controllers\Front\UserMessagesController;
use App\Http\Controllers\Front\ForumController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Front\NotificationsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', [PageController::class, 'index'])->name('home');

Route::post('user/create', [UserController::class, 'create'])->name('user.create');
Route::get('/user/verify', [UserController::class, 'verifyForm'])->name('user.verify');
Route::post('/user/verification', [UserController::class, 'verify'])->name('user.verification');
Route::post('/user/login', [LoginController::class, 'login'])->name('user.login');
Route::post('/general_messages', [PageController::class, 'contactStore'])->name('general_messages.store');
Route::get('/password/verify', [ForgotPasswordController::class, 'verifyForm'])->name('password.verify');
Route::post('/password/verification', [ForgotPasswordController::class, 'verify'])->name('password.verification');

Route::get('/user/resend', [UserController::class, 'resendCode'])->name('resend.code');
Route::post('/user/resend', [UserController::class, 'resendStore'])->name('resend.store');

Auth::routes(['verify' => true]);


Route::any('/{key}',[PageController::class, 'pages'])->name('pages');

Route::group(['prefix'=>'profile', 'middleware'=>['auth', 'verified']], function () {
Route::get('/session-details/{id}', [PageController::class, 'sessionDetail'])->name('session.details');
Route::get('/policy-details/{id}', [PageController::class, 'policyDetail'])->name('policy.details');

Route::get('/course/detail/{key}', [CourseController::class, 'detail'])->name('course.detail');
Route::get('/courses/enroll/{id}', [CourseController::class, 'enroll'])->name('course.enroll');
Route::any('/myProfile', [UserLicenseController::class, 'profile'])->name('profile.courses');



Route::any('/resources', [PageController::class, 'resources'])->name('resources');
Route::get('/resource-details/{id}', [PageController::class, 'resourceDetails'])->name('resource.details');
Route::post('save-likedislike', [CommentController::class,'save_likedislike'])->name('save_likedislike');




    Route::any('/achievements', [UserLicenseController::class, 'achievements'])->name('profile.achievements');
    Route::any('/myCommunity', [UserLicenseController::class, 'community'])->name('profile.myCommunity');
    Route::any('/myCommunity/follow/{follow}/{id}/', [UserLicenseController::class, 'myFollow'])->name('profile.myFollow');
    Route::any('/myCommunity/followRequests/', [UserLicenseController::class, 'myFollowRequests'])->name('profile.myFollowRequests');
    Route::any('/myCommunity/accept/{id}/{follow}', [UserLicenseController::class, 'myFollowAccept'])->name('profile.myFollowAccept');


    Route::any('/trainingFeed', [UserLicenseController::class, 'trainingFeed'])->name('profile.trainingFeed');
    Route::any('/like/{follow}/{id}/', [UserLicenseController::class, 'myLike'])->name('profile.myLike');
    Route::post('/comment', [UserLicenseController::class, 'myComment'])->name('profile.myComment');
    Route::get('/comments/{follow}/{course}/', [UserLicenseController::class, 'myComments'])->name('profile.myComments');
    Route::get('/subCommentCount/{follow}/{course}/{id}/', [UserLicenseController::class, 'subCommentCount'])->name('profile.subCommentCount');
    Route::get('/subComment/{follow}/{course}/{id}/', [UserLicenseController::class, 'subComment'])->name('profile.subComment');
    Route::post('/course/evaluate/store', [UserLicenseController::class, 'Evaluationstore'])->name('course.evaluate.store');
    Route::any('/course/evaluate/{courseid}', [UserLicenseController::class, 'evaluate'])->name('course.evaluate');
    Route::any('/course/certificate/{id}', [UserLicenseController::class, 'certificate'])->name('course.certificate');
    Route::any('/edit', [UserController::class, 'profile'])->name('profile.edit');
    Route::any('/password/set', [UserController::class, 'password'])->name('profile.password.set');
    Route::any('/ppic/change', [UserController::class, 'picture'])->name('profile.ppic.change');

    Route::any('/course/certificate/{id}', [UserLicenseController::class, 'certificate'])->name('course.certificate');
    Route::any('/myProgress', [UserLicenseController::class, 'myProgress'])->name('profile.myProgress');


    Route::any('/connect', [ConnectController::class, 'activity'])->name('connect.activity');
    Route::post('/connect/create', [ConnectController::class, 'createAcivity'])->name('activity.create');
    Route::post('/connect/delete/{userActivity}', [ConnectController::class, 'deleteAcivity'])->name('activity.delete');
    Route::post('/connect/reply/create/{userActivity}', [ConnectController::class, 'createAcivityReply'])->name('reply.create');
    Route::post('/connect/reactions/{activityId}', [UserActivityReactionsController::class, 'reaction'])->name('reactions.create');
    Route::post('/connect/reply/reply/create/{userActivity}/{userActivityReply}', [UserActivityReactionsController::class, 'replyOnActionReply'])->name('reply.reply.create');
    Route::post('/connect/reply/reacttions/create/{userActivityId}/{userActivityReplyId}', [UserActivityReactionsController::class, 'reactOnReply'])->name('react.reply.create');

    Route::post('/messages/create', [UserMessagesController::class, 'sentMessage'])->name('message.create');
    Route::get('/messages/all', [UserMessagesController::class, 'index'])->name('message.index');
    Route::post('/messages/users/search/{name?}', [UserMessagesController::class, 'searchForUser'])->name('message.search');
    Route::get('/messages/chat/{user}', [UserMessagesController::class, 'getUserChat'])->name('message.chat');

    Route::any('/connect-profile/{id}', [ConnectController::class, 'profile'])->name('connect.profile');
    Route::any('/connect-profile', [ConnectController::class, 'profile'])->name('connect.my_profile');


    
    Route::any('/forum', [ForumController::class, 'index'])->name('connect.forum');
    Route::any('/connect/forum/topic', [ForumController::class, 'storeTopic'])->name('forum.topic.store');
    Route::any('/connect/forum/create', [ForumController::class, 'store'])->name('forum.store');

    Route::any('/connect/forum/follow/{id}', [ForumController::class, 'follow'])->name('forum.follow');
    Route::any('/forum/{id}', [ForumController::class, 'details'])->name('forum.details');
    Route::post('/connect/forum/reply/create/{userActivity}', [ForumController::class, 'createAcivityReply'])->name('forum.reply.create');
    Route::post('/connect/forum/reply/edit/{userActivity}', [ForumController::class, 'updateReply'])->name('connect.reply.update');
    Route::post('/connect/forum/reply/delete/{userActivity}', [ForumController::class, 'deleteReply'])->name('connect.reply.delete');
    Route::post('/connect/forum/reactions/{activityId}', [ForumController::class, 'reaction'])->name('forum.reactions.create');
    Route::post('/connect/forum/reply/reply/create/{userActivity}/{userActivityReply}', [ForumController::class, 'replyOnActionReply'])->name('forum.reply.reply.create');
    Route::post('/connect/forum/reply/reply/edit/{userActivity}', [ForumController::class, 'updateReplyReply'])->name('connect.reply.reply.update');
    Route::post('/connect/forum/reply/reply/delete/{userActivity}', [ForumController::class, 'deleteReplyReply'])->name('connect.reply.reply.delete');
    Route::post('/connect/forum/reply/reacttions/create/{userActivityId}/{userActivityReplyId}', [ForumController::class, 'reactOnReply'])->name('forum.react.reply.create');

    Route::any('/connect/forum/getforum/{id}', [ForumController::class, 'getForums'])->name('get.forums');
    Route::any('/connect/forum/getStatus/{id}/{user}', [ForumController::class, 'getStatus'])->name('get.status');

    Route::any('/groups', [GroupController::class, 'list'])->name('groups');
    Route::get('/group/activate/{id}/{key}', [GroupController::class, 'activate'])->name('group.activate');
    Route::get('/group/decline/{id}/{key}', [GroupController::class, 'decline'])->name('group.decline');
    Route::get('/group/request/{id}/{key}', [GroupController::class, 'request'])->name('group.request');
     
    Route::get('/groups/details/{id}/{key}', [GroupController::class, 'details'])->name('group.details');
    Route::any('/myGroup/followRequests/{id}', [GroupController::class, 'myFollowRequests'])->name('group.profile.myFollowRequests');
    Route::any('/myGroup/accept/{id}/{follow}', [GroupController::class, 'myFollowAccept'])->name('group.profile.myFollowAccept');

    Route::get('/group/create', [GroupController::class, 'form'])->name('group.form');
    Route::post('/group/store', [GroupController::class, 'store'])->name('group.store');
    Route::get('/group/edit/{id}', [GroupController::class, 'edit'])->name('group.edit');
    Route::post('/group/edit/{id}', [GroupController::class, 'update'])->name('group.update');
    Route::any('/group/{id}', [GroupController::class, 'delete'])->name('group.delete');

    Route::post('/group/activity/create', [GroupController::class, 'createAcivity'])->name('group.activity.create');
    Route::post('/connect/group/delete/{userActivity}', [GroupController::class, 'deleteAcivity'])->name('group.activity.delete');
    Route::post('/connect/group/reply/create/{userActivity}', [GroupController::class, 'createAcivityReply'])->name('group.reply.create');
    Route::post('/connect/group/reactions/{activityId}', [GroupController::class, 'reaction'])->name('group.reactions.create');
    Route::post('/connect/group/reply/reply/create/{userActivity}/{userActivityReply}', [GroupController::class, 'replyOnActionReply'])->name('group.reply.reply.create');
    Route::post('/connect/group/reply/reacttions/create/{userActivityId}/{userActivityReplyId}', [GroupController::class, 'reactOnReply'])->name('group.react.reply.create');

    Route::any('/connect/group/resource', [GroupController::class, 'groupResource'])->name('group.resource'); 
    Route::any('/connect/group/forum/topic', [GroupController::class, 'groupStoreTopic'])->name('group.forum.topic.store');
    Route::any('/connect/group/forum/create', [GroupController::class, 'storeForum'])->name('group.forum.store');
    Route::any('/connect/group/forum/follow/{id}', [GroupController::class, 'follow'])->name('group.forum.follow');
    Route::any('/connect/group/forum/details/{id}', [GroupController::class, 'detailForum'])->name('group.forum.details');
    Route::post('/connect/group/forum/reply/create/{userActivity}', [GroupController::class, 'forumCreateAcivityReply'])->name('group.forum.reply.create');
    Route::post('/connect/group/forum/reactions/{activityId}', [GroupController::class, 'forumReaction'])->name('group.forum.reactions.create');
    Route::post('/connect/group/forum/reply/reply/create/{userActivity}/{userActivityReply}', [GroupController::class, 'forumReplyOnActionReply'])->name('group.forum.reply.reply.create');
    Route::post('/connect/group/forum/reply/reacttions/create/{userActivityId}/{userActivityReplyId}', [GroupController::class, 'forumReactOnReply'])->name('group.forum.react.reply.create');

    Route::any('/connect/group/forum/getforum/{id}', [GroupController::class, 'getForums'])->name('get.forumz');
    Route::any('/connect/group/forum/getStatus/{id}/{user}', [GroupController::class, 'getStatus'])->name('get.statuz');
    Route::any('/connect/group/forum/{groupid}', [GroupController::class, 'forumIndex'])->name('group.connect.forum');
   
   
   
    Route::get('/single/notification/{id}', [NotificationsController::class, 'singleNotification'])->name('single.notification');
    
    
    
});

Route::middleware(['auth', 'verified','admin'])->prefix('facilitator')->group(function () {
    Route::get('/dashboard', [HomeController::class, 'index'])->name('facilitator.dashboard');

    Route::get('/meetings', [SessionController::class, 'list'])->name('sessions.index');
    // Create meeting room using topic, agenda, start_time.
    Route::get('/meetings/create', [SessionController::class, 'form'])->name('sessions.form');
    Route::post('/meetings/store', [SessionController::class, 'store'])->name('sessions.store');
    Route::get('/meetings/edit/{id}', [SessionController::class, 'edit'])->name('sessions.edit');
    Route::post('/meetings/edit/{id}', [SessionController::class, 'update'])->name('sessions.update');
    // Get information of the meeting room by ID.
    Route::get('/meetings/{id}', [SessionController::class, 'get'])->where('id', '[0-9]+');
    Route::delete('/meetings/{id}', [SessionController::class, 'delete'])->where('id', '[0-9]+')->name('sessions.delete');

    Route::get('/session/resource/json', [SessionResourceController::class, 'json'])->name('sessionsResource.json');
    Route::get('/session/resource', [SessionResourceController::class, 'list'])->name('sessionsResource.index');

    Route::get('/group/json', [GroupController::class, 'json'])->name('group.json');
    Route::get('/group', [GroupController::class, 'list'])->name('group.index');
   
    
});


// Route::post('courses/process-reflection', [CourseController::class, 'processReflection'])->name('process-reflection');
// /* Auto-generated admin routes */
// Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
//     Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
//         Route::prefix('user-licenses')->name('user-licenses/')->group(static function() {

//         });
//     });
// });

// /* Auto-generated admin routes */
// Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
//     Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
//         Route::prefix('courses')->name('courses/')->group(static function() {
//             Route::get('/',                                             'CoursesController@index')->name('index');
//             Route::get('/create',                                       'CoursesController@create')->name('create');
//             Route::post('/',                                            'CoursesController@store')->name('store');
//             Route::get('/{course}/edit',                                'CoursesController@edit')->name('edit');
//             Route::post('/bulk-destroy',                                'CoursesController@bulkDestroy')->name('bulk-destroy');
//             Route::post('/{course}',                                    'CoursesController@update')->name('update');
//             Route::delete('/{course}',                                  'CoursesController@destroy')->name('destroy');
//         });
//     });
// });

/* Auto-generated admin routes */
// Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
//     Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
//         Route::prefix('session-meetings')->name('session-meetings/')->group(static function() {
//             Route::get('/',                                             'SessionMeetingsController@index')->name('index');
//             Route::get('/create',                                       'SessionMeetingsController@create')->name('create');
//             Route::post('/',                                            'SessionMeetingsController@store')->name('store');
//             Route::get('/{sessionMeeting}/edit',                        'SessionMeetingsController@edit')->name('edit');
//             Route::post('/bulk-destroy',                                'SessionMeetingsController@bulkDestroy')->name('bulk-destroy');
//             Route::post('/{sessionMeeting}',                            'SessionMeetingsController@update')->name('update');
//             Route::delete('/{sessionMeeting}',                          'SessionMeetingsController@destroy')->name('destroy');
//         });
//     });
// });
